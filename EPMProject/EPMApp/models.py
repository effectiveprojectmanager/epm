
from __future__ import unicode_literals
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, Group, Permission
from django.contrib.auth.models import UserManager
from django.conf import settings
from django.db import models


class EstadosAtributos(models.Model):
    """Representa a los posibles estados de las relaciones Antecesor-Sucesor entre items del sistema."""
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300, blank=True)

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        """Establece las configuraciones del modelo de base de datos"""
        managed = True
        db_table = 'estados_atributos'

class Atributos(models.Model):
    """
    Clase que representa los atributos
    """
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=100)
    fecha_creacion = models.DateField(auto_now_add=True)
    fecha_eliminacion = models.DateField(blank=True, null=True)
    estado = models.ForeignKey('EstadosAtributos', related_name='estado', default=1)
    tipo_atributo=models.ForeignKey('TipoAtributo',db_column='Tipo Atributo',null=True)

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        permissions = (
            ("listar_atributos", "Listar atributos"),
        )
        managed = True
        db_table = 'atributos'


class ComiteCambio(models.Model):
    """
    Clase que representa a un comite de cambio
    """
    nombre = models.CharField(max_length=100, blank=True)
    fecha_creacion = models.DateField(auto_now_add=True)
    fecha_eliminacion = models.DateField(blank=True, null=True)
    cantidad_miembros = models.IntegerField(blank=True, null=True)
    estado = models.IntegerField(default=1)
    id_proyecto = models.ForeignKey('Proyectos', db_column='id_proyecto', related_name='proyecto')
    usuarios = models.ManyToManyField('Usuarios', related_name='miembros')

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        permissions = (
            ("listar_comites", "Listar comites de cambios"),
            ("asignar_miembros_comite", "Asignar miembros al comite"),
            ("asignar_comite", "Asignar comite al proyecto"),
        )
        managed = True
        db_table = 'comite_cambio'

    def delete(self):
        """Elimina logicamente el comite de cambio del sistema"""
        if self.estado == 1:
            self.estado = 2
            self.save()

class EstadosFases(models.Model):
    """Representa a los posibles estados de los fases del sistema."""
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300, blank=True)

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        """Establece las configuraciones del modelo de base de datos"""
        managed = True
        db_table = 'estados_fases'

class Fases(models.Model):
    """
    Clase que representa las fases de un proyecto del sistema"""
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=100, blank=True)
    numero_fase = models.IntegerField(blank=True, null=True)
    estado = models.ForeignKey('EstadosFases', related_name='estado', default=1)
    fecha_creacion = models.DateField(auto_now_add=True)
    fecha_inicio = models.DateField(blank=True, null=True)
    fecha_finalizacion = models.DateField(blank=True, null=True)
    id_proyecto = models.ForeignKey('Proyectos', db_column='id_proyecto')

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        """Establece las configuraciones del modelo de base de datos"""
        permissions = (
            ("listar_fases", "Listar fases"),
            ("importar_fases", "Importar fases"),
            ("finalizar_fases", "Finalizar fases"),
        )
        managed = True
        db_table = 'fases'

class EstadosItem(models.Model):
    """
    Clase que representa a los posibles estados de los items del sistema.
    """
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300, blank=True)

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        """Establece las configuraciones del modelo de base de datos"""
        managed = True
        db_table = 'estados_item'

class Item(models.Model):
    """
    Clase que representa a los items de una fase del sistema"""
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300, blank=True)
    version = models.IntegerField(blank=True, null=True)
    prioridad = models.IntegerField(blank=True, null=True)
    complejidad = models.IntegerField(blank=True, null=True)
    fecha_creacion = models.DateField(auto_now_add=True)
    fecha_eliminacion = models.DateField(blank=True, null=True)
    estado = models.ForeignKey('EstadosItem', related_name='estado', default=1)
    calculo_impacto=models.IntegerField(blank=True, null=True)
    id_fase = models.ForeignKey(Fases, db_column='id_fase')
    item_version=models.IntegerField(blank=True,null=True)
    tipo_item = models.ForeignKey('TiposItems', db_column='tipo_item')
    archivo=models.FileField(upload_to="/home/mai/IS2/Repositorio/codigo/epm/EPMProject/files",null=True)

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        permissions = (
            ("listar_items", "Listar items"),
            ("aprobar_item", "Aprobar item"),
            ("confirmar_item", "Confirmar item"),
            ("revertir_item", "Revertir item"),
            ("revivir_item", "Revivir item"),
            ("historial_item", "Historial item"),
            ("calculo_impacto", "Calcular impacto de item"),
            ("listar_atributos", "Listar atributos"),
            ("cargar_atributos", "Cargar atributos"),
        )
        managed = True
        db_table = 'item'

class AtributoItem(models.Model):
    """
    Clase que representa a los posibles estados de los items del sistema.
    """
    item = models.ForeignKey(Item)
    atributo=models.ForeignKey(Atributos)
    valor = models.CharField(max_length=300, blank=True)

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.atributo.nombre

    class Meta:
        """Establece las configuraciones del modelo de base de datos"""
        managed = True
        db_table = 'atributos_item'
        verbose_name = 'Atributo del Item'
        verbose_name_plural = 'Atributos del Item'

class EstadosLineaBase(models.Model):
    """
    Clase que representa a los posibles estados de los items del sistema.
    """
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300, blank=True)

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        """Establece las configuraciones del modelo de base de datos"""
        managed = True
        db_table = 'estados_linea_base'

class LineaBase(models.Model):
    """
    Clase que representa el modelo de Linea Base de items que forman parte de una fase del proyecto"""
    nombre = models.CharField(max_length=100)
    fecha_creacion = models.DateField(auto_now_add=True)
    fecha_eliminacion = models.DateField(blank=True, null=True)
    estado = models.ForeignKey('EstadosLineaBase', related_name='estado', default=1)
    id_fase = models.ForeignKey(Fases, db_column='id_fase')
    items = models.ManyToManyField('Item')

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        permissions = (
            ("listar_lineas_base", "Listar lineas bases"),
        )
        managed = True
        db_table = 'linea_base'


class EstadosProyectos(models.Model):
    """Representa a los posibles estados de los proyectos del sistema."""
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300, blank=True)

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        """Establece las configuraciones del modelo de base de datos"""
        managed = True
        db_table = 'estados_proyectos'


class Proyectos(models.Model):
    """Representa a los proyectos del sistema."""
    nombre = models.CharField('Nombre', max_length=100)
    descripcion = models.TextField('Descripcion', max_length=300, blank=True)
    cantidad_fases = models.IntegerField(blank=True, null=True)
    complejidad_total = models.IntegerField(blank=True, null=True)
    fecha_creacion = models.DateField(auto_now_add=True)
    fecha_finalizacion = models.DateField(blank=True, null=True)
    estado = models.ForeignKey('EstadosProyectos', related_name='estado', default=1)
    lider = models.ForeignKey('Usuarios', related_name='lider', blank=True, null=True)
    miembros = models.ManyToManyField('Usuarios')

    def delete(self):
        """Elimina logicamente el proyecto del sistema"""
        if self.estado == 1:
            self.estado = 2
            self.save()

    def delete_selected(self):
        """Elimina logicamente el proyecto del sistema"""
        if self.estado == 1:
            self.estado = 2
            self.save()

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        """Establece las configuraciones del modelo de base de datos"""
        managed = True
        permissions = (
            ("listar_proyectos", "Listar proyectos"),
            ("listar_miembros", "Listar miembros de proyectos"),
            ("cancelar_proyectos", "Cancelar proyectos"),
            ("iniciar_proyectos", "Iniciar proyectos"),
            ("finalizar_proyectos", "Finalizar proyectos"),
            ("asignar_usuario_proyecto", "Asignar usuario a proyecto"),
            ("desasignar_usuario_proyecto", "Desasignar usuario a proyecto"),
        )
        db_table = 'proyectos'
        ordering = ('nombre',)


class EstadosRelacionPadre(models.Model):
    """Representa a los posibles estados de las relaciones Padre-Hijo entre items del sistema."""
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300, blank=True)

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        """Establece las configuraciones del modelo de base de datos"""
        managed = True
        db_table = 'estados_relacion_padre'


class RelacionPadre(models.Model):
    """Representa la relacion Padre-Hijo entre items del sistema"""
    #id_relacion = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=300, blank=True)
    descripcion = models.CharField(max_length=300, blank=True)
    fecha_creacion = models.DateField(auto_now_add=True)
    fecha_eliminacion = models.DateField(blank=True, null=True)
    estado = models.ForeignKey('EstadosRelacionPadre', related_name='estado', default=1)
    item_hijo = models.ForeignKey(Item, related_name='item_hijo', null=True)
    item_padre = models.ForeignKey(Item, related_name='item_padre', null=True)

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        permissions = (
            ("listar_relaciones", "Listar relaciones"),
        )
        managed = True
        db_table = 'relacion_padre'


class EstadosRelacionAntecesor(models.Model):
    """Representa a los posibles estados de las relaciones Antecesor-Sucesor entre items del sistema."""
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300, blank=True)

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        """Establece las configuraciones del modelo de base de datos"""
        managed = True
        db_table = 'estados_relacion_antecesor'


class RelacionAntecesor(models.Model):
    """Representa la relacion Padre-Hijo entre items del sistema"""
    #id_relacion = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=300, blank=True)
    descripcion = models.CharField(max_length=300, blank=True)
    fecha_creacion = models.DateField(auto_now_add=True)
    fecha_eliminacion = models.DateField(blank=True, null=True)
    estado = models.ForeignKey('EstadosRelacionAntecesor', related_name='estado', default=1)
    item_sucesor = models.ForeignKey(Item, related_name='item_sucesor', null=True)
    item_antecesor = models.ForeignKey(Item, related_name='item_antecesor', null=True,
                                       help_text="Seleccione el item antecesor")

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        managed = True
        db_table = 'relacion_antecesor'


class RolesProyectos(models.Model):
    #id_rol_proyecto = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300, blank=True)
    fecha_creacion = models.DateField(auto_now_add=True)
    fecha_eliminacion = models.DateField(blank=True, null=True)
    estado = models.IntegerField(default=1)
    id_proyecto = models.ManyToManyField(Proyectos, db_column='id_proyecto', blank=True, null=True,
                                         verbose_name="Proyectos")
    permisos = models.ManyToManyField(Permission)
    usuarios = models.ManyToManyField('Usuarios', through='UsuariosRolesProyectos')

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        permissions = (
            ("listar_roles_proyectos", "Listar roles de proyectos"),
            ("asignar_rol_proyecto", "Asignar roles de proyectos a usuarios"),
            ("listar_permisos", "Listar permisos"),
        )
        managed = True
        verbose_name = 'rol del proyecto'
        verbose_name_plural = 'roles del proyecto'
        db_table = 'roles_proyectos'


class EstadosSolicitud(models.Model):
    """
    Clase que representa a los posibles estados de los items del sistema.
    """
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300, blank=True)

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        """Establece las configuraciones del modelo de base de datos"""
        managed = True
        db_table = 'estados_solicitud'

class SolicitudItems(models.Model):
    solicitud = models.ForeignKey('SolicitudCambio',)
    items=models.ForeignKey('Item')
    impacto=models.IntegerField(null=True)

    def __str__(self):
        return self.items.descripcion


    class Meta:
            """Establece las configuraciones del modelo de base de datos"""
            managed = True
            db_table = 'solicitud_items'
            verbose_name = 'Item de la solicitud'
            verbose_name_plural = 'Items de la solicitud'

class SolicitudCambio(models.Model):
    nombre=models.CharField(max_length=1000, blank=True)
    motivo = models.CharField(max_length=1000, blank=True)
    fecha_creacion = models.DateField(auto_now_add=True)
    fecha_eliminacion = models.DateField(blank=True, null=True)
    estado = models.ForeignKey(EstadosSolicitud, default=1)
    proyecto=models.ForeignKey(Proyectos)
    usuario=models.ForeignKey('Usuarios')
    votos_aprobacion = models.IntegerField(default=0,blank=True, null=True)
    votos_rechazo = models.IntegerField(default=0, blank=True, null=True)
    items= models.ManyToManyField(Item, through='SolicitudItems')

    def __str__(self):
        return self.nombre

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        permissions = (
            ("listar_solicitudes", "Listar solicitudes de cambios"),
            ("confirmar_solicitud", "Confirmar solicitud de cambio"),
            ("procesar_solicitud", "Procesar Solicitud de cambio"),
        )
        managed = True
        db_table = 'solicitud_cambio'

class TiposVotaciones(models.Model):
    """Representa a los posibles estados de los proyectos del sistema."""
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300, blank=True)

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        """Establece las configuraciones del modelo de base de datos"""
        managed = True
        db_table = 'tipos_votaciones'

class Votaciones(models.Model):
    solicitud = models.ForeignKey('SolicitudCambio',)
    miembro=models.ForeignKey('Usuarios')
    voto=models.IntegerField(blank=True)

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        if self.voto==1:
            return "Aprobacion"
        else:
            return "Rechazo"


    class Meta:
            """Establece las configuraciones del modelo de base de datos"""
            permissions = (
                        ("listar_votos", "Listar votos registrados"),
                    )
            managed = True
            db_table = 'votaciones'
            verbose_name = 'Votacion'
            verbose_name_plural = 'Votaciones'

class PermisosTemporales(models.Model):
    solicitud = models.ForeignKey('SolicitudCambio',)
    miembro=models.ForeignKey('Usuarios')
    item=models.ForeignKey('Item')
    permiso=models.ForeignKey(Permission)
    estados=models.ForeignKey('Estados', default=1)


    class Meta:
            """Establece los permisos temporales a los usuarios para modificar los items de una solicitud dada"""
            managed = True
            db_table = 'permisos_temporales'
            verbose_name = 'Permisos temporales'
            verbose_name_plural = 'Permisos temporales'

class Estados(models.Model):
    """
    Clase que representa a los posibles estados activo e inactivo de las entidades del sistema.
    """
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300, blank=True)

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        """Establece las configuraciones del modelo de base de datos"""
        managed = True
        db_table = 'estados'

class TipoAtributo(models.Model):
    """
    Representa la clase que almacena el tipo de atributo
    """
    nombre = models.CharField(max_length=100, blank=True)
    descripcion = models.TextField(max_length=100,blank=True, null=True)
    def __str__(self):
        return self.nombre
    class Meta:
        managed = True
        db_table = 'tipo_atributo'

class EstadosTipoItem(models.Model):
    """Representa a los posibles estados de las relaciones Antecesor-Sucesor entre items del sistema."""
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300, blank=True)

    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre

    class Meta:
        """Establece las configuraciones del modelo de base de datos"""
        managed = True
        db_table = 'estados_tipo_item'

class TiposItems(models.Model):
    """
    Representa la clase que almacena el tipo de item
    """
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300, blank=True)
    fecha_creacion = models.DateField(auto_now_add=True)
    fecha_eliminacion = models.DateField(blank=True, null=True)
    estado = models.ForeignKey('EstadosTipoItem', related_name='estado', default=1)
    id_fase = models.ForeignKey(Fases, db_column='id_fase')
    atributos = models.ManyToManyField(Atributos,blank=True, null=True)
    def __str__(self):
        """Retorna una cadena identificadora de la clase representacion de la clase"""
        return self.nombre
    class Meta:
        permissions = (
            ("listar_tipos_item", "Listar tipos de item"),
            ("importar_tipos_item", "Importar tipos de item"),
        )
        managed = True
        db_table = 'tipos_items'


# class TiposItemsAtributos(models.Model):
#     id_tipo_item_atributo = models.IntegerField(primary_key=True)
#     id_tipo_item = models.ForeignKey(TiposItems, db_column='id_tipo_item')
#     id_atributo = models.ForeignKey(Atributos, db_column='id_atributo')
#     fecha_creacion = models.DateField()
#     fecha_eliminacion = models.DateField(blank=True, null=True)
#     valor = models.CharField(max_length=1000, blank=True)
#     class Meta:
#         managed = True
#         db_table = 'tipos_items_atributos'

class Usuarios(AbstractBaseUser, PermissionsMixin):
    """Representa los usuarios del sistema."""
    username = models.CharField(max_length=254, unique=True, db_index=True)
    nombres = models.CharField(max_length=30, blank=True)
    apellidos = models.CharField(max_length=30, blank=True)
    email = models.EmailField(verbose_name='correo electronico', max_length=255, unique=True, db_index=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField()
    date_joined = models.DateTimeField()
    documento = models.IntegerField(blank=True, null=True)
    direccion = models.CharField(max_length=30, blank=True)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    estado = models.IntegerField(default=1)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def get_full_name(self):
        """El usuario es identificado por la direccion de mail."""
        return self.email

    def get_short_name(self):
        """El usuario es identificado por su direccion de email."""

        return self.email

    def __str__(self):
        return self.username

    def delete(self):
        if self.estado == 1:
            self.estado = 2
            self.is_active = False
            self.is_staff = False
            self.save()




def get_group_permissions(self, obj=None):
    print("get_group_permissions")


def has_module_perms(self, app_label):
    print("has_module_perms")
    return super(Usuarios, self).has_module_perms(app_label)


def has_perm(self, perm, obj=None):
    print("has_perm")
    return super(Usuarios, self).has_perm(perm, obj)


def has_perms(self, perm_list, obj=None):
    print("has_perms 3")

    return self.super(Usuarios, self).has_perms()


def get_all_permissions(self, obj=None):
    print("se obtuvieron los permisos del usuario")
    return self.super(Usuarios, self).get_all_permissions()


class Balance(models.Model):
    """
    Historial de cargas y deducciones del usuario
    """
    fecha = models.DateTimeField(auto_now_add=True, blank=True)
    usuario = models.ForeignKey(settings.AUTH_USER_MODEL)

    def __str__(self):
        return self.usuario

    class Meta:
        managed = True
        db_table = 'usuarios'


# class UsuariosProyectos(models.Model):
#     #id_usuario_proyecto = models.IntegerField(primary_key=True)
#     id_usuario = models.ForeignKey(Usuarios)
#     id_proyecto = models.ForeignKey(Proyectos)
#     estado = models.IntegerField()
#     fecha_creacion = models.DateField()
#     fecha_eliminacion = models.DateField(blank=True, null=True)
#     class Meta:
#         managed = True
#         db_table = 'usuarios_proyectos'

class UsuariosRolesProyectos(models.Model):
    rol_proyecto = models.ForeignKey(RolesProyectos, db_column='id_rol_proyecto')
    usuario = models.ForeignKey(Usuarios, db_column='id_usuario_proyecto')
    fase = models.ForeignKey(Fases, db_column='id_fase')
    fecha_creacion = models.DateField(auto_now_add=True)
    fecha_eliminacion = models.DateField(blank=True, null=True)
    estado = models.IntegerField(default=1)

    class Meta:
        managed = True
        db_table = 'usuarios_roles_proyectos'

    def __str__(self):
        return 'El usuario %s posee el rol de %s en la fase %s' % (
            self.usuario.username, self.rol_proyecto, self.fase.nombre)

# class UsuariosRolesSistemas(models.Model):
#     id_usuario_roles_sistemas = models.IntegerField(primary_key=True)
#     id_usuario = models.ForeignKey(Usuarios, db_column='id_usuario')
#     id_rol = models.ForeignKey(RolesSistema, db_column='id_rol')
#     fecha_eliminacion = models.DateField(blank=True, null=True)
#     estado = models.IntegerField(blank=True, null=True)
#     fecha_creacion = models.DateField()
#     class Meta:
#         managed = True
#         db_table = 'usuarios_roles_sistemas'
