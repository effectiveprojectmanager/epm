from django.db.models import Q

from EPMApp.models import RolesProyectos, PermisosTemporales

#from EPMApp.models import RolesProyectos,PermisosTemporales, Estados


__author__ = 'mai'

def has_project_permission(id_user, id_proyecto, permiso):
    """
    Metodo que comprueba que un usuario determinado tenga el permiso en un proyecto dado
    @return: True, False
    @rtype: Boolean
    """
    print("has_project_permission")
    print(id_user)
    print(id_proyecto)
    print(permiso)
    roles = RolesProyectos.objects.filter(
        Q(permisos__codename=permiso) & Q(usuarios=id_user) & Q(id_proyecto=id_proyecto))
    print(roles)

    has_permission = False
    if roles:
        has_permission = True
        print("El usuario tiene permiso")

    return has_permission

def has_fase_permission(id_user, id_proyecto, permiso, id_fase):
    print("has_fase_permission")
    print(id_user)
    print(id_proyecto)
    print(permiso)
    print(id_fase)
    roles = RolesProyectos.objects.filter(
        Q(permisos__codename=permiso) & Q(usuarios=id_user) & Q(id_proyecto=id_proyecto) & Q(
            usuarios__usuariosrolesproyectos__fase=id_fase))
    print(roles)

    has_permission = False
    if roles:
        has_permission = True

    return has_permission


def has_perm_temporal(id_user, permiso, id_item):
    print("has_perm_temporal")

    perm = PermisosTemporales.objects.filter(
        Q(permiso__codename=permiso) & Q(miembro__id=id_user) & Q(item__id=id_item) & Q(estados=1))

    print(perm)

    has_permission = False

    if perm:
        has_permission = True
        # estado=Estados.object.get(id=2)#inactivo
        # perm.estado=estado
        # perm.save()
    return has_permission

# def has_temporal_permission(id_user, id_item, permiso):
#     perm = PermisosTemporales.objects.get(
#            Q(permisos__codename=permiso) & Q(miembro__id=id_user) & Q(item__id=id_item) & Q(estado=1))
#
#
#     has_permission = False
#     if perm:
#         has_permission = True
#
#         estado=Estados.object.get(id=2)#inactivo
#         perm.estado=estado
#         perm.save()
#     return has_permission
