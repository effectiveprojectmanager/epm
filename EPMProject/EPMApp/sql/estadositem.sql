INSERT INTO estados_item (id, nombre, descripcion) VALUES (1, 'Desarrollo','Esta habilitado para modificaciones');
INSERT INTO estados_item (id, nombre, descripcion) VALUES (2, 'Desarrollo Restringido','Solo puede ser modificado por el usuario que solicito su cambio cuando este se encontraba en una linea base.');
INSERT INTO estados_item (id, nombre, descripcion) VALUES (3, 'Pendiente','El item ha sido terminado y se encuentra pendiente de ser aprobado.');
INSERT INTO estados_item (id, nombre, descripcion) VALUES (4, 'Aprobado con modificacion','El item ha sido aprobado, y se permitira regresar al estado de desarrollo en caso de que se requieran modificaciones sobre el.');
INSERT INTO estados_item (id, nombre, descripcion) VALUES (5, 'Aprobado sin modificacion','El item ha sido aprobado y ya no se permiten modificaciones sobre el item.');
INSERT INTO estados_item (id, nombre, descripcion) VALUES (6, 'Eliminado','El item ha sido eliminado y no se podran realizar cambios sobre el mismo.');
INSERT INTO estados_item (id, nombre, descripcion) VALUES (7, 'Bloqueado','El item se encuentra en una linea base y solo se permitiran modificaciones a traves de solicitudes de cambio.');
INSERT INTO estados_item (id, nombre, descripcion) VALUES (8, 'Revision','Las modificaciones sobre el item padre o el item antecesor del ítem en cuestion ha sido aprobada en una solicitud de cambio.');
INSERT INTO estados_item (id, nombre, descripcion) VALUES (9, 'Pendiente sin modificacion','');
