INSERT INTO tipo_atributo (id, nombre, descripcion) VALUES (1, 'Cadena','Almacena caracteres de A-Z');
INSERT INTO tipo_atributo (id, nombre, descripcion) VALUES (2, 'Numerico','Almacena caracteres numericos');
INSERT INTO tipo_atributo (id, nombre, descripcion) VALUES (3, 'Fecha','Almacena el formato fecha');
INSERT INTO tipo_atributo (id, nombre, descripcion) VALUES (4, 'Archivo','Almacena el formato archivo');