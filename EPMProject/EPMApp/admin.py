from django.contrib import admin

from django.contrib.auth.models import Permission
from django.contrib.auth.models import Group

from EPMApp.admins.atributos import AtributosAdmin

from EPMApp.admins.fases import FasesAdmin
from EPMApp.admins.items import ItemAdmin, AtributosItemAdmin
from EPMApp.admins.linea_base import LineaBaseAdmin
from EPMApp.admins.permisos import PermisosAdmin
from EPMApp.admins.proyectos import ProyectosAdmin
from EPMApp.admins.relaciones import RelacionPadreAdmin, RelacionAntecesorAdmin
from EPMApp.admins.roles import RolesAdmin
from EPMApp.admins.solicitudes import SolicitudAdmin
from EPMApp.admins.tipo_item import TiposItemsAdmin
from EPMApp.admins.user_rol_proyecto import UsuarioRolesProyectosAdmin
from EPMApp.admins.usuarios import UserAdmin
from EPMApp.admins.votaciones import VotacionesAdmin
from EPMApp.models import ComiteCambio, RolesProyectos, UsuariosRolesProyectos, RelacionPadre, \
    RelacionAntecesor, SolicitudCambio, SolicitudItems, Votaciones, AtributoItem
from EPMApp.models import Proyectos, Fases, TiposItems, Item, Atributos
from EPMApp.models import Usuarios
from EPMApp.models import LineaBase
from EPMApp.admins.comites import ComiteAdmin

SolicitudItems._meta.auto_created = True
admin.site.register(Usuarios, UserAdmin)
admin.site.register(Proyectos, ProyectosAdmin)
admin.site.register(RolesProyectos, RolesAdmin)
admin.site.register(Permission, PermisosAdmin)
admin.site.register(ComiteCambio, ComiteAdmin)
admin.site.register(Fases, FasesAdmin)
admin.site.register(TiposItems, TiposItemsAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Atributos, AtributosAdmin)
admin.site.register(UsuariosRolesProyectos, UsuarioRolesProyectosAdmin)
admin.site.register(LineaBase, LineaBaseAdmin)
admin.site.register(RelacionPadre, RelacionPadreAdmin)
admin.site.register(RelacionAntecesor, RelacionAntecesorAdmin)
admin.site.register(SolicitudCambio,  SolicitudAdmin)
admin.site.register(Votaciones, VotacionesAdmin)
admin.site.register(AtributoItem, AtributosItemAdmin)
admin.site.disable_action('delete_selected')

