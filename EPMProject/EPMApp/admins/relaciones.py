from django.contrib.admin.views.main import ChangeList

from django.contrib import admin
from django.db.models import Q
from django.http import HttpResponseRedirect
from EPMApp import utils
from EPMApp import configuraciones

from EPMApp.models import RelacionPadre, RelacionAntecesor, SolicitudItems, Proyectos


from EPMApp.models import Fases, Item

SESSION_RELA_PADRE=6
SESSION_RELA_ANTECESOR=7
SESSION_VER_RELACION=9
SESSION_VER_PADRE=10
SESSION_VER_ANTECESOR=11
class RelacionPadreAdmin(admin.ModelAdmin):
    """
    Representa el administrador de la relacion Padre-Hijo
    """
    model=RelacionPadre
    fields=('nombre', 'descripcion', 'item_padre')
    list_display = ('nombre','descripcion','estado')
    actions = ['conservar_relacion','eliminar_relacion_padre']

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
         Despliega la lista de los items candidatos a ser padre para la nueva relacion

        """
        print(db_field.name)
        if db_field.name=="item_padre":
            fase_pk=request.session['id_fase']
            item_hijo_pk=request.session['id_item']
            if fase_pk:
                print("Id Fases:", fase_pk)
                f=Fases.objects.get(id=fase_pk)
                qs=f.item_set.all()
                rela_padre=RelacionPadre.objects.filter(item_hijo_id=item_hijo_pk) #obtiene la relacion en la cual es hijo
                rela_hijo=RelacionPadre.objects.filter(item_padre_id=item_hijo_pk) # obtiene la relacion en la cual es padre
                if rela_padre:
                    for i in rela_padre:
                        item_padre=i.item_padre
                    if item_padre:
                        id_padre=item_padre.id
                        kwargs["queryset"]=qs.exclude(Q(pk=item_hijo_pk)|Q(pk=id_padre))
                elif rela_hijo:
                    for i in rela_hijo:
                        item_hijo=i.item_hijo
                    if item_hijo:
                        id_hijo=item_hijo.id
                        kwargs["queryset"]=qs.exclude(Q(pk=item_hijo_pk)|Q(pk=id_hijo))
                else:
                    kwargs["queryset"]=qs.exclude(pk=item_hijo_pk)
        return super(RelacionPadreAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def eliminar_relacion_padre(self, request, queryset):
        """Elimina logicamente una relacion Padre-Hijo dada. La relacion adquiere el estado de Eliminada """
        proyecto_pk= request.session['id_proyecto']
        fase_pk=request.session['id_fase']

        if len(queryset) == 1:
            for obj in queryset:
                has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.ELIMINAR_RELACION_P, fase_pk)
                if has_perm:
                    estado = obj.estado.id
                    item_actual=obj.id
                    # Comprueba si el item esta en una solicitud de cambio y que la misma este en estado pendiente
                    # Si la solicitud se encuentra en estado pendiente, no se podra agregar ni eliminar relacion al item
                    sol_items=SolicitudItems.objects.filter(items=item_actual) #objeto de la tabla intermedia de solicitud de cambio
                    solicitud=None #en caso de que no existe una solicitud
                    for i in sol_items:
                        solicitud=i.solicitud #solicitud de cambio del item actual
                    if solicitud:
                        estado=solicitud.estado.id
                        if estado==3: #si la solicitud de cambio ya ha sido confirmada y esta en espera de ser procesada
                            message_bi="No se puede realizar la accion ya que el item se encuentra en una solicitud de cambio en espera de ser procesada"
                            self.message_user(request,message_bi)
                    elif estado == 1:
                        rows_updated = queryset.update(estado=2)
                        if rows_updated == 1:
                            message_bi = "La relacion se ha eliminado"
                            self.message_user(request, message_bi)
                        else:
                            message_bit = "%s relaciones se han eliminado" % rows_updated
                            self.message_user(request, "%s exitosamente." % message_bit)
                    elif estado == 2:
                        message_bi = "La relacion no puede eliminarse debido a que ya se encuentra Eliminado"
                        self.message_user(request, message_bi)

                    # else:
                    #     message_bi = "No posee permiso para eliminar el proyecto %s" % obj
                    #     self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para Eliminar Relacion Padre Hijo"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione una sola relacion"
            self.message_user(request, message_bi)
    eliminar_relacion_padre.short_description = "Eliminar Relacion"

    # def response_post_save_add(self, request, obj):
    #         fase_pk=request.session['id_fase']
    #         return HttpResponseRedirect("/admin/EPMApp/relacionpadre/?item_hijo=%s" %fase_pk)

    def conservar_relacion(self, request, queryset):
        """
        Conserva la relacion Padre-Hijo de la version revertida del item
        @param: request,queryset
        @xtype:
        """
        item_pk=0
        try:
            item_pk=request.session['item_revertido']
        except:
            print("No se pudo obtener el id de item")
        print("ID item revertido: ", item_pk)
        for obj in queryset:
            relacion=RelacionPadre.objects.get(pk=obj.id)
            print("La relacion es: ",obj.id)
            if relacion:
                relacion_nuevo=RelacionPadre(nombre=relacion.nombre,descripcion=relacion.descripcion,item_padre_id=relacion.item_padre_id)
                relacion_nuevo.item_hijo_id=item_pk
                relacion_nuevo.save()
                message_bit = "Se ha conservado la relacion"
                self.message_user(request, "%s exitosamente." % message_bit)
            else:
                message_bi = "No se ha podido conservar la relacion"
                self.message_user(request, message_bi)
    conservar_relacion.short_description = "Conservar Relacion"

    def get_actions(self, request):
        """
         Desactiva las acciones predeterminadas del sistema de la Administracion de Relaciones
        @param: request
        @xtype:
        @return: Lista de acciones
        @rtype: actions
        """
        print("Entro en el actions")
        b=0
        try:
            b=request.session['proceso']
        except:
            print("No pudo obtenerse el id de proceso")
        actions = super(RelacionPadreAdmin, self).get_actions(request)
        if b:
            if b!=SESSION_RELA_PADRE:
                print("Llego a entrar")
                del actions['conservar_relacion']


        return actions

    def save_model(self, request, obj, form, change):
        """
        Metodo que almacena los valores de la nueva relacion creada
        @param: request,queryset, obj, form, change
        @xtype:
        """
        item_hijo_pk=request.session['id_item']
        rela_padre=RelacionPadre.objects.filter(item_hijo_id=item_hijo_pk) # obtiene la relacion en la cual es hijo
        rela_hijo=RelacionPadre.objects.filter(item_padre_id=item_hijo_pk) #obtiene la relacion en la cual es padre
        print("El item actual es:",item_hijo_pk)
        print("La relacion donde es padre es:",rela_hijo)
        # if rela_hijo: #relacion en la que es padre
        #     for i in rela_hijo:
        #         item_hijo=i.item_hijo #item hijo
        #         print("El item hijo es: ",item_hijo)
        #     id_hijo=item_hijo.id # id del item hijo
        #     print("El id hijo es:",id_hijo)
        #     rela_padre=RelacionPadre.objects.filter(item_hijo_id=id_hijo) #obtiene la relacion en la cual es hijo
        #     print("La relacion donde es hijo es:",rela_padre)
        #     if rela_padre:
        #         print("Igual entro")
        #         for i in rela_padre:
        #             item_padre=i.item_padre
        #         id_padre=item_padre.id
        #         print("El id del padre es:",id_padre)
        #         if item_hijo_pk==id_padre:
        #             message_bi = "No se puede crear la relacion ya que el item ya posee una relacion con el item seleccionado"
        #             self.message_user(request, message_bi)
        #         else:
        #             obj.item_hijo=Item.objects.get(pk=item_hijo_pk)
        #             obj.save()
        #     else:
        #         print("Debe guardar la relacion")
        #         obj.item_hijo=Item.objects.get(pk=item_hijo_pk)
        #         obj.save()
        # else:
        obj.item_hijo=Item.objects.get(pk=item_hijo_pk)
        obj.save()

        #obj.save()

    def response_change(self, request, obj):
        """
        Metodo que utiliza el boton cancelar para anular la operacion
        @param: request,obj
        @xtype:
        @return: URL que redirecciona al listado de items
        @rtype: HttpResponseRedirect
        """
        if request.POST.has_key("_cancelar"):
                #  Aqui la magia
            msg = (('The %(name)s "%(obj)s" has been Commited.') %
                        {'name': str(obj._meta.verbose_name),
                         'obj': str(obj)})
            self.message_user(request, msg)
            #return HttpResponseRedirect("../%s/" % obj.pk)
            return HttpResponseRedirect("/admin/EPMApp/item/" % obj.pk)
        return super(RelacionPadreAdmin,self).response_change(request,obj)

    def has_add_permission(self, request):
        """
        Funcion que desactiva la opcion de agregar una relacion Padre-Hijo
        @return: True o False
        @rtype: Boolean
        """
        b=0
        item_actual=0
        try:
            b=request.session['proceso']
            item_actual=request.session['id_item']
            print("La b vale: ",b)
        except:
            print("No se puede obtener el ID proceso")
        # Comprueba si el item esta en una solicitud de cambio y si la misma esta en estado pendiente
        # En caso de que la solicitud este en estado pendiente no se podran agregar ni eliminar la relacion del item
        sol_items=SolicitudItems.objects.filter(items=item_actual) #objeto de la tabla intermedia de solicitud de cambio
        solicitud=None #en caso de que no existe una solicitud
        rela_padre=RelacionPadre.objects.filter(item_hijo_id=item_actual) #obtiene la relacion en la cual es hijo
        for i in sol_items:
            solicitud=i.solicitud #solicitud de cambio del item actual
            print("La solicitud es:",solicitud)
        if solicitud:   #si el item se encuentra en una solicitud de cambio
            estado=solicitud.estado.id
            if estado==3: #si la solicitud de cambio ya ha sido confirmada y esta en espera de ser procesada
                message_bi="El item se encuentra en una solicitud de cambio en espera de ser procesada, no se puede agregar relacion al item"
                self.message_user(request,message_bi)
                return False
            else:
                return True
        elif not solicitud and rela_padre: #si el item no esta en una solicitud de cambio y ya tiene un padre
            message_bi="El item ya posee un padre y ya no puede ser duenho de otra relacion"
            self.message_user(request,message_bi)
            return False
        elif item_actual==1:
            return False
            message_bi="El item no puede ser duenho de la relacion"
            self.message_user(request,message_bi)
        else:
            return True

    def get_queryset(self, request):
        """
        Metodo que obtiene las relaciones a los que pertenece un determinado item
        @param: request
        @xtype:
        """
        b=0
        item=0

        try:
            b=request.session['id_item']
            item=request.session['id_item'] #id del item actual
        except:
            print("No se puede obtener el ID proceso")
        print("El id del item actual es:",item)
        relacionHijo=RelacionPadre.objects.filter(Q(item_hijo_id=b)|Q(item_padre_id=b)) #relacion donde es hijo
        rela_padre=RelacionPadre.objects.filter(item_hijo_id=item) #obtiene la relacion en la cual el item es hijo
        print("La relacion donde es hijo es:",rela_padre)
        if rela_padre: #si el item seleccionado ya posee un padre
            request.session['proceso']=SESSION_VER_PADRE
        else:
            request.session['proceso']=SESSION_VER_RELACION
        return relacionHijo

    def response_post_save_add(self, request, obj):
        """
        Redirige a la lista de fases de un proyecto despues de crear una fase
        @param: request,obj
        @xtype:
        @return: URL que redirecciona a la lista de fases de un proyecto, luego de crear una fase
        @rtype: HttpResponseRedirect
        """
        item_pk=request.session['id_item']
        return HttpResponseRedirect("/admin/EPMApp/relacionpadre/")

    def get_readonly_fields(self, request, obj=None):
        """Deshabilita los campos a modificar si un proyecto ya no se encuentra en estado Iniciado
        o la fase ya no se encuentra en estado de Desarrollo
        @param: request, obj=None
        @xtype:
        """
        proyecto_pk= request.session['id_proyecto']
        p=Proyectos.objects.get(id=proyecto_pk)
        estado_proyecto=p.estado_id

        fase_pk=request.session['id_fase']
        f=Fases.objects.get(id=fase_pk)
        estado_fase=f.estado_id

        item=request.session['id_item']

        #cuando se crea
        if obj == None:
            return ('')

        #cuando existe una realcion
        if obj != None:
            estado = obj.estado.id

            #Estados del proyecto
            if estado_proyecto==1:
                return ('nombre', 'descripcion', 'item_padre')
            if estado_proyecto==2:
                #Estados de la fase
                if estado_fase==1:
                    return ('nombre', 'descripcion', 'item_padre')
                if estado_fase==2 :
                    return ('nombre', 'descripcion', 'item_padre')
                if estado_fase==3:
                    #Estado de la relacion
                    if estado==1:

                        item_padre=obj.item_padre.id
                        item_hijo=obj.item_hijo.id

                        print("Item Padre")
                        print(item_padre)
                        print("Item Hijo")
                        print(item_hijo)

                        if item == item_padre:
                            request.session['estado_relacion_padre']='sin_permiso'
                            return ('nombre', 'descripcion', 'item_padre')
                        if item == item_hijo:
                            return ('')
                    if estado==2:
                        request.session['estado_relacion_padre']=estado
                        return ('nombre', 'descripcion', 'item_padre')
                if estado_fase==4:
                    return ('nombre', 'descripcion', 'item_padre')
                if estado_fase==5:
                    return ('nombre', 'descripcion', 'item_padre')
                if estado_fase==6:
                    return ('nombre', 'descripcion', 'item_padre')
            if estado_proyecto==3:
                return ('nombre', 'descripcion', 'item_padre')
            if estado_proyecto==4:
                return ('nombre', 'descripcion', 'item_padre')
            if estado_proyecto==5:
                return ('nombre', 'descripcion', 'item_padre')

    def get_changelist(self, request, **kwargs):
        return RelacionPadreChangeList

    def has_change_permission(self, request, obj=None):
        id_proyecto=None
        id_fase=None
        try:
            id_proyecto=request.session['id_proyecto']
            id_fase=request.session['id_fase']
        except:
            print("No se encontro el id")
        has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.LISTAR_RELACIONES,id_fase)
        return has_perm

    # def has_add_permission(self, request, obj=None):
    #     id_proyecto=None
    #     id_fase=None
    #     try:
    #         id_proyecto=request.session['id_proyecto']
    #         id_fase=request.session['id_fase']
    #     except:
    #         print("No se encontro el id")
    #     has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.AGREGAR_RELACION_PADRE,id_fase)
    #     return has_perm


class RelacionAntecesorAdmin(admin.ModelAdmin):
    """Representa el administrador de la relacion Antecesor-Sucesor"""
    model=RelacionAntecesor
    fields=('nombre', 'descripcion', 'item_antecesor')
    list_display = ('nombre','descripcion','estado')
    actions = ['conservar_relacion','eliminar_relacion_antecesor']


    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        Retorna una lista de items candidatos para ser antecesores para la relacion Antecesor-Sucesor
        @param: db_field, request, **kwargs
        @xtype: RelacionAntecesorAdmin,
        """
        if db_field.name=="item_antecesor":
            print(db_field.name)
            item_pk=request.session['id_item']
            print("El item pk es")
            print(item_pk)
            fases_pk=request.session['id_fase']
            print(fases_pk)
            if fases_pk:
                f=Fases.objects.get(id=fases_pk)
                print(f)
                # comprobar que los items candidatos a ser antecesores esten en linea base
                items=Item.objects.filter(Q(id_fase=fases_pk) & Q(estado__id=7))
                kwargs["queryset"] =items.exclude(pk=item_pk)


        return super(RelacionAntecesorAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def eliminar_relacion_antecesor(self, request, queryset):
        """Elimina logicamente una relacion Antecesor-Sucesor dada. La relacion adquiere el estado de Eliminada """
        if len(queryset) == 1:
            for obj in queryset:
                #has_perm = utils.has_project_permission(request.user.id, obj.id, configuraciones.ELIMINAR_PROYECTOS)
                #if has_perm:
                proyecto_pk= request.session['id_proyecto']
                fase_pk=request.session['id_fase']

                estado = obj.estado.id
                has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.ELIMINAR_RELACION_S, fase_pk)
                if has_perm:
                    if estado == 1:
                        rows_updated = queryset.update(estado=2)
                        if rows_updated == 1:
                            #request.session['id_proyecto'] = obj.id
                            message_bit = "La relacion se ha eliminado"
                            self.message_user(request, "%s exitosamente." % message_bit)
                        else:
                            message_bit = "La relacion no puede eliminarse"
                            self.message_user(request, message_bit)
                    elif estado == 2:
                        message_bi = "La relacion no puede eliminarse debido a que ya se encuentra Eliminado"
                        self.message_user(request, message_bi)

                    # else:
                    #     message_bi = "No posee permiso para eliminar el proyecto %s" % obj
                    #     self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para Eliminar Relacion Antecesor-Sucesor"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione una sola relacion"
            self.message_user(request, message_bi)
    eliminar_relacion_antecesor.short_description = "Eliminar Relacion"

    def response_change(self, request, obj):
        """
        Metodo que utiliza el boton cancelar para anular la operacion
        @param: request,obj
        @xtype:
        @return: URL que redirecciona al listado de items
        @rtype: HttpResponseRedirect
        """
        if request.POST.has_key("_cancelar"):
                #  Aqui la magia
            msg = (('La %(name)s "%(obj)s" ha sido cancelada.') %
                        {'name': str(obj._meta.verbose_name),
                         'obj': str(obj)})
            self.message_user(request, msg)
            #return HttpResponseRedirect("../%s/" % obj.pk)
            return HttpResponseRedirect("/admin/EPMApp/item/" % obj.pk)
        return super(RelacionAntecesorAdmin,self).response_change(request,obj)

    def save_model(self, request, obj, form, change):
        """
        Metodo que almacena los valores de la nueva relacion creada
        @param: request,obj, form, change
        @xtype:
        """
        print(request)
        item_pk=request.session['id_item']
        print(item_pk)
        rela_antecesor=RelacionAntecesor.objects.filter(item_sucesor_id=item_pk) #obtiene la relacion en la cual es sucesor
        #rela_sucesor=RelacionAntecesor.objects.filter(item_antecesor_id=item_pk) #obtiene la relacion en la cual es antecesor
        if rela_antecesor:
            message_bi = "El item ya posee una relacion con el item seleccionado"
            self.message_user(request, message_bi)
        # elif rela_sucesor:
        #     message_bi = "El item ya posee una relacion con el item seleccionado"
        #     self.message_user(request, message_bi)
        else:
            obj.item_sucesor=Item.objects.get(pk=item_pk)
            obj.save()

    def has_add_permission(self, request):
        """
         Funcion que desactiva la opcion de agregar una relacion
         @return: True o False
         @rtype: Boolean
        """
        id_proyecto=None
        id_fase=None
        b=0
        try:
            id_proyecto=request.session['id_proyecto']
            id_fase=request.session['id_fase']
            b=request.session['proceso']
        except:
            print("No se puede obtener el ID proceso")
        if b:
            if b==SESSION_VER_RELACION:
                return False
            elif b==SESSION_VER_ANTECESOR:
                return False
            else:
                #return True
                has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.AGREGAR_RELACION_AS,id_fase)
                return has_perm


    def get_queryset(self, request):
        """
        Metodo que obtiene las relaciones a los que pertenece un determinad item
        @param: request
        @xtype:
        """
        b=0
        item=0
        request.session['proceso']=SESSION_VER_RELACION
        try:
            b=request.session['id_item']
            item=request.session['id_item'] #id del item actual
        except:
            print("No se puede obtener el ID proceso")
        relacionAntecesor=RelacionAntecesor.objects.filter(Q(item_sucesor_id=b)|Q(item_antecesor_id=b)) #relacion donde es sucesor
        print("La relacion es: ", relacionAntecesor)
        rela_antecesor=RelacionAntecesor.objects.filter(item_sucesor_id=item) #obtiene la relacion en la cual el item es sucesor
        print("La relacion donde es sucesor es:",rela_antecesor)
        if rela_antecesor: #si el item seleccionado ya posee un antecesor
            request.session['proceso']=SESSION_VER_ANTECESOR
        else:
            request.session['proceso']=SESSION_VER_RELACION
        return relacionAntecesor

    def conservar_relacion(self, request, queryset):
        """
        Conserva la relacion de la version anterior del item
        @param: request,queryset
        @xtype:
        """
        item_pk=0
        try:
            item_pk=request.session['item_revertido']
        except:
            print("No se pudo obtener el id de item")
        print("ID item revertido: ", item_pk)
        for obj in queryset:
            relacion=RelacionAntecesor.objects.get(pk=obj.id)
            print("La relacion es: ",obj.id)
            if relacion:
                relacion_nuevo=RelacionAntecesor(nombre=relacion.nombre,descripcion=relacion.descripcion,item_antecesor_id=relacion.item_antecesor_id)
                relacion_nuevo.item_sucesor_id=item_pk
                relacion_nuevo.save()
                message_bit = "Se ha conservado la relacion"
                self.message_user(request, "%s exitosamente." % message_bit)
            else:
                message_bi = "No se ha podido conservar la relacion"
                self.message_user(request, message_bi)
    conservar_relacion.short_description = "Conservar Relacion"

    def get_actions(self, request):
        """
        Desactiva las acciones predeterminadas del sistema de la Administracion de Relacion Antecesor-Sucesor
        @param: request
        @xtype:
        @return: Lista de acciones
        @rtype: actions
        """
        b=0
        try:
            b=request.session['proceso']
        except:
            print("No pudo obtenerse el id de proceso")
        actions = super(RelacionAntecesorAdmin, self).get_actions(request)
        print("La b aqui vale: ",b)
        if b:
            if b!=SESSION_RELA_ANTECESOR:
                del actions['conservar_relacion']



        return actions

    def response_post_save_add(self, request, obj):
        """
        Redirige a la lista de fases de un proyecto despues de crear una fase
        @param: request,obj
        @xtype:
        @return: URL que redirecciona a la lista de fases de un proyecto, luego de crear una fase
        @rtype: HttpResponseRedirect
        """
        item_pk=request.session['id_item']
        return HttpResponseRedirect("/admin/EPMApp/relacionantecesor/")



    def get_changelist(self, request, **kwargs):
        """
        Configuraciones de la lista principal de relaciones
        @param: request,**kwargs
        @xtype:
        @return Lista de relaciones
        """
        return RelacionAntecesorChangeList

    def get_readonly_fields(self, request, obj=None):
        """Deshabilita los campos a modificar si un proyecto ya no se encuentra en estado Iniciado
        o la fase ya no se encuentra en estado de Desarrollo
        @param: request, obj=None
        @xtype:
        """
        proyecto_pk= request.session['id_proyecto']
        p=Proyectos.objects.get(id=proyecto_pk)
        estado_proyecto=p.estado_id

        fase_pk=request.session['id_fase']
        f=Fases.objects.get(id=fase_pk)
        estado_fase=f.estado_id

        item=request.session['id_item']

        #cuando se crea
        if obj == None:
            return ('')

        #cuando existe una realcion
        if obj != None:
            estado = obj.estado.id

            #Estados del proyecto
            if estado_proyecto==1:
                return ('nombre', 'descripcion', 'item_antecesor')
            if estado_proyecto==2:
                #Estados de la fase
                if estado_fase==1:
                    return ('nombre', 'descripcion', 'item_antecesor')
                if estado_fase==2 :
                    return ('nombre', 'descripcion', 'item_antecesor')
                if estado_fase==3:
                    #Estado de la relacion
                    if estado==1:
                        item_sucesor=obj.item_padre.id
                        item_antecesor=obj.item_hijo.id

                        print("Item Sucesor")
                        print(item_sucesor)
                        print("Item Antecesor")
                        print(item_antecesor)

                        if item == item_antecesor:
                            request.session['estado_relacion_ante']='sin_permiso'
                            return ('nombre', 'descripcion', 'item_antecesor')
                        if item == item_sucesor:
                            return ('')

                    if estado==2:
                        request.session['estado_relacion_ante']=estado
                        return ('nombre', 'descripcion', 'item_antecesor')
                if estado_fase==4:
                    return ('nombre', 'descripcion', 'item_antecesor')
                if estado_fase==5:
                    return ('nombre', 'descripcion', 'item_antecesor')
                if estado_fase==6:
                    return ('nombre', 'descripcion', 'item_antecesor')
            if estado_proyecto==3:
                return ('nombre', 'descripcion', 'item_antecesor')
            if estado_proyecto==4:
                return ('nombre', 'descripcion', 'item_antecesor')
            if estado_proyecto==5:
                return ('nombre', 'descripcion', 'item_antecesor')

    def has_change_permission(self, request, obj=None):
        id_proyecto=None
        id_fase=None
        try:
            id_proyecto=request.session['id_proyecto']
            id_fase=request.session['id_fase']
        except:
            print("No se encontro el id")
        has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.LISTAR_RELACIONES,id_fase)
        return has_perm

    # def has_add_permission(self, request, obj=None):
    #     id_proyecto=None
    #     id_fase=None
    #     try:
    #         id_proyecto=request.session['id_proyecto']
    #         id_fase=request.session['id_fase']
    #     except:
    #         print("No se encontro el id")
    #     has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.AGREGAR_RELACION_AS,id_fase)
    #     return has_perm


class RelacionPadreChangeList(ChangeList):
    """
    Clase que administra la lista de cambio de relaciones padre-hijo
    """
    def __init__(self, *args, **kwargs):
        super(RelacionPadreChangeList, self).__init__(*args, **kwargs)
        self.title = "Seleccione la relacion y la operacion que desea realizar"


class RelacionAntecesorChangeList(ChangeList):
    """
    Clase que administra la lista de cambio de relaciones antecesor-sucesor
    """
    def __init__(self, *args, **kwargs):
        super(RelacionAntecesorChangeList, self).__init__(*args, **kwargs)
        self.title = "Relacion Antecesor-Sucesor"