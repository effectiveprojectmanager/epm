from django.contrib import admin
from django.contrib.admin.views.main import ChangeList
from django.contrib.auth.models import Permission
from django.db.models import Q
from EPMApp.admins import items
from EPMApp.admins.items import ItemAdmin, obtener_items
from EPMApp import utils
from EPMApp.models import Proyectos, Item, Usuarios, SolicitudCambio, RelacionPadre, RelacionAntecesor, Votaciones, \
    Fases, PermisosTemporales, EstadosFases, LineaBase, EstadosLineaBase, EstadosItem, EstadosSolicitud, ComiteCambio, \
    SolicitudItems, TiposVotaciones
from django.http import HttpResponseRedirect
from EPMApp import configuraciones
VOTO_APROBACION=1
VOTO_RECHAZO=2

class SolicitudInlineAdmin(admin.TabularInline):
    model = SolicitudCambio.items.through
    fields = ['items']
    list_display= ['items']

    def has_add_permission(self, request, obj=None):
       id_proyecto=None
       id_fase=None
       try:
           id_proyecto=request.session['id_proyecto']

       except:
           print("No se encontro el id")
       has_perm=utils.has_project_permission(request.user.id,id_proyecto,configuraciones.AGREGAR_SOLICITUD)
       return has_perm

    def has_change_permission(self, request, obj=None):
       id_proyecto=None
       id_fase=None
       try:
           id_proyecto=request.session['id_proyecto']
       except:
           print("No se encontro el id")
       has_perm=utils.has_project_permission(request.user.id,id_proyecto,configuraciones.LISTAR_SOLICITUDES)
       print("Has_Perm:", has_perm)
       return has_perm

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        proyecto_pk=None
        if db_field.name == "items":
            print(request)
            try:
                proyecto_pk=request.session['id_proyecto']
            except:
                print("No se pudo obtener el id del proyecto")
            if proyecto_pk:
                items=Item.objects.filter(Q(id_fase__id_proyecto=proyecto_pk) & Q(estado__id=7))
                kwargs["queryset"] =items

        return super(SolicitudInlineAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)



class SolicitudAdmin(admin.ModelAdmin):
    """Representa el administrador de Solicitudes de cambios"""
    list_display = ('nombre', 'motivo', 'fecha_creacion', 'estado')
    fields = ('nombre', 'motivo', 'proyecto', 'usuario', 'items')
    list_per_page = 13
    search_fields = ('nombre', 'motivo', 'fecha_creacion', 'estado')
    filter_horizontal = ['items']
    actions = ['confirmar_solicitud', 'aprobar_solicitud' , 'rechazar_solicitud','ver_votacion','eliminar_solicitud']


    def ver_votacion(self, request, queryset):
        """
        Despliega las lista de los miembros del comite de la solicitud de cambio con sus correspondientes votos
        @return: Lista de los usuarios miembros del comite
        @rtype: HttpResponseRedirect
        """

        if len(queryset) == 1:
            for obj in queryset:

                return HttpResponseRedirect("/admin/EPMApp/votaciones/")

        else:
            message_bi = "Seleccione una sola solicitud"
            self.message_user(request, message_bi)

    ver_votacion.short_description = "Visualizar votacion"

    def confirmar_solicitud(self, request, queryset):
        """
        Metodo que confirma una solicitud de cambio, el estado de la solicitud cambia a Pendiente
        """
        id_proyecto=request.session['id_proyecto']
        if len(queryset) == 1:
            for obj in queryset:
                has_perm = utils.has_project_permission(request.user.id, id_proyecto, configuraciones.CONFIRMAR_SOLICITUD)
                if has_perm:
                    estado = obj.estado.id

                    solicitud=SolicitudCambio.objects.get(id=obj.id)
                    print("La solicitud es: ",solicitud)
                    usuario=solicitud.usuario.id
                    print("El usuario que creo la solicitud es:",usuario)

                    print(solicitud.items.all())
                    items=solicitud.items.all()
                    usuario_actual=request.user.id
                    print("El usuario actual es: ",usuario_actual)
                    if usuario==usuario_actual:
                        if estado == 1:
                            rows_updated = queryset.update(estado=3)
                            if rows_updated == 1:
                                message_bit = "La solicitud de cambio se ha confirmado"
                            self.message_user(request, "%s exitosamente." % message_bit)
                        if estado==2:
                            message_bi = "La solicitud no puede confirmarse debido a que se encuentra Aprobado"
                            self.message_user(request, message_bi)
                        if estado==4:
                            message_bi = "La solicitud no puede confirmarse debido a que se encuentra Eliminado"
                            self.message_user(request, message_bi)
                        if estado==5:
                            message_bi = "La solicitud no puede confirmarse debido a que se encuentra en estado Rechazado"
                            self.message_user(request, message_bi)
                    else:
                        message_bi="La solicitud debe ser confirmada por el usuario que la creo"
                        self.message_user(request,message_bi)
                else:
                    message_bi = "No posee permisos para confirmar la solicitud %s" % obj
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione una sola solicitud"
            self.message_user(request, message_bi)
    confirmar_solicitud.short_description = "Confirmar Solicitud"

    def eliminar_solicitud(self, request, queryset):
        """
        Metodo que elimina una solicitud de cambio, el estado de la solicitud cambia a Eliminada
        """
        id_proyecto=request.session['id_proyecto']
        if len(queryset) == 1:
            for obj in queryset:
                has_perm = utils.has_project_permission(request.user.id, id_proyecto, configuraciones.ELIMINAR_SOLICITUD)
                if has_perm:
                    estado = obj.estado.id

                    solicitud=SolicitudCambio.objects.get(id=obj.id)
                    print("La solicitud es: ",solicitud)
                    usuario=solicitud.usuario.id
                    print("El usuario que creo la solicitud es:",usuario)

                    print(solicitud.items.all())
                    items=solicitud.items.all()
                    usuario_actual=request.user.id
                    print("El usuario actual es: ",usuario_actual)
                    if usuario==usuario_actual:
                        if estado == 1:
                            rows_updated = queryset.update(estado=4)
                            if rows_updated == 1:
                                message_bit = "La solicitud de cambio se ha Eliminado"
                            self.message_user(request, "%s exitosamente." % message_bit)
                        if estado==2:
                            message_bi = "La solicitud no puede Eliminarse debido a que se encuentra Aprobado"
                            self.message_user(request, message_bi)
                        if estado==4:
                            message_bi = "La solicitud no puede Eliminarse debido a que se encuentra Eliminado"
                            self.message_user(request, message_bi)
                        if estado==5:
                            message_bi = "La solicitud no puede Eliminarse debido a que se encuentra en estado Rechazado"
                            self.message_user(request, message_bi)
                    else:
                        message_bi="La solicitud debe ser Eliminarse por el usuario que la creo"
                        self.message_user(request,message_bi)
                else:
                    message_bi = "No posee permisos para Eliminar la solicitud %s" % obj
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione una sola solicitud"
            self.message_user(request, message_bi)
    eliminar_solicitud.short_description = "Eliminar Solicitud"

    def aprobar_solicitud(self, request, queryset):
            """
            Metodo que registra el voto de aprobacion de una solicitud de cambio
            @param: request, queryset
            """
            comite=None
            id_proyecto=request.session['id_proyecto']
            proyecto=Proyectos.objects.get(id=id_proyecto)
            miembros=proyecto.miembros.all()
            estado_sol_aprobada=EstadosSolicitud.objects.get(id=2)
            estado_sol_rechazo=EstadosSolicitud.objects.get(id=5)

            try:
             comite=ComiteCambio.objects.get(id_proyecto__id=id_proyecto)
            except:
                print("El proyecto no posee comite asociado")



            if proyecto.estado.id==2:
               # miembro=Usuarios.objects.get(id=request.user.id)
                miembro=request.user
                #se verifica que solo miembros del comite puedan votar

                if comite:
                    if miembro in comite.usuarios.all():

                        if len(queryset) == 1:
                            for obj in queryset:

                                has_perm = utils.has_project_permission(request.user.id, id_proyecto, configuraciones.PROCESAR_SOLICITUD)
                                if has_perm:
                                    estado = obj.estado.id
                                    solicitud=SolicitudCambio.objects.get(id=obj.id)
                                    items_a_modificar=solicitud.items.all()
                                    print("Items a modificar", items_a_modificar)

                                    if not solicitud.votos_aprobacion:
                                        solicitud.votos_aprobacion=0
                                    if not solicitud.votos_rechazo:
                                        solicitud.votos_rechazo=0
                                    solicitud.save()


                                    print("items de solicitud")
                                    print(solicitud.items.all())
                                    items=solicitud.items.all()

                                    if estado == 1:
                                        message_bi = "No se puede procesar la solicitud debido a que aun no ha sido confirmada"
                                        self.message_user(request, message_bi)
                                    if estado==2:
                                        message_bi = "No se puede procesar la solicitud debido a que se encuentra Aprobado"
                                        self.message_user(request, message_bi)
                                    if estado==3:
                                        votaciones=Votaciones.objects.filter(Q(solicitud__id=obj.id) & Q(miembro__id=request.user.id))
                                        if not votaciones:
                                            print("El miembro no tiene votos")
                                            voto_aprob=Votaciones(solicitud=solicitud, miembro=miembro, voto=VOTO_APROBACION)
                                            voto_aprob.save()
                                            solicitud.votos_aprobacion=solicitud.votos_aprobacion+1
                                            solicitud.save()

                                            message_bit = "El voto por la aprobacion de la solicitud se ha registrado "
                                            self.message_user(request, "%s exitosamente." % message_bit)

                                            print("Cantidad de miembros del comite ",comite.usuarios.count())
                                            #se verifica si es que todos los miembros votaron
                                            if comite.usuarios.count() == (solicitud.votos_aprobacion+solicitud.votos_rechazo) :
                                                if solicitud.votos_aprobacion>solicitud.votos_rechazo:
                                                    #se ejecuta la solicitud
                                                    solicitud.estado=estado_sol_aprobada
                                                    solicitud.save()
                                                    ejecutar_solicitud(items_a_modificar, solicitud, miembro)

                                                    message_bit = "La solicitud de cambio ha sido aprobada por mayoria de votos "
                                                    self.message_user(request, "%s exitosamente." % message_bit)
                                                else:
                                                    #se ejecuta la solicitud
                                                    solicitud.estado=estado_sol_rechazo
                                                    solicitud.save()
                                                    message_bit = "La solicitud de cambio ha sido rechazada por mayoria de votos "
                                                    self.message_user(request, "%s exitosamente." % message_bit)
                                            else:
                                                print("Aun no han votado todos los miembros")
                                        else:
                                             message_bit = "No se pudo procesar la solicitud debido a que ya pose un voto registrado"
                                             self.message_user(request, "%s." % message_bit)

                                    if estado==4:
                                        message_bi = "No se puede procesar la solicitud debido a que se encuentra Eliminado"
                                        self.message_user(request, message_bi)
                                    if estado==5:
                                        message_bi = "No se puede procesar la solicitud debido a que se encuentra en estado Rechazado"
                                        self.message_user(request, message_bi)
                                else:
                                    message_bi = "No posee permisos para aprobar la solicitud %s" % obj
                                    self.message_user(request, message_bi)
                        else:
                            message_bi = "Seleccione una sola solicitud"
                            self.message_user(request, message_bi)
                    else:
                        message_bi = "No se pudo realizar la votacion debido a que solo miembros del comite de cambio del proyecto %s pueden realizar la votacion" %proyecto
                        self.message_user(request, message_bi)
                else:
                    message_bi = "No se pudo realizar la votacion debido a que el proyecto %s no posee un comite de cambio asignado." %proyecto
                    self.message_user(request, message_bi)
            else:
                message_bi = "No se pudo realizar la votacion debido a que el proyecto %s no se encuentra en estado iniciado." %proyecto
                self.message_user(request, message_bi)
    aprobar_solicitud.short_description = "Votar por aprobacion"

    def rechazar_solicitud(self, request, queryset):
            """
            Metodo que registra el voto de rechazo de una solicitud de cambio
            @param: request, queryset
            """
            comite=None
            id_proyecto=request.session['id_proyecto']
            proyecto=Proyectos.objects.get(id=id_proyecto)
            miembros=proyecto.miembros.all()
            estado_sol_aprobada=EstadosSolicitud.objects.get(id=2)
            estado_sol_rechazo=EstadosSolicitud.objects.get(id=5)
            try:
             comite=ComiteCambio.objects.get(id_proyecto__id=id_proyecto)
            except:
                print("El proyecto no posee comite asociado")




           # miembro=Usuarios.objects.get(id=request.user.id)
            miembro=request.user
            #se verifica que solo miembros del comite puedan votar

            if comite:
                if miembro in comite.usuarios.all():

                    if len(queryset) == 1:
                        for obj in queryset:

                            has_perm = utils.has_project_permission(request.user.id, id_proyecto, configuraciones.PROCESAR_SOLICITUD)
                            if has_perm:
                                estado = obj.estado.id
                                solicitud=SolicitudCambio.objects.get(id=obj.id)
                                items_a_modificar=solicitud.items.all()
                                print("Items a modificar", items_a_modificar)

                                if not solicitud.votos_aprobacion:
                                    solicitud.votos_aprobacion=0
                                if not solicitud.votos_rechazo:
                                    solicitud.votos_rechazo=0
                                solicitud.save()


                                print("items de solicitud")
                                print(solicitud.items.all())
                                items=solicitud.items.all()

                                if estado == 1:
                                    message_bi = "No se puede procesar la solicitud debido a que aun no ha sido confirmada"
                                    self.message_user(request, message_bi)
                                if estado==2:
                                    message_bi = "No se puede procesar la solicitud debido a que se encuentra Aprobado"
                                    self.message_user(request, message_bi)
                                if estado==3:
                                    votaciones=Votaciones.objects.filter(Q(solicitud__id=obj.id) & Q(miembro__id=request.user.id))
                                    if not votaciones:
                                        print("El miembro no tiene votos")
                                        voto_aprob=Votaciones(solicitud=solicitud, miembro=miembro, voto=VOTO_APROBACION)
                                        voto_aprob.save()
                                        solicitud.votos_rechazo=solicitud.votos_rechazo+1
                                        solicitud.save()
                                        message_bit = "El voto por el rechazo de la solicitud se ha registrado "
                                        self.message_user(request, "%s exitosamente." % message_bit)
                                        print("Cantidad de miembros del comite ",comite.usuarios.count())
                                        #se verifica si es que todos los miembros votaron
                                        if comite.usuarios.count() == (solicitud.votos_aprobacion+solicitud.votos_rechazo) :
                                            if solicitud.votos_aprobacion>solicitud.votos_rechazo:
                                                #se ejecuta la solicitud
                                                solicitud.estado=estado_sol_aprobada
                                                solicitud.save()
                                                ejecutar_solicitud(items_a_modificar, solicitud, miembro)
                                                message_bit = "La solicitud de cambio ha sido aprobada por mayoria de votos "
                                                self.message_user(request, "%s exitosamente." % message_bit)
                                            else:
                                                #se ejecuta la solicitud
                                                solicitud.estado=estado_sol_rechazo
                                                solicitud.save()
                                                message_bit = "La solicitud de cambio ha sido rechazada por mayoria de votos "
                                                self.message_user(request, "%s exitosamente." % message_bit)
                                        else:
                                            print("Aun no han votado todos los miembros")

                                    else:
                                        message_bit = "No se pudo procesar la solicitud debido a que ya pose un voto registrado"
                                        self.message_user(request, "%s." % message_bit)


                                if estado==4:
                                    message_bi = "No se puede procesar la solicitud debido a que se encuentra Eliminado"
                                    self.message_user(request, message_bi)
                                if estado==5:
                                    message_bi = "No se puede procesar la solicitud debido a que se encuentra en estado Rechazado"
                                    self.message_user(request, message_bi)
                            else:
                                message_bi = "No posee permisos para rechazar la solicitud %s" % obj
                                self.message_user(request, message_bi)
                    else:
                        message_bi = "Seleccione una sola solicitud"
                        self.message_user(request, message_bi)
                else:
                    message_bi = "No se pudo realizar la votacion debido a que solo miembros del comite de cambio del proyecto %s pueden realizar la votacion" %proyecto
                    self.message_user(request, message_bi)
            else:
                message_bi = "No se pudo realizar la votacion debido a que el proyecto %s no posee un comite de cambio asignado." %proyecto
                self.message_user(request, message_bi)
    rechazar_solicitud.short_description = "Votar por rechazo"

    def get_readonly_fields(self, request, obj=None):
        """Deshabilita los campos a modificar si una solicitud de cambio ya ha sido confirmada
        @param: request, obj=None
        @xtype:
        """
        proyecto_pk= request.session['id_proyecto']
        p=Proyectos.objects.get(id=proyecto_pk)
        estado_proyecto=p.estado_id

        request.session['estado_proyecto'] = estado_proyecto


        if obj == None:
            has_perm = utils.has_project_permission(request.user.id, proyecto_pk, configuraciones.MODIFICAR_SOLICITUD_CAMBIO)
            if has_perm:
                request.session['estado_solicitud']=None
                return  ('')
            else:
                message_bi = "No posee permisos para Crear Solicitud de Cambio"
                self.message_user(request, message_bi)

        if obj != None:
            estado = obj.estado.id
            print('estado')
            print(estado)
            if estado==1:
                if request.user.id == obj.usuario_id:
                    has_perm = utils.has_project_permission(request.user.id, proyecto_pk, configuraciones.MODIFICAR_SOLICITUD_CAMBIO)
                    if has_perm:
                        request.session['estado_solicitud']=estado
                        return  ('')
                    else:
                        message_bi = "No posee permisos para Modificar Solicitud de Cambio"
                        self.message_user(request, message_bi)
                        request.session['estado_solicitud']='sin_permiso'
                        return ('nombre', 'motivo', 'proyecto', 'usuario', 'items')
                else:
                    message_bi = "No posee permisos para Modificar Solicitud de Cambio"
                    self.message_user(request, message_bi)
                    request.session['estado_solicitud']='sin_permiso'
                    return ('nombre', 'motivo', 'proyecto', 'usuario', 'items')
            else:
                request.session['estado_solicitud']=estado
                print (request.session['estado_solicitud'])
                return ('nombre', 'motivo', 'proyecto', 'usuario', 'items')

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        """Realiza el filtrado de los usuarios del proyecto actual"""

        if db_field.name == "items":
            print(request)
            try:
                proyecto_pk=request.session['id_proyecto']
            except:
                print("No se pudo obtener el id del proyecto")
            if proyecto_pk:
                items=Item.objects.filter(Q(id_fase__id_proyecto=proyecto_pk) & Q(estado__id=7))
                kwargs["queryset"] =items

        return super(SolicitudAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)


    def save_related(self,request, form, formsets, change):
        if change:
            super(SolicitudAdmin, self).save_related(request, form, formsets, change)
            # items_sol = form.cleaned_data.pop('items', ())
            # solicitud = form.instance
            # for item in items_sol:
            #    impacto=items.obtener_impacto(item)
            #    SolicitudItems.objects.create(solicitud=solicitud, items=item, impacto=impacto)
        else:

            items_sol = form.cleaned_data.pop('items', ())
            solicitud = form.instance
            for item in items_sol:
               impacto=items.obtener_impacto(item)
               SolicitudItems.objects.create(solicitud=solicitud, items=item, impacto=impacto)
            super(SolicitudAdmin, self).save_related(request, form, formsets, change)






    def add_solicitud(self, request, queryset):
            """
             Metodo que despliega el formulario de creacion de solicitudes de cambio
             @param: request,queryset
            @xtype:
             """
            if len(queryset)==1:
                for obj in queryset:
                    estado= obj.estado.id

                    proyecto_pk= request.session['id_proyecto']
                    p=Proyectos.objects.get(id=proyecto_pk)
                    estado_proyecto=p.estado_id

                    if estado_proyecto==1:
                        message_bi = "No se pueden agregar solicitudes de cambio al proyecto %s ya que aun no ha Iniciado"%obj
                        self.message_user(request, message_bi)
                    if estado_proyecto==2:
                        has_perm = utils.has_project_permission(request.user.id, obj.id, configuraciones.LISTAR_SOLICITUDES)
                        if has_perm:
                            request.session['id_proyecto'] = obj.id
                            return HttpResponseRedirect("/admin/EPMApp/solicitudcambio/?proyecto=%s" % obj.id)
                        else:
                            message_bi = "No posee permisos para visualizar las solicitudes de cambio en el proyecto %s"%obj
                            self.message_user(request, message_bi)
                    if estado_proyecto==3:
                        message_bi = "No se pueden agregar solicitudes de cambio al proyecto %s ya que ha sido cancelado"%obj
                        self.message_user(request, message_bi)
                    if estado_proyecto==4:
                        message_bi = "No se pueden agregar solicitudes de cambio al proyecto %s ya que ha sido finalizado"%obj
                        self.message_user(request, message_bi)
                    if estado_proyecto==5:
                        message_bi = "No se pueden agregar solicitudes de cambio al proyecto %s ya que aun no ha eliminado"%obj
                        self.message_user(request, message_bi)
            else:
                message_bi = "Seleccione un solo tipo item"
                self.message_user(request, message_bi)
    add_solicitud.short_description = "Agregar"

    # def save_model(self, request, obj, form, change):
    #         """
    #         Guarda los atributos del item creado
    #         @param: request,obj,form,change
    #         @xtype:
    #         """
    #         #print(obj)
    #         solicitud=obj.id
    #
    #         proyecto_pk=request.session['id_proyecto']
    #         proyecto=Proyectos.objects.get(id=proyecto_pk)
    #         obj.proyecto=proyecto
    #
    #         usuario=Usuarios.objects.get(id=request.user.id)
    #         obj.usuario=usuario
    #
    #         obj.save()
    #         super(SolicitudAdmin, self).save_model(request, obj, form, change)
    #         solicitud=SolicitudCambio.objects.get(id=obj.id)
    #         print("items")
    #         print(solicitud.id)
    #         items=obj.items.all()
    #         #items= form.cleaned_data['items']
    #         print(items)
    #         for i in items:
    #             print( "impacto")
    #             #print(obtener_impacto(i))
    #             #itemssol=items.solicitudcambios_set(id=solicitud.id)
    #             impacto=obtener_impacto(self,i)
    #             print("Impacto: ",impacto)
    #             #i.through.impacto=impacto
    #             i.save()
    #              #solicitud.items.impacto=impacto
    #              #print("Solicitud: ",solicitud.impacto)
                 



    def response_post_save_add(self, request, obj):
        """Metodo que sobre-escribe el metodo que redirige despues de guardar una una solicitud de cambio
        @param: request, obj
        @xtype: SolicitudAdmin
        @return: URL que redirecciona al listado de solicitud de cambio
        @rtype: HttpResponseRedirect
        """
        proyecto_pk= request.session['id_proyecto']
        return HttpResponseRedirect("/admin/EPMApp/solicitudcambio/?proyecto=%s" %proyecto_pk)

    def get_changelist(self, request, **kwargs):
        """
        @param: request, **kwargs
        @xtype: SolicitudAdmin,
        @return: SolicitudChangeList
        @rtype: ChangeList
        """
        return SolicitudChangeList


    def has_add_permission(self, request, obj=None):
       id_proyecto=None
       id_fase=None
       try:
           id_proyecto=request.session['id_proyecto']

       except:
           print("No se encontro el id")
       has_perm=utils.has_project_permission(request.user.id,id_proyecto,configuraciones.AGREGAR_SOLICITUD)
       return has_perm

    def has_change_permission(self, request, obj=None):
       id_proyecto=None
       id_fase=None
       try:
           id_proyecto=request.session['id_proyecto']
       except:
           print("No se encontro el id")
       has_perm=utils.has_project_permission(request.user.id,id_proyecto,configuraciones.LISTAR_SOLICITUDES)
       print("Has_Perm:", has_perm)
       return has_perm

    # def save_related(self, request,obj, form, change):
    #     if not change:
    #         print("save realted")
    #         items_sol = form.cleaned_data.pop('items', ())
    #         solicitud = form.instance
    #         for item in items_sol:
    #             impacto=items.obtener_impacto(item)
    #             SolicitudItems.objects.create(solicitud=solicitud, items=item, impacto=impacto)
    #     super(SolicitudAdmin, self).save_related(request,obj, form, change)




class SolicitudChangeList(ChangeList):
    """Representa el administrador de Linea Base
    @param: ChangeList
    @xtype: SolicitudChangeList
    """
    def __init__(self, *args, **kwargs):
        """
        @param: request, **kwargs
        @xtype: SolicitudChangeList
        """
        super(SolicitudChangeList, self).__init__(*args, **kwargs)
        self.title = "Administracion de Solicitudes de cambio"


def ejecutar_solicitud(items, solicitud, miembro):
    permiso_modif_item=Permission.objects.get(codename='change_item')
    estado_item_desa_restringido=EstadosItem.objects.get(id=2)
    estado_desarr_restringido=EstadosFases.objects.get(id=6)
    estado_fase_comprometido=EstadosFases.objects.get(id=5)
    #estado_eliminado=EstadosLineaBase.objects.get(id=2)
    estado_lb_abierto=EstadosLineaBase.objects.get(id=4)
    estado_lb_comprometida=EstadosLineaBase.objects.get(id=3)
    estado_item_revision=EstadosItem.objects.get(id=8)
    linea_base=None
    print("items de la solicitud a ejecutar", items)

    for i in items:
        print("i.id", i.id)
        #Se le asigna un permiso temporal al usuario por cada item de la solicitud
        permiso_temp=PermisosTemporales(solicitud=solicitud, miembro=solicitud.usuario, item=i, permiso=permiso_modif_item)
        permiso_temp.save()
        fase=Fases.objects.get(id=i.id_fase.id)

        #si la fase esta finalizada se abre temporalmente
        if fase.estado.id==4 or fase.estado.id==5:
            fase.estado=estado_desarr_restringido
            fase.save()

        #la linea base a la que pertenece el item se elimina y se crea una copia temporal
        try:
            linea_base=LineaBase.objects.get(Q(items__id=i.id))
            linea_base.estado=estado_lb_abierto
            linea_base.save()
        except:
            print("la linea base a la que pertenece el item de la solicitud ya estaba eliminada")


        #se pone en revision todos los items de la linea base
        items_lb=linea_base.items.all()
        print("Items de la linea base", items_lb)
        estado_item_revision=EstadosItem.objects.get(id=8)
        for ilb in items_lb: #itera por cada item de la linea base
            if not ilb in items: #si el item de la lb no esta en  la solicitud
                if not ilb.id==i.id: #si el id del item de la lb no es igual
                    ilb.estado=estado_item_revision
                    ilb.save()





        #se pone en revision los antecesores y sucesores
        items_relacionados=obtener_items_relacionados(i)

        for item_rel in items_relacionados:

            item=Item.objects.get(pk=item_rel) # obtiene el item
            if not item in items:
                item.estado=estado_item_revision
                item.save()

                linea_base_rel=LineaBase.objects.get(items=item_rel)

                #se compromete la linea base si aplica
                if linea_base_rel.estado.id==1:
                    linea_base_rel.estado=estado_lb_comprometida
                    linea_base_rel.save()
                #se compromete la fase si aplica

                fase_rel=Fases.objects.get(id=linea_base_rel.id_fase.id)
                if fase_rel.estado.id==4:
                    fase_rel.estado=estado_fase_comprometido
                    fase_rel.save()



        #se dispone el estado de modificacion del item
        i.estado=estado_item_desa_restringido
        i.save()





def obtener_items_relacionados (item):
    items=obtener_items(item) # vector que almacena los items relacionados
    return items


