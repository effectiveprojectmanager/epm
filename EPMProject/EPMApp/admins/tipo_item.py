from django.contrib import admin
from django.contrib.admin.views.main import ChangeList
from django.db.models import Q

from django.http import HttpResponseRedirect
from EPMApp import configuraciones, utils
from EPMApp.admins import atributos

from EPMApp.models import Fases, TiposItems, Item, Proyectos, Atributos,RolesProyectos


SESSION_IMPORT_TIPO=3

class TipoItemAdmin(admin.TabularInline):
    """Representa el administrador de tipo de item"""
    fields = ('nombre', 'descripcion' )
    model = TiposItems
    extra = 0


class TiposItemsAdmin(admin.ModelAdmin):
    """
    Representa el administrador de Tipos de Items
    """
    fields=('nombre', 'descripcion','id_fase', 'atributos')
    list_display = ('nombre','descripcion','fecha_creacion', 'estado')
    filter_horizontal = ['atributos']

    actions = ['eliminar_tipo_item','ver_importar']

    #Importar tipo de item
    def ver_importar(self, request, queryset):
        """
        Representa la relacion Antecesor-Sucesor
        @param: request,queryset
        @xtype:
        """
        for obj in queryset:
            has_perm = utils.has_fase_permission(request.user.id, request.session['id_proyecto'], configuraciones.IMPORTAR_TIPO_ITEM, request.session['id_fase_actual'] )
            if has_perm:
                fase_pk=request.session['id_fase_actual']
                tipo_item=TiposItems.objects.get(pk=obj.id)
                atributos=tipo_item.atributos.all()
                #print('Atributos')
                #print(atributos)
                if tipo_item:
                    tipo_item_nuevo=TiposItems(nombre=tipo_item.nombre,descripcion=tipo_item.descripcion)
                    tipo_item_nuevo.id_fase=Fases.objects.get(pk=fase_pk)
                    tipo_item_nuevo.save()
                    tipo_item_nuevo.atributos=atributos
                    tipo_item_nuevo.save()
                    message_bit = "Se ha importado el tipo de item"
                    self.message_user(request, "%s exitosamente." % message_bit)
                else:
                    message_bi = "No se ha podido importar"
                    self.message_user(request, message_bi)
            else:
                message_bi = "No posee permisos para Importar Tipos de Items"
                self.message_user(request, message_bi)

    ver_importar.short_description = "Importar Tipo de Item"

    #Eliminar Tipo de Item
    def eliminar_tipo_item(self, request, queryset):
        """
         Metodo que inactiva el Tipo de Item seleccionado
         @param: request,queryset
        @xtype:
         """
        if len(queryset)==1:
            for obj in queryset:
                estado= obj.estado.id

                proyecto_pk= request.session['id_proyecto']
                p=Proyectos.objects.get(id=proyecto_pk)
                estado_proyecto=p.estado_id

                fase_pk=request.session['id_fase']
                f=Fases.objects.get(id=fase_pk)
                estado_fase=f.estado_id

                has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.ELIMINAR_TIPO_ITEM, fase_pk)
                if has_perm:
                    if estado==2:
                        message_bit = "El Tipo de Item no se puede Eliminar ya que se ha Eliminado previamente"
                        self.message_user(request, message_bit)
                    if estado_proyecto==1:
                        message_bi = "El tipo item no puede Eliminarse ya que el Proyecto no ha Iniciado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==2:
                        if estado_fase==1:
                            message_bi = "El tipo item no puede Eliminarse ya que la Fase no ha Iniciado"
                            self.message_user(request, message_bi)
                        if estado_fase==2:
                            message_bi = "El tipo item no puede Eliminarse ya que la Fase se ha Eliminado"
                            self.message_user(request, message_bi)
                        if estado_fase==3:
                            if estado==1:
                                tipo_item_pk= obj.id
                                items=Item.objects.filter(tipo_item=tipo_item_pk)
                                if items:
                                    message_bi = "El tipo item no puede Eliminarse ya que se crearon Items de este tipo"
                                    self.message_user(request, message_bi)
                                else:
                                    rows_updated = queryset.update(estado=2)
                                    if rows_updated == 1:
                                        message_bit = "El Tipo de Item se ha Eliminado"
                                    self.message_user(request, "%s exitosamente." % message_bit)
                            #if estado==2:
                                #message_bit = "El Tipo de Item no se puede Eliminar ya que se ha Eliminado previamente"
                                #self.message_user(request, message_bit)
                        if estado_fase==4:
                            message_bi = "El tipo item no puede Eliminarse ya que la Fase ha Finalizado"
                            self.message_user(request, message_bi)
                        if estado_fase==5:
                            message_bi = "El tipo item no puede Eliminarse ya que la Fase se encuentra Comprometida"
                            self.message_user(request, message_bi)
                        if estado_fase==6:
                            message_bi = "El tipo item no puede Eliminarse ya que la Fase se encuentra en Desarrollo Restringido"
                            self.message_user(request, message_bi)
                    if estado_proyecto==3:
                        message_bi = "El tipo item no puede Eliminarse ya que el Proyecto se ha Cancelado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==4:
                        message_bi = "El tipo item no puede Eliminarse ya que el Proyecto ha Finalizado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==5:
                        message_bi = "El tipo item no puede Eliminarse ya que el Proyecto se ha Eliminado"
                        self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para Eliminar Tipo de Item"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo tipo item"
            self.message_user(request, message_bi)
    eliminar_tipo_item.short_description = "Eliminar Tipo de Item"

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """Despliega la fase en el cual se desea agregar un Item
        @param: db_field, request, **kwargs
        @xtype: ItemAdmin
        """
        if db_field.name=="id_fase":
            parametros=request.session['id_fase']
            if parametros:
                fases_id= parametros
                f=Fases.objects.filter(id=fases_id)
                kwargs["queryset"]=f.filter()

        return super(TiposItemsAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        """Retorna una lista de items para formar parte de la linea base
        @param: db_field, request, **kwargs
        @xtype: LineaBaseAdmin,
        """
        if db_field.name == "atributos":

            a = Atributos.objects.filter(estado=1)
            print(a)
            kwargs["queryset"] = a.filter()
            print(kwargs)
        return super(TiposItemsAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)

    def response_post_save_add(self, request, obj):
        """
        Redirige a la lista de tipos de items de una fase despues de crear un tipo de item
        @param: request,obj
        @xtype:
        @return: URL que redirecciona a la lista de tipos de items de una fase, luego de crear un tipo de item
        @rtype: HttpResponseRedirect
        """
        fase_pk=request.session['id_fase']
        return HttpResponseRedirect("/admin/EPMApp/tipositems/?id_fase=%s" %fase_pk)

    def get_readonly_fields(self, request, obj=None):
        """Deshabilita los campos a modificar si un proyecto  ya no se encuentra en estado Iniciado
        o la fase ya no se encuentra en estado de Desarrollo
        @param: request, obj=None
        @xtype:
        """
        proyecto_pk= request.session['id_proyecto']
        p=Proyectos.objects.get(id=proyecto_pk)
        estado_proyecto=p.estado_id

        fase_pk=request.session['id_fase']
        f=Fases.objects.get(id=fase_pk)
        estado_fase=f.estado_id

        #cuando se crea
        if obj == None:
            has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.AGREGAR_TIPO_ITEMS, fase_pk)
            if has_perm:
                request.session['estado_tipo_item']=None
                return  ('')
            else:
                message_bi = "No posee permisos para Crear un Tipo de Item"
                self.message_user(request, message_bi)

        #cuando existe un tipo de item
        if obj != None:
            estado = obj.estado.id

            #Estados del proyecto
            if estado_proyecto==1:
                return ('nombre', 'descripcion','id_fase', 'atributos')
            if estado_proyecto==2:
                #Estados de la fase
                if estado_fase==1:
                    return ('nombre', 'descripcion','id_fase', 'atributos')
                if estado_fase==2 :
                    return ('nombre', 'descripcion','id_fase', 'atributos')
                if estado_fase==3:
                     #Estado del tipo de item
                    if estado==1:
                        tipo_item_pk= obj.id
                        items=Item.objects.filter(tipo_item=tipo_item_pk)
                        if items:
                            request.session['estado_tipo_item']='USADO'
                            return ('nombre', 'descripcion','id_fase', 'atributos')
                        else:
                            has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.MODIFICAR_TIPO_ITEM, fase_pk)
                            if has_perm:
                                request.session['estado_tipo_item']=estado
                                return  ('')
                            else:
                                message_bi = "No posee permisos para Modificar un Tipo de Item"
                                self.message_user(request, message_bi)
                                request.session['estado_tipo_item']='sin_permiso'
                                return ('nombre', 'descripcion','id_fase', 'atributos')
                    if estado==2:
                        request.session['estado_tipo_item']=estado
                        return ('nombre', 'descripcion','id_fase', 'atributos')
                if estado_fase==4:
                    return ('nombre', 'descripcion','id_fase', 'atributos')
                if estado_fase==5:
                    return ('nombre', 'descripcion','id_fase', 'atributos')
            if estado_proyecto==3:
                return ('nombre', 'descripcion','id_fase', 'atributos')
            if estado_proyecto==4:
                return ('nombre', 'descripcion','id_fase', 'atributos')
            if estado_proyecto==5:
                return ('nombre', 'descripcion','id_fase', 'atributos')

    def get_changelist(self, request, **kwargs):
        """
        Configuraciones de la lista principal de Tipo de Item
        @param: request,**kwargs
        @xtype:
        @return Lista de tipo de item
        """
        return TipoItemChangeList


    def get_actions(self, request):
        """
        Desactiva las acciones predeterminadas del sistema de la Administracion de Tipos de Items
        @param: request
        @xtype:
        @return: Lista de acciones
        @rtype: actions
        """
        b=0
        try:
            b=request.session['proceso']
        except:
            print("No pudo obtenerse el id de proceso")
        actions = super(TiposItemsAdmin, self).get_actions(request)
        if b:
            if b==SESSION_IMPORT_TIPO:
                del actions['eliminar_tipo_item']

        else:
            del actions['ver_importar']

        return actions

    def has_add_permission(self, request, obj=None):
        id_proyecto=None
        id_fase=None
        try:
            id_proyecto=request.session['id_proyecto']
            id_fase=request.session['id_fase']
        except:
            print("No se encontro el id")
        has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.AGREGAR_TIPO_ITEMS,id_fase)

        return has_perm

    def has_change_permission(self, request, obj=None):
        id_proyecto=None
        id_fase=None
        try:
            id_proyecto=request.session['id_proyecto']
            id_fase=request.session['id_fase']
        except:
            print("No se encontro el id")
        has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.LISTAR_TIPO_ITEM,id_fase)
        return has_perm

class TipoItemChangeList(ChangeList):
    """
    Clase que administra la lista de cambio de tipo de item
    """
    def __init__(self, *args, **kwargs):
        super(TipoItemChangeList, self).__init__(*args, **kwargs)
        self.title = "Seleccione un Tipo de Item y la operacion a realizar"

