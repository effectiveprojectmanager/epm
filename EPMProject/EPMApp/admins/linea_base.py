from django.contrib.admin.views.main import ChangeList
from django.contrib import admin
from django.http import HttpResponseRedirect
from EPMApp.models import EstadosItem
from EPMApp.models import Fases
from EPMApp.models import Proyectos
from EPMApp import utils
from EPMApp import configuraciones


class LineaBaseAdmin(admin.ModelAdmin):
    """Representa el administrador de Linea Base"""
    list_display = ('nombre', 'fecha_creacion','estado')
    fields = ('nombre', 'id_fase', 'items')
    filter_horizontal = ['items']
    list_per_page = 13
    search_fields = ('nombre', 'fecha_creacion', 'estado')

    def has_delete_permission(self, request, obj=None):
        """Permiso denegado para delete
        @param: request, obj=None
        @xtype: LineaBaseAdmin
        @return: False
        @rtype: Boolean
        """
        return False

    def save_model(self, request, obj, form, change):
        """Metodo que sobre-escribe el metodo de guardar de linea base
        @param: request, obj, form, change
        @xtype: LineaBaseAdmin,
        """
        obj.save()
        items = form.cleaned_data['items']
        for i in items:
            i.estado = EstadosItem.objects.get(pk=7)
            i.save()

    def response_post_save_add(self, request, obj):
        """Metodo que sobre-escribe el metodo que redirige despues de guardar una linea base
        @param: request, obj
        @xtype: LineaBaseAdmin,
        @return: URL que redirecciona al listado de lineas base
        @rtype: HttpResponseRedirect
        """
        fase_pk=request.session['id_fase']
        return HttpResponseRedirect("/admin/EPMApp/lineabase/?id_fase=%s" %fase_pk)

    def get_readonly_fields(self, request, obj=None):
        """Deshabilita el modificar los campos
        @param: request, obj=None
        @xtype: LineaBaseAdmin,
        """
        proyecto_pk= request.session['id_proyecto']
        p=Proyectos.objects.get(id=proyecto_pk)
        estado_proyecto=p.estado_id

        fase_pk=request.session['id_fase']
        f=Fases.objects.get(id=fase_pk)
        estado_fase=f.estado_id

        if obj == None:
            has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.AGREGAR_LINEA_BASE, fase_pk)
            if has_perm:
                return  ('')
            else:
                message_bi = "No posee permisos para Crear Linea Base"
                self.message_user(request, message_bi)
        else:
            return ('nombre', 'id_fase', 'items')

    def get_changelist(self, request, **kwargs):
        """
        @param: request, **kwargs
        @xtype: LineaBaseAdmin,
        @return: LineaBaseChangeList
        @rtype: ChangeList
        """
        return LineaBaseChangeList

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        """Retorna una lista de items para formar parte de la linea base
        @param: db_field, request, **kwargs
        @xtype: LineaBaseAdmin,
        """
        if db_field.name == "items":
            parametros = request.session['id_fase']
            if parametros:
                fases_pk = parametros
                p = Fases.objects.get(id=fases_pk)
                kwargs["queryset"] = p.item_set.filter(estado=4)
            return super(LineaBaseAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """Despliega la fase en el cual se desea agregar un Item
        @param: db_field, request, **kwargs
        @xtype: ItemAdmin
        """
        if db_field.name=="id_fase":
            parametros=request.session['id_fase']
            if parametros:
                fases_id= parametros
                f=Fases.objects.filter(id=fases_id)
                kwargs["queryset"]=f.filter()

        return super(LineaBaseAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


    def has_add_permission(self, request, obj=None):
       id_proyecto=None
       id_fase=None
       try:
           id_proyecto=request.session['id_proyecto']
           id_fase=request.session['id_fase']
       except:
           print("No se encontro el id")
       has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.AGREGAR_LINEA_BASE,id_fase)
       return has_perm

    def has_change_permission(self, request, obj=None):
        id_proyecto=None
        id_fase=None
        try:
            id_proyecto=request.session['id_proyecto']
            id_fase=request.session['id_fase']
        except:
            print("No se encontro el id")
        has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.LISTAR_LINEA_BASE,id_fase)
        return has_perm


class LineaBaseChangeList(ChangeList):
    """Representa el administrador de Linea Base
    @param: ChangeList
    @xtype: LineaBaseChangeList
    """
    def __init__(self, *args, **kwargs):
        """
        @param: request, **kwargs
        @xtype: LineaBaseChangeList
        """
        super(LineaBaseChangeList, self).__init__(*args, **kwargs)
        self.title = "Administracion de Lineas Bases"
