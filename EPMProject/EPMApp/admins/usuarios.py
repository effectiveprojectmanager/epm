from django.contrib.admin import helpers

from django.db import transaction
from django.conf import settings
from django.contrib import admin
from django.contrib.auth.forms import (UserCreationForm, AdminPasswordChangeForm)
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext, ugettext_lazy as _
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.template.defaultfilters import escape
from EPMApp import utils
from EPMApp import configuraciones

from EPMApp.models import Usuarios


csrf_protect_m = method_decorator(csrf_protect)
sensitive_post_parameters_m = method_decorator(sensitive_post_parameters())


class UserCreationForm(forms.ModelForm):
    """Formulario para creacion de usuario."""

    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmar password', widget=forms.PasswordInput)

    class Meta:
        model = Usuarios
        fields = ('username', 'email', 'fecha_nacimiento')

    def clean_password2(self):
        """Comprueba la igualdad de password."""
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords no coinciden")
        return password2

    def save(self, commit=True):
        """
        Guarda el password ingresado.
        @return: Retorna el usuario cuyo password fue modificado
        """

        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """
    Formulario para modificacion de usuario. .
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = Usuarios

    def clean_password(self):
        return self.initial["password"]


class UserAdmin(admin.ModelAdmin):
    """
    Clase que representa al administrador de usuarios
    """
    add_form_template = 'admin/auth/user/add_form.html'
    change_user_password_template = None
    filter_horizontal = ['groups']
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('nombres', 'apellidos', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'is_admin', 'groups',
        )}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined', 'fecha_nacimiento')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2', 'is_active', 'is_staff', 'date_joined')},

        ),
    )
    form = UserChangeForm
    add_form = UserCreationForm
    change_password_form = AdminPasswordChangeForm
    list_display = ('username', 'email', 'nombres', 'apellidos', 'documento', 'direccion', 'is_staff', 'date_joined',)
    list_filter = ('is_staff', 'is_superuser', 'is_active')
    search_fields = ('username', 'nombres', 'apellidos', 'email')
    ordering = ('username',)
    actions = ['ver_roles']

    def ver_roles(self, request, queryset):
        """
        Visualiza la lista de roles a nivel proyecto que posee el usuario
        @return: URL que redirecciona al listado de roles del proyecto
        @rtype: HttpResponseRedirect
        """
        id_proyecto=None
        #id_fase=None
        try:
           id_proyecto=request.session['id_proyecto']
        except:
            print("No se encontro el id")

        if len(queryset) == 1:
            for obj in queryset:
                if request.user.is_superuser:
                    has_perm = True
                else:
                    has_perm = utils.has_project_permission(request.user.id, id_proyecto, configuraciones.LISTAR_MIEMBROS_PROY)
                if has_perm:
                    return HttpResponseRedirect("/admin/EPMApp/rolesproyectos/?usuarios=%s" % obj.id)
                else:
                    message_bi = "No posee permisos para listar los roles del usuario %s" % obj
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo usuario"
            self.message_user(request, message_bi)

    ver_roles.short_description = "Roles asociados"

    # def get_queryset(self, request):
    #    qs = super(UserAdmin, self).get_queryset(request)
    #    #proceso=request.session['proceso']
    #    if proceso:
    #        if proceso==3:
    #            proyecto_pk=request.session['id_proyecto']
    #            if proyecto_pk:
    #                p=Proyectos.objects.get(id=proyecto_pk)
    #                qs= p.miembros.all()
    #    return qs

    def get_fieldsets(self, request, obj=None):
        if not obj:
            return self.add_fieldsets
        return super(UserAdmin, self).get_fieldsets(request, obj)

    def get_form(self, request, obj=None, **kwargs):

        defaults = {}
        if obj is None:
            defaults.update({
                'form': self.add_form,
                'fields': admin.util.flatten_fieldsets(self.add_fieldsets),
            })
        defaults.update(kwargs)
        return super(UserAdmin, self).get_form(request, obj, **defaults)

    def get_urls(self):
        from django.conf.urls import patterns

        return patterns('',
                        (r'^(\d+)/password/$',
                         self.admin_site.admin_view(self.user_change_password))
        ) + super(UserAdmin, self).get_urls()

    def lookup_allowed(self, lookup, value):

        if lookup.startswith('password'):
            return False

        return super(UserAdmin, self).lookup_allowed(lookup, value)

    @sensitive_post_parameters_m
    @csrf_protect_m
    @transaction.commit_on_success
    def add_view(self, request, form_url='', extra_context=None):

        if not self.has_change_permission(request):
            if self.has_add_permission(request) and settings.DEBUG:
                raise Http404(
                    'Your user does not have the "Change user" permission. In '
                    'order to add users, Django requires that your user '

                    'account have both the "Add user" and "Change user" '
                    'permissions set.')
            raise PermissionDenied
        if extra_context is None:
            extra_context = {}
        username_field = self.model._meta.get_field(self.model.USERNAME_FIELD)
        defaults = {
            'auto_populated_fields': (),
            'username_help_text': username_field.help_text,
        }
        extra_context.update(defaults)
        return super(UserAdmin, self).add_view(request, form_url,
                                               extra_context)

    @sensitive_post_parameters_m
    def user_change_password(self, request, id, form_url=''):
        if not self.has_change_permission(request):
            raise PermissionDenied
        user = get_object_or_404(self.queryset(request), pk=id)
        if request.method == 'POST':

            form = self.change_password_form(user, request.POST)
            if form.is_valid():
                form.save()
                msg = ugettext('Password cambiado correctamente.')
                messages.success(request, msg)
                return HttpResponseRedirect('..')
        else:
            form = self.change_password_form(user)

        fieldsets = [(None, {'fields': list(form.base_fields)})]
        adminForm = admin.helpers.AdminForm(form, fieldsets, {})

        context = {
            'title': _('Change password: %s') % escape(user.get_username()),
            'adminForm': adminForm,
            'form_url': form_url,
            'form': form,
            'is_popup': '_popup' in request.REQUEST,
            'add': True,
            'change': False,
            'has_delete_permission': False,
            'has_change_permission': True,
            'has_absolute_url': False,
            'opts': self.model._meta,
            'original': user,
            'save_as': False,
            'show_save': True,
        }
        return TemplateResponse(request,
                                self.change_user_password_template or
                                'admin/auth/user/change_password.html',
                                context, current_app=self.admin_site.name)

    def response_add(self, request, obj, post_url_continue=None):

        if '_addanother' not in request.POST and '_popup' not in request.POST:
            request.POST['_continue'] = 1
        return super(UserAdmin, self).response_add(request, obj,
                                                   post_url_continue)