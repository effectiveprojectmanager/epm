from django.contrib import admin
from django.contrib.admin.views.main import ChangeList
from django.db.models import Q
from EPMApp import configuraciones,utils
from EPMApp.models import TiposItems, Proyectos, Fases,Atributos, RolesProyectos


class AtributosAdmin(admin.ModelAdmin):
    """Representa el administrador de Atributos"""
    fields=('nombre', 'descripcion' ,'tipo_atributo')
    list_display = ('nombre','descripcion','fecha_creacion','tipo_atributo', 'estado')

    actions = ['eliminar_atributo']

    def eliminar_atributo(self, request, queryset):
        """
         Metodo que inactiva el Tipo de Item seleccionado
         @param: request,queryset
        @xtype:
         """
        if len(queryset)==1:
            for obj in queryset:
                estado= obj.estado.id

                proyecto_pk= request.session['id_proyecto']
                p=Proyectos.objects.get(id=proyecto_pk)
                estado_proyecto=p.estado_id

                fase_pk=request.session['id_fase']
                f=Fases.objects.get(id=fase_pk)
                estado_fase=f.estado_id

                has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.ELIMINAR_ATRIBUTOS, fase_pk)
                if has_perm:
                    if estado==2:
                        message_bit = "El Atributo no se puede Eliminar ya que ha sido Eliminado previamente"
                        self.message_user(request, message_bit)
                    if estado_proyecto==1:
                        message_bi = "El Atributo no puede Eliminarse ya que el Proyecto no ha Iniciado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==2:
                        if estado_fase==1:
                            message_bi = "El Atributo no puede Eliminarse ya que la Fase no ha Iniciado"
                            self.message_user(request, message_bi)
                        if estado_fase==2:
                            message_bi = "El Atributo no puede Eliminarse ya que la Fase se ha Eliminado"
                            self.message_user(request, message_bi)
                        if estado_fase==3:
                            if estado==1:
                                atributo_pk= obj.id
                                tipo_item=TiposItems.objects.filter(atributos=atributo_pk)
                                if tipo_item:
                                    message_bi = "El Atributo no puede Eliminarse ya que se crearon Tipos de Items de este Atributo"
                                    self.message_user(request, message_bi)
                                else:
                                    rows_updated = queryset.update(estado=2)
                                    if rows_updated == 1:
                                        message_bit = "El Atributo se ha Eliminado"
                                    self.message_user(request, "%s exitosamente." % message_bit)
                            #if estado==2:
                                #message_bit = "El Atributo no se puede Eliminar ya que ha sido Eliminado previamente"
                                #self.message_user(request, message_bit)
                        if estado_fase==4:
                            message_bi = "El Atributo item no puede Eliminarse ya que la Fase ha Finalizado"
                            self.message_user(request, message_bi)
                        if estado_fase==5:
                            message_bi = "El Atributo no puede Eliminarse ya que la Fase se encuentra Comprometida"
                            self.message_user(request, message_bi)
                        if estado_fase==6:
                            message_bi = "El Atributo no puede Eliminarse ya que la Fase se encuentra en Desarrollo Restringuido"
                            self.message_user(request, message_bi)
                    if estado_proyecto==3:
                        message_bi = "El Atributo no puede Eliminarse ya que el Proyecto se ha Cancelado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==4:
                        message_bi = "El Atributo no puede Eliminarse ya que el Proyecto ha Finalizado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==5:
                        message_bi = "El Atributo no puede Eliminarse ya que el Proyecto se ha Eliminado"
                        self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para Eliminar Atributos"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo Atributo"
            self.message_user(request, message_bi)
    eliminar_atributo.short_description = "Eliminar Atributo"


    def get_readonly_fields(self, request, obj=None):
        """Deshabilita los campos a modificar si un proyecto  ya no se encuentra en estado Iniciado
        o la fase ya no se encuentra en estado de Desarrollo
        @param: request, obj=None
        @xtype:
        """
        proyecto_pk= request.session['id_proyecto']
        p=Proyectos.objects.get(id=proyecto_pk)
        estado_proyecto=p.estado_id

        fase_pk=request.session['id_fase']
        f=Fases.objects.get(id=fase_pk)
        estado_fase=f.estado_id

        #cuando se crea
        if obj == None:
            has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.AGREGAR_ATRIBUTO, fase_pk)
            if has_perm:
                request.session['estado_atributo']=None
                return  ('')
            else:
                message_bi = "No posee permisos para Crear un Atributo"
                self.message_user(request, message_bi)

        #cuando existe un atributo
        if obj != None:
            estado = obj.estado.id

            #Estados del proyecto
            if estado_proyecto==1:
                return ('nombre', 'descripcion' ,'tipo_atributo')
            if estado_proyecto==2:
                #Estados de la fase
                if estado_fase==1:
                    return ('nombre', 'descripcion' ,'tipo_atributo')
                if estado_fase==2 :
                    return ('nombre', 'descripcion' ,'tipo_atributo')
                if estado_fase==3:
                    #Estado del atributo
                    if estado==1:
                        atributo_pk= obj.id
                        tipo_item=TiposItems.objects.filter(atributos=atributo_pk)
                        print(tipo_item)
                        if tipo_item:
                            request.session['estado_atributo']='USADO'
                            return ('nombre', 'descripcion' ,'tipo_atributo')
                        else:
                            has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.MODIFICAR_ATRIBUTO, fase_pk)
                            if has_perm:
                                request.session['estado_atributo']=estado
                                return  ('')
                            else:
                                message_bi = "No posee permisos para Modificar un Atributo"
                                self.message_user(request, message_bi)
                                request.session['estado_atributo']='sin_permiso'
                                return ('nombre', 'descripcion' ,'tipo_atributo')
                    if estado==2:
                            request.session['estado_atributo']=estado
                            return ('nombre', 'descripcion' ,'tipo_atributo')
                if estado_fase==4:
                    return ('nombre', 'descripcion' ,'tipo_atributo')
                if estado_fase==5:
                    return ('nombre', 'descripcion' ,'tipo_atributo')
                if estado_fase==6:
                    return ('nombre', 'descripcion' ,'tipo_atributo')
            if estado_proyecto==3:
                return ('nombre', 'descripcion' ,'tipo_atributo')
            if estado_proyecto==4:
                return ('nombre', 'descripcion' ,'tipo_atributo')
            if estado_proyecto==5:
                return ('nombre', 'descripcion' ,'tipo_atributo')


    def get_changelist(self, request, **kwargs):
        """
        Configuraciones de la lista principal de atributos
        @param: request,**kwargs
        @xtype:
        @return Lista de tipo de item
        """
        return AtributosChangeList

    def has_change_permission(self, request, obj=None):
        id_proyecto=None
        id_fase=None
        try:
            id_proyecto=request.session['id_proyecto']
            id_fase=request.session['id_fase']
        except:
            print("No se encontro el id")
        has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.LISTAR_ATRIBUTOS,id_fase)
        return has_perm

    def has_add_permission(self, request, obj=None):
        id_proyecto=None
        id_fase=None
        try:
            id_proyecto=request.session['id_proyecto']
            id_fase=request.session['id_fase']
        except:
            print("No se encontro el id")
        has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.AGREGAR_ATRIBUTO,id_fase)
        return has_perm

    def get_actions(self, request):
        """
        Desactiva las acciones predeterminadas del sistema de la Administracion de Atributos
        @param: request
        @xtype:
        @return: Lista de acciones
        @rtype: actions
        """
        try:
            id_proyecto=request.session['id_proyecto']
            id_fase=request.session['id_fase']
        except:
            print("No se encontro el id")

        actions = super(AtributosAdmin, self).get_actions(request)
        has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.LISTAR_ATRIBUTOS,id_fase)
        if  has_perm == False:
            del actions['eliminar_atributo']
        return actions

class AtributosChangeList(ChangeList):
    """
    Clase que administra la lista de cambio de atributos
    """
    def __init__(self, *args, **kwargs):
        super(AtributosChangeList, self).__init__(*args, **kwargs)
        self.title = "Seleccione un Atributo y la operacion a realizar"

