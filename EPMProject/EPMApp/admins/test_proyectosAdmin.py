from unittest import TestCase
from django.test.client import Client
from EPMApp.models import Proyectos, EstadosProyectos, Usuarios

from EPMApp.admins.proyectos import ProyectosAdmin

__author__ = 'mai'


class TestProyectosAdmin(TestCase):

    def test_eliminar_proyecto(self):
        self.client = Client()
        session = self.client.session
        session['id_proyecto'] = 3
        response = self.client.get('/admin/EPMApp/proyectos/',{})
        estado1= EstadosProyectos(id='1')
        lider1= Usuarios(id='1')
        proyect1= Proyectos(id='3', nombre='Proyecto1',
            descripcion ='Proyecto de ...',
            fecha_creacion = '2014-05-16',
            estado =estado1,
            lider =lider1)

        Padmin= ProyectosAdmin(model=Proyectos, admin_site=None)

        vect = [proyect1]
        if((Padmin.eliminar_proyecto(request=response, queryset=vect))=='El proyecto se ha eliminado'):
        #if(Padmin.eliminar_proyecto(request=None, queryset=vect)):
            print("Test exitoso")
        else:
            self.fail()