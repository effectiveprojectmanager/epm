from django.contrib import admin
from django.contrib.auth.models import Permission
from django.db.models import Q
from EPMApp import utils
from EPMApp import configuraciones

from django.http import HttpResponseRedirect


class RolesAdmin(admin.ModelAdmin):
    """Representa el administrador de Permisos del sistema"""
    list_display = ('nombre', 'descripcion')
    actions = None
    fields = ('nombre', 'descripcion', 'id_proyecto', 'permisos')
    filter_horizontal = ['permisos', 'id_proyecto']
    list_per_page = 13
    search_fields = ('nombre', 'descripcion')
   # actions = ['asignar_rol']



    def formfield_for_manytomany(self, db_field, request, **kwargs):
        """Realiza el filtrado de los usuarios del proyecto actual"""

        if db_field.name == "permisos":
            print(request)
            kwargs["queryset"] = Permission.objects.filter(
                Q(content_type__app_label="EPMApp") | Q(content_type__app_label="auth"))

        return super(RolesAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)


    # def has_change_permission(self, request, obj=None):
    #     id_proyecto=None
    #     id_fase=None
    #     try:
    #         id_proyecto=request.session['id_proyecto']
    #         id_fase=request.session['id_fase']
    #     except:
    #         print("No se encontro el id")
    #     has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.LISTAR_ROLES_PROYECTOS,id_fase)
    #     return has_perm

    def has_change_permission(self, request, obj=None):
       id_proyecto=None
       id_fase=None
       if request.user.is_superuser:
           return True
       else:
           try:
               id_proyecto=request.session['id_proyecto']
               id_fase=request.session['id_fase']
           except:
               print("No se encontro el id")
           has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.LISTAR_ROLES_PROYECTOS,id_fase)
           return has_perm


class RolesChangeList(object):
    """
    Clase que administra la lista de cambio de Roles
    """
    def __init__(self, *args, **kwargs):
        super(RolesChangeList, self).__init__(*args, **kwargs)
        self.title = "Roles del sistema"