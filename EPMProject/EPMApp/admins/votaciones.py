from django.contrib import admin
from django.contrib.admin.views.main import ChangeList
from EPMApp.models import Votaciones
from EPMApp import utils
from EPMApp import configuraciones


class VotacionesAdmin(admin.ModelAdmin):
    """Representa el administrador de las votaciones realizadas por los usuarios a una solicitud de cambio"""
    model=Votaciones
    list_display = ('miembro','voto')
    fields = ('miembro','voto')


    def get_changelist(self, request, **kwargs):
        """Retorna las configuraciones de la lista principal de votaciones"""
        return VotacionesChangeList

    def has_change_permission(self, request, obj=None):
       id_proyecto=None
       id_fase=None
       try:
           id_proyecto=request.session['id_proyecto']
       except:
           print("No se encontro el id")
       has_perm=utils.has_project_permission(request.user.id,id_proyecto,configuraciones.LISTAR_VOTACIONES)
       print("Has_Perm:", has_perm)
       return has_perm

class VotacionesChangeList(ChangeList):
    """
    Clase que administra las votaciones de una solicitud de cambio
    """
    def __init__(self, *args, **kwargs):
        super(VotacionesChangeList, self).__init__(*args, **kwargs)
        self.title = "1=Voto por aprobacion, 2=Voto por rechazo"
