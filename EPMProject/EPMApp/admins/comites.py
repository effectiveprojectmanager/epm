from django.contrib.admin.views.main import ChangeList
from django.http import HttpResponseRedirect
from django.contrib import admin
from EPMApp import utils
from EPMApp import configuraciones
from EPMApp.models import Proyectos
from django.contrib import messages


class ComiteAdmin(admin.ModelAdmin):
    """Representa el administrador de Comites de Cambios de los proyectos"""
    list_display = ('nombre', 'fecha_creacion')
    fields = ('nombre', 'id_proyecto', 'usuarios')
    list_per_page = 13
    search_fields = ('nombre', 'fecha_creacion')
    filter_horizontal = ['usuarios']
    actions = ['edit_comite', 'delete_comite']

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        """Realiza el filtrado de los usuarios del proyecto actual"""
        if db_field.name == "usuarios":
            print(request)
            proyecto_pk = request.session['id_proyecto']
            if proyecto_pk:
                print(proyecto_pk)
                p = Proyectos.objects.get(id=proyecto_pk)
                kwargs["queryset"] = p.miembros.all()
        return super(ComiteAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)

    def add_comite(self, request, queryset):
        """Redirige a la pagina de agregar un comite de cambio"""
        print('agregar_comite')
        proyecto_pk = request.GET.get('id_proyecto', '')

        for obj in queryset:
            if request.user.is_superuser:
                has_perm = True
            else:
                has_perm = utils.has_project_permission(request.user.id, proyecto_pk, configuraciones.AGREGAR_COMITE)
            if has_perm:
                url = HttpResponseRedirect("/admin/EPMApp/comitecambio/add/")
                return url
            else:
                message_bi = "No posee permisos para Agregar Comite de Cambio"
                self.message_user(request, message_bi)


    add_comite.short_description = "Agregar"

    def delete_comite(self, request, queryset):
        """Redirige a la pagina de eliminar comite de cambio"""
        print('delete_comite')
        proyecto_pk = request.session['id_proyecto']

        if len(queryset) == 1:
            for obj in queryset:
                if request.user.is_superuser:
                    has_perm = True
                else:
                    has_perm = utils.has_project_permission(request.user.id, proyecto_pk, configuraciones.ELIMINAR_COMITE)
                if has_perm:
                    url = HttpResponseRedirect("/admin/EPMApp/comitecambio/%s/delete" % obj.id)
                    return url
                else:
                    message_bi = "No posee permisos para Eliminar Comite de Cambio"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo Comite de Cambio"
            self.message_user(request, message_bi)


    delete_comite.short_description = "Eliminar"

    def edit_comite(self, request, queryset):
        """Redirige a la pagina de editar comite de cambio"""
        print(request)
        proyecto_pk = request.GET.get('id_proyecto', '')

        if len(queryset) == 1:
            for obj in queryset:
                if request.user.is_superuser:
                    has_perm = True
                else:
                    has_perm = utils.has_project_permission(request.user.id, proyecto_pk, configuraciones.MODIFICAR_COMITE)
                if has_perm:
                    url = HttpResponseRedirect("/admin/EPMApp/comitecambio/%s/?_changelist_filters=id_proyecto=%s" % (obj.id, proyecto_pk))
                    return url
                else:
                    message_bi = "No posee permisos para Modificar Comite de Cambio"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo Comite de Cambio"
            self.message_user(request, message_bi)


    edit_comite.short_description = "Editar"

    def get_changelist(self, request, **kwargs):
        """Obtiene las configuraciones de las listas a desplegar"""
        return ComiteChangeList

    def get_queryset(self, request):
        """Retorna el comite de cambio activo"""
        qs = super(ComiteAdmin, self).get_queryset(request)
        return qs.filter(estado=1)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """Despliega el proyecto en el cual se desea agregar una fase
        @param: db_field, request, **kwargs
        @xtype: FasesAdmin
        """
        if db_field.name=="id_proyecto":
            parametros=request.session['id_proyecto']
            if parametros:
                proyecto_id= parametros
                p=Proyectos.objects.filter(id=proyecto_id)
                kwargs["queryset"]=p.filter()

        return super(ComiteAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def has_change_permission(self, request, obj=None):
        id_proyecto=None
        id_fase=None
        try:
            id_proyecto=request.session['id_proyecto']
            #id_fase=request.session['id_fase']
        except:
            print("No se encontro el id")
        if request.user.is_superuser:
            has_perm=True
        else:
            has_perm=utils.has_project_permission(request.user.id,id_proyecto,configuraciones.LISTAR_COMITE)
        return has_perm

    def has_add_permission(self, request, obj=None):
        id_proyecto=None
        id_fase=None
        try:
            id_proyecto=request.session['id_proyecto']
            #id_fase=request.session['id_fase']
        except:
            print("No se encontro el id")
        if request.user.is_superuser:
            has_perm = True
        else:
            has_perm=utils.has_project_permission(request.user.id,id_proyecto,configuraciones.AGREGAR_COMITE)
        return has_perm


    def get_readonly_fields(self, request, obj=None):
        """Deshabilita los campos a modificar si un proyecto  ya no se encuentra en estado Iniciado
        o la fase ya no se encuentra en estado de Desarrollo
        @param: request, obj=None
        @xtype:
        """
        proyecto_pk= request.session['id_proyecto']
        p=Proyectos.objects.get(id=proyecto_pk)
        estado_proyecto=p.estado_id


        #cuando se crea
        if obj == None:
            if request.user.is_superuser:
                has_perm = True
            else:
                has_perm = utils.has_project_permission(request.user.id, proyecto_pk, configuraciones.AGREGAR_COMITE)
            if has_perm:
                request.session['estado_comite']=None
                return  ('')
            else:
                message_bi = "No posee permisos para Crear Comite de Cambio"
                self.message_user(request, message_bi)

        #cuando existe una solicitud
        if obj != None:
            estado = obj.estado

            #Estados del proyecto
            if estado_proyecto==1:
                #Estado de la solicitud
                #Creado
                if estado==1:
                    if request.user.is_superuser:
                        has_perm = True
                    else:
                        has_perm = utils.has_project_permission(request.user.id, proyecto_pk, configuraciones.MODIFICAR_COMITE)
                    if has_perm:
                        request.session['estado_comite']=estado
                        return  ('')
                    else:
                        message_bi = "No posee permisos para Modificar Comite de Cambio"
                        self.message_user(request, message_bi)
                        request.session['estado_comite']='sin_permiso'
                        return ('nombre', 'id_proyecto', 'usuarios')
                #Eliminado
                if estado==2:
                    request.session['estado_comite']=estado
                    return ('nombre', 'id_proyecto', 'usuarios')
            if estado_proyecto==2:
                #Estado de la solicitud
                #Creado
                if estado==1:
                    if request.user.is_superuser:
                        has_perm = True
                    else:
                        has_perm = utils.has_project_permission(request.user.id, proyecto_pk, configuraciones.MODIFICAR_COMITE)
                    if has_perm:
                        request.session['estado_comite']=estado
                        return  ('')
                    else:
                        message_bi = "No posee permisos para Modificar Comite de Cambio"
                        self.message_user(request, message_bi)
                        request.session['estado_comite']='sin_permiso'
                        return ('nombre', 'id_proyecto', 'usuarios')
                #Eliminado
                if estado==2:
                    request.session['estado_comite']=estado
                    return ('nombre', 'id_proyecto', 'usuarios')
            if estado_proyecto==3:
                return ('nombre', 'id_proyecto', 'usuarios')
            if estado_proyecto==4:
                return ('nombre', 'id_proyecto', 'usuarios')
            if estado_proyecto==5:
                return ('nombre', 'id_proyecto', 'usuarios')

    # def save_model(self, request, obj, form, change):
    #
    #     proyecto_pk=request.session['id_proyecto']
    #     usuarios = form.cleaned_data['usuarios']
    #     total = len(usuarios)
    #     resto = (total%2)
    #     if resto != 0:
    #         print('hola')
    #         super(ComiteAdmin,self).save_model(request, obj, form, change)
    #     else:
    #         message_bi = "Los miembros del Comite de Cambio deben de ser impares"
    #         self.message_user(request, message_bi)
    #         super(ComiteAdmin,self).save_model(request, None, None, None)





class ComiteChangeList(ChangeList):
    """
    Clase que administra la lista de cambio de comite
    """
    def __init__(self, *args, **kwargs):
        super(ComiteChangeList, self).__init__(*args, **kwargs)
        self.title = "Comites de cambios"