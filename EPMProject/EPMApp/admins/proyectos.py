from datetime import date
from django.contrib.admin.views.main import ChangeList
from reportlab.lib import colors
from reportlab.lib.colors import pink, green, black
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import inch
from reportlab.pdfgen import canvas
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Table, Spacer
from EPMApp import utils
from EPMApp.admins import items
from EPMApp.admins.items import ItemAdmin, obtener_items
from django.contrib import admin
from django.contrib.messages.constants import ERROR
from django.core import urlresolvers
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from EPMApp import configuraciones
from EPMApp.admins.fases import FaseAdmin

from EPMApp.models import RolesProyectos, Fases

from EPMApp.models import Proyectos
from EPMApp.models import Usuarios
from EPMApp.models import ComiteCambio
from EPMApp.models import Item
from EPMApp.models import TiposItems
from EPMApp.models import RelacionPadre
from EPMApp.models import SolicitudCambio
from EPMApp.models import Votaciones
from EPMApp.models import LineaBase

SESSION_ANTECESOR_FASE = 1
SESSION_IMPORTAR_FASE = 15
SESSION_IMPORT_TIPO=3



class ProyectosAdmin(admin.ModelAdmin):
    """Representa el administrador de Proyectos"""
    list_display = ('nombre', 'fecha_creacion', 'estado')
    fields = ('nombre', 'descripcion', 'lider', 'miembros')
    actions = ['iniciar_proyecto', 'cancelar_proyecto', 'eliminar_proyecto', 'finalizar_proyecto','ver_fases','importar_fases', 'ver_comites', 'ver_miembros',
               'ver_fase_tipo','ver_roles_proyectos', 'asignar_rol', 'administrar_solicitudes','ver_fase_tipo2','generar_pdf_items','generar_pdf_solicitud']
    filter_horizontal = ['miembros']
    inlines = [FaseAdmin]

    #Lista de Usuarios
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        """Realiza el filtrado de los usuarios del proyecto actual"""
        if db_field.name == "usuarios":
            #print(request)
            proyecto_pk = request.session['id_poyecto']
            if proyecto_pk:
                #print(proyecto_pk)
                p = Usuarios.objects.filter()
                kwargs["queryset"] = p.miembros.all()
        return super(ProyectosAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        """
         Obtiene la lista de proyectos a los que pertenece un determinado usuario
         @param: request
         @xtype:
         @return: query
         @rtype: queryset
         """
        #print(request)
        #try:
            #b = request.session['importar_fase']
        #except:
            #print("No se pudo obtener el id de proceso")
        if request.GET.get('proceso'):
            print(request.GET.get('proceso'))
            request.session['proceso'] = 0
        id_usuario = request.user.id
        usuario = Usuarios.objects.filter(pk=id_usuario)
        proyectos = Proyectos.objects.filter(miembros__id=id_usuario)
        proyectos = proyectos.exclude(estado=5)
        return proyectos

    def get_actions(self, request):
        """
        Desactiva las acciones predeterminadas del sistema de la Administracion de Proyectos
        @param: request
        @xtype:
        @return: Lista de acciones
        @rtype: actions
        """
        b = 0
        try:
            b = request.session['proceso']
        except:
            print("No se pudo obtener el id de proceso")

        actions = super(ProyectosAdmin, self).get_actions(request)
        if b:
            if b == SESSION_ANTECESOR_FASE:
                del actions['iniciar_proyecto']
                del actions['cancelar_proyecto']
                del actions['eliminar_proyecto']
                del actions['ver_fases']
                del actions['ver_comites']
                del actions['ver_miembros']
                del actions['ver_roles_proyectos']
                del actions['asignar_rol']
                del actions['importar_fases']
                del actions['administrar_solicitudes']
                del actions['finalizar_proyecto']

            if b==SESSION_IMPORTAR_FASE:
                del actions['iniciar_proyecto']
                del actions['cancelar_proyecto']
                del actions['eliminar_proyecto']
                del actions['ver_comites']
                del actions['ver_miembros']
                del actions['ver_roles_proyectos']
                del actions['asignar_rol']
                del actions['importar_fases']
                del actions['administrar_solicitudes']
                del actions['finalizar_proyecto']
                del actions['ver_fases']
                del actions['ver_fase_tipo2']
            if b==SESSION_IMPORT_TIPO:
                del actions['iniciar_proyecto']
                del actions['cancelar_proyecto']
                del actions['eliminar_proyecto']
                del actions['ver_comites']
                del actions['ver_miembros']
                del actions['ver_roles_proyectos']
                del actions['asignar_rol']
                del actions['importar_fases']
                del actions['administrar_solicitudes']
                del actions['finalizar_proyecto']
                del actions['ver_fases']
                del actions['ver_fase_tipo']
        else:
            del actions['ver_fase_tipo']
            del actions['ver_fase_tipo2']

            if request.user.is_superuser:
                del actions['iniciar_proyecto']
                del actions['cancelar_proyecto']
                del actions['finalizar_proyecto']
                del actions['importar_fases']
                del actions['administrar_solicitudes']
                del actions['ver_fases']
            else:
                del actions['eliminar_proyecto']
                del actions['asignar_rol']
                del actions['ver_comites']
        # if 'change_proyectos' in request.user.permission.codename:
        #     #if 'edit_comite' in actions:
        #         #del actions['edit_comite']
        #     print("Tiene permiso de modificar proyectos")
        return actions

    def get_changelist(self, request, **kwargs):
        """Retorna las configuraciones de la lista principal de proyectos"""
        return ProyectosChangeList

    def generar_pdf_items(self,request,queryset):
        """
        Metodo que realiza la generacion de informes sobre items agrupados por fases relativos a proyecto
         dado en formato PDF
         @return: PDF con los items y sus correspondientes caracteristicas
         @rtype: PDF
        """
        x=100
        y=800

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="informeProyecto.pdf"'

        p = canvas.Canvas(response,pagesize=A4)
        p.drawImage("/home/mai/IS2/Repositorio/codigo/epm/EPMProject/static/bootstrap/img/imagen1.jpg",100,700,width=100, height=120)
        p.drawImage("/home/mai/IS2/Repositorio/codigo/epm/EPMProject/static/bootstrap/img/imagen2.jpg",400,700,width=100, height=120)
        p.setFont("Helvetica-Bold",30,None)
        p.drawString(100,600,"Sistema de Gestion de Proyecto")
        p.setFont("Helvetica-Bold",40,None)
        p.drawCentredString(300,550,"EPM")
        p.setFont("Helvetica-Bold",40,None)
        p.drawString(100,400,"Informes del proyecto")
        p.showPage()

        for obj in queryset:
            obj.id
            proyecto=Proyectos.objects.get(id=obj.id)
            nombre=proyecto.nombre
            #p.setFillColor(black)
            p.setFont("Helvetica-Bold",17,None)
            p.drawString(100,800,"Nombre del Proyecto: ")
            p.setFont("Helvetica",15,None)
            p.drawString(280,800,nombre)
            #p.drawString(A4[0] / 2,810, nombre)

            fases=Fases.objects.filter(id_proyecto=obj.id)
            for fase in fases:

                nombre_fase=fase.nombre
                y=y-20
                if y <= 0:
                    p.showPage()
                    p.setFont("Helvetica",15,None)
                    y=800
                #p.setFillColor(black)
                p.setFont("Helvetica-Bold",16,None)
                p.drawString(x,y, nombre_fase)
                y=y-15
                p.line(x,y,x+400,y)
                p.lineWidth=7
                items_fase=Item.objects.filter(id_fase=fase.id)
                if items_fase:
                    for item in items_fase:
                        codigo_item=str(item.id)
                        nombre_item=item.nombre
                        descripcion_item=item.descripcion
                        tipo_item=TiposItems.objects.get(id=item.tipo_item.id)
                        nombre_tipo=tipo_item.nombre
                        rela=RelacionPadre.objects.filter(item_hijo_id=item) #relacion donde es hijo
                        padre=None
                        nombre_padre=None
                        for relacion in rela:
                            padre=relacion.item_padre
                            nombre_padre=padre.nombre
                        costo=items.obtener_impacto(item)
                        #print("El costo es: ",costo)
                        #costo=str(item.calculo_impacto)
                        costo=str(costo)
                        version=str(item.version)

                        y=y-20
                        if y <= 0:
                            p.showPage()
                            y=800
                            p.setFont("Helvetica",15,None)
                        x=100
                        p.setFont("Times-Bold",15,None)
                        p.drawString(x,y,"Codigo del item: ")
                        #codigo_desc="Codigo del item: "+codigo_item
                        p.setFont("Helvetica",15,None)
                        p.drawString(230,y,codigo_item)

                        y=y-20
                        if y <= 0:
                            p.showPage()
                            y=800
                            p.setFont("Helvetica",15,None)

                        p.setFont("Times-Bold",15,None)
                        p.drawString(x,y,"Nombre del Item: ")
                        p.setFont("Helvetica",15,None)
                        p.drawString(230,y,nombre_item)
                        #nombre_desc=nombre_item+ ": "+descripcion_item
                        y=y-20
                        p.setFont("Times-Bold",15,None)
                        p.drawString(x,y,"Descripcion del Item: ")
                        p.setFont("Helvetica",15,None)
                        p.drawString(240,y,descripcion_item)


                        x=100
                        y=y-20
                        if y <= 0:
                            p.showPage()
                            y=800
                            p.setFont("Helvetica",15,None)
                        p.setFont("Times-Bold",15,None)
                        p.drawString(x,y,"Tipo de Item: ")
                        p.setFont("Helvetica",15,None)
                        p.drawString(230,y,nombre_tipo)

                        x=100
                        y=y-20
                        if y <= 0:
                            p.showPage()
                            y=800
                            p.setFont("Helvetica",15,None)
                        if rela:
                            p.setFont("Times-Bold",15,None)
                            p.drawString(x,y,"Nombre del padre: ")
                            p.setFont("Helvetica",15,None)
                            p.drawString(230,y,nombre_padre)

                        else:
                            p.setFont("Times-Bold",15,None)
                            p.drawString(x,y,"Nombre del padre: ")

                            p.setFont("Helvetica",15,None)
                            p.drawString(230,y,"No posee item padre")

                        x=100
                        y=y-20
                        if y <= 0:
                            p.showPage()
                            y=800
                            p.setFont("Helvetica",15,None)
                        p.setFont("Times-Bold",15,None)
                        p.drawString(x,y,"Costo: ")
                        p.setFont("Helvetica",15,None)
                        p.drawString(230,y,costo)

                        x=100
                        y=y-20
                        if y <= 0:
                            p.showPage()
                            y=800
                            p.setFont("Helvetica",15,None)
                        p.setFont("Times-Bold",15,None)
                        p.drawString(x,y,"Version: ")
                        p.setFont("Helvetica",15,None)
                        p.drawString(230,y,version)

                        y=y-15
                        #p.moveTo(x,y)
                        #x=x+500
                        p.line(x,y,x+400,y)
                        p.lineWidth=7
                        #p.stroke()
                        y=y-25
                        if y <= 0:
                            p.showPage()
                            y=800
                            p.setFont("Helvetica",15,None)
                        x=100
                else:
                    y=y-20
                    p.setFont("Helvetica",15,None)
                    p.drawString(x,y,"La fase no posee Items")
                    y=y-30
                x=100


        # Close the PDF object cleanly, and we're done.
        p.showPage()
        p.save()
        return response
    generar_pdf_items.short_description="Generar Informe de items"


    def generar_pdf_solicitud(self,request,queryset):
        """
        Metodo que realiza la generacion de informes sobre la solicitudes de cambios relativos a proyecto
         dado en formato PDF
         @return: PDF de las solicitudes de cambio del proyecto
         @rtype: PDF
        """
        x=100
        y=800

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="informeProyecto.pdf"'

        p = canvas.Canvas(response,pagesize=A4)
        p.drawImage("/home/mai/IS2/Repositorio/codigo/epm/EPMProject/static/bootstrap/img/imagen1.jpg",100,700,width=100, height=120)
        p.drawImage("/home/mai/IS2/Repositorio/codigo/epm/EPMProject/static/bootstrap/img/imagen2.jpg",400,700,width=100, height=120)
        p.setFont("Helvetica-Bold",30,None)
        p.drawString(100,600,"Sistema de Gestion de Proyecto")
        p.setFont("Helvetica-Bold",40,None)
        p.drawCentredString(300,550,"EPM")
        p.setFont("Helvetica-Bold",30,None)
        p.drawString(100,400,"Informes Solicitud de cambio")
        p.showPage()

        for obj in queryset:
            proyecto_id=obj.id
            proyecto=Proyectos.objects.get(id=proyecto_id)
            nombre=proyecto.nombre
            #p.setFillColor(black)
            p.setFont("Helvetica-Bold",17,None)
            p.drawString(100,800,"Nombre del Proyecto: ")
            p.setFont("Helvetica",15,None)
            p.drawString(280,800,nombre)
            #p.drawString(A4[0] / 2,810, nombre)
            solicitudes=SolicitudCambio.objects.filter(proyecto=obj.id) #solicitudes de cambio existentes del proyecto


            y=y-15
            p.line(x,y,x+400,y)
            p.lineWidth=7
            if solicitudes:
                for solicitud in solicitudes:

                    nombre_solicitud=solicitud.nombre #nombre de la solicitud seleccionada
                    items_sol=solicitud.items.all() #items que estan en una solicitud de cambio
                    print("Los items en la solicitud son: ",items_sol)
                    y=y-20
                    if y <= 0:
                        p.showPage()
                        p.setFont("Helvetica",15,None)
                        y=800
                    p.setFont("Times-Bold",15,None)
                    p.drawString(x,y,"Solicitud: ")
                    p.setFont("Helvetica",15,None)
                    p.drawString(230,y,nombre_solicitud)
                    print("La solicitud es: ",nombre_solicitud)

                    nombre_usuario=str(solicitud.usuario)
                    print("El nombre es: ",nombre_usuario)
                    y=y-20
                    if y <= 0:
                        p.showPage()
                        p.setFont("Helvetica",15,None)
                        y=800
                    p.setFont("Times-Bold",15,None)
                    p.drawString(x,y,"Usuario: ")
                    p.setFont("Helvetica",15,None)
                    p.drawString(230,y,nombre_usuario)


                    id_user=request.user.id #id del usuario logueado
                    print("Id logueo: ",id_user)
                    comite=ComiteCambio.objects.get(id_proyecto=proyecto_id) #obtiene el comite de cambio del proyecto
                    miembros=comite.usuarios.all() #obtiene los miembros del comite
                    print("Miembros: ",miembros)
                    for miembro in miembros:
                        print("Id miembro: ",miembro.id)
                        lider_miembro=0
                        if id_user== miembro.id: #si el usuario forma parte del comite
                            lider_miembro=1

                    if lider_miembro==1:
                        votaciones_miembros=Votaciones.objects.filter(solicitud=solicitud.id) #obtiene todas las votaciones realizadas a la solicitud
                        voto_realizado=0
                        for votos in  votaciones_miembros:
                            if votos.miembro==id_user:
                                voto_realizado=1
                        if voto_realizado==1:
                            y=y-20
                            if y <= 0:
                                p.showPage()
                                p.setFont("Helvetica",15,None)
                                y=800
                            p.setFont("Times-Bold",15,None)
                            p.drawString(x,y,"Voto del Lider: ")
                            p.setFont("Helvetica",15,None)
                            p.drawString(230,y,"El lider ya ha realizado la votacion")

                        else:
                            y=y-20
                            if y <= 0:
                                p.showPage()
                                p.setFont("Helvetica",15,None)
                                y=800
                            p.setFont("Times-Bold",15,None)
                            p.drawString(x,y,"Voto del Lider: ")
                            p.setFont("Helvetica",15,None)
                            p.drawString(230,y,"El lider se encuentra pendiente para la votacion")
                    else:
                        y=y-20
                        if y <= 0:
                            p.showPage()
                            p.setFont("Helvetica",15,None)
                            y=800
                        p.setFont("Times-Bold",15,None)
                        p.drawString(x,y,"Voto del Lider: ")
                        p.setFont("Helvetica",15,None)
                        p.drawString(230,y,"El lider no es miembro del comite de cambio")
                    y=y-20
                    if y <= 0:
                        p.showPage()
                        p.setFont("Helvetica",15,None)
                        y=800
                    p.setFont("Times-Bold",15,None)
                    p.drawString(x,y,"Estado: ")
                    if solicitud.estado.id ==1:
                        p.setFont("Helvetica",15,None)
                        p.drawString(230,y,"Activo,pendiente a ser confirmada")
                    elif solicitud.estado.id==2:
                        p.setFont("Helvetica",15,None)
                        p.drawString(230,y,"Aprobado")
                    elif solicitud.estado.id==3:
                        p.setFont("Helvetica",15,None)
                        p.drawString(230,y,"Pendiente")
                    elif solicitud.estado.id==4:
                        p.setFont("Helvetica",15,None)
                        p.drawString(230,y,"Eliminado")
                    elif solicitud.estado.id==5:
                        p.setFont("Helvetica",15,None)
                        p.drawString(230,y,"Rechazado")
                    y=y-20
                    if y <= 0:
                        p.showPage()
                        y=800
                    p.setFont("Times-Bold",15,None)
                    p.drawString(x,y,"Linea Base afectada: ")
                    for item_soli in items_sol:
                        fase_id=item_soli.id_fase
                        linea_base=LineaBase.objects.filter(id_fase=fase_id)
                        for lb in linea_base:
                            item_lb=lb.items.all()
                            print("Items en la linea base:",item_lb )
                            for item_linea in item_lb:
                                lb_sol=0
                                if item_soli.id==item_linea.id:
                                    lb_sol=1
                            if lb_sol==1:

                                p.setFont("Helvetica",15,None)
                                p.drawString(232,y,lb.nombre)
                                x=x+30
                    y=y-15
                    #p.moveTo(x,y)
                    #x=x+500
                    p.line(x,y,x+400,y)
                    p.lineWidth=7
                    #p.stroke()
                    y=y-25
                    if y <= 0:
                        p.showPage()
                        y=800
                        p.setFont("Helvetica",15,None)
                    x=100

            else:
                y=y-20
                p.setFont("Helvetica",15,None)
                p.drawString(x,y,"El proyecto no posee Solicitud de cambio")
                y=y-30
            x=100
        # Close the PDF object cleanly, and we're done.
        p.showPage()
        p.save()
        return response
    generar_pdf_solicitud.short_description="Informe Solicitud"

    #Lista las fases para importar fase con su tipo de item
    def ver_fase_tipo(self, request, queryset):
        """
        Metodo que representa la Importacion de tipos de items
        @param: request,queryset
        @return: URL que redirecciona al listado de fases
        @rtype: HttpResponseRedirect
        """
        if len(queryset) == 1:
            for obj in queryset:
                estado= obj.estado_id
                has_perm = utils.has_project_permission(request.user.id, obj.id, configuraciones.IMPORTAR_TIPO_ITEM)
                if has_perm:
                    #if estado==1:
                        request.session['proceso']= SESSION_IMPORTAR_FASE
                        request.session['id_proyecto'] = obj.id
                        return HttpResponseRedirect("/admin/EPMApp/fases/?id_proyecto=%s" % obj.id)
                    #if estado==2:
                        #message_bi = "No se puede Importar Tipo de Item, el Proyecto ha Iniciado"
                        #self.message_user(request, message_bi)
                    #if estado==3:
                        #message_bi = "No se puede Importar Fases, el Proyecto se ha Anulado"
                        #self.message_user(request, message_bi)
                    #if estado==4:
                        #message_bi = "No se puede Importar Fases, el Proyecto se ha Eliminado"
                        #self.message_user(request, message_bi)
                    #if estado==4:
                        #message_bi = "No se puede Importar Fases, el Proyecto ha Finalizado"
                        #self.message_user(request, message_bi)
                else:
                        message_bi = "No posee permisos para importar fases con sus tipos de items"
                        self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione una sola fase"
            self.message_user(request, message_bi)

    ver_fase_tipo.short_description = "Fases del Proyecto"

    #Lista las fases para Importar Tipo de Item a una fase
    def ver_fase_tipo2(self, request, queryset):
        """
        Metodo que representa la Importacion de tipos de items
        @param: request,queryset
        @return: URL que redirecciona al listado de fases
        @rtype: HttpResponseRedirect
        """
        if len(queryset) == 1:
            for obj in queryset:
                estado= obj.estado_id
                has_perm = utils.has_project_permission(request.user.id, obj.id, configuraciones.IMPORTAR_TIPO_ITEM)
                if has_perm:
                    #if estado==1:
                        request.session['proceso']= SESSION_IMPORT_TIPO
                        request.session['id_proyecto'] = obj.id
                        return HttpResponseRedirect("/admin/EPMApp/fases/?id_proyecto=%s" % obj.id)
                    #if estado==2:
                        #message_bi = "No se puede Importar Tipo de Item, el Proyecto ha Iniciado"
                        #self.message_user(request, message_bi)
                    #if estado==3:
                        #message_bi = "No se puede Importar Fases, el Proyecto se ha Anulado"
                        #self.message_user(request, message_bi)
                    #if estado==4:
                        #message_bi = "No se puede Importar Fases, el Proyecto se ha Eliminado"
                        #self.message_user(request, message_bi)
                    #if estado==4:
                        #message_bi = "No se puede Importar Fases, el Proyecto ha Finalizado"
                        #self.message_user(request, message_bi)
                else:
                        message_bi = "No posee permisos para importar tipos de items"
                        self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione una sola fase"
            self.message_user(request, message_bi)

    ver_fase_tipo2.short_description = "Ver Fases del Proyecto"

    #Lista Proyectos para importar fases al Proyecto actual
    def importar_fases(self, request, queryset):
        """
        Importacion de fases
        @param: request, queryset
        @xtype:
        @return: URL que redirecciona al listado de proyectos
        @rtype: HttpResponseRedirect
        """
        #request.session['proceso']=SESSION_IMPORTAR_FASE
        url = None
        if len(queryset) == 1:
            for obj in queryset:
                estado= obj.estado_id
                has_perm = utils.has_project_permission(request.user.id, obj.id, configuraciones.IMPORTAR_FASES)
                if has_perm:
                    if estado==1:
                        request.session['id_proyecto_actual'] = obj.id
                        #request.session['importar_fase']= SESSION_IMPORTAR_FASE
                        request.session['proceso']= SESSION_IMPORTAR_FASE
                        print('Entro')
                        return HttpResponseRedirect("/admin/EPMApp/proyectos/")
                    if estado==2:
                        message_bi = "No se puede Importar Fases, el Proyecto ha Iniciado"
                        self.message_user(request, message_bi)
                    if estado==3:
                        message_bi = "No se puede Importar Fases, el Proyecto se ha Anulado"
                        self.message_user(request, message_bi)
                    if estado==4:
                        message_bi = "No se puede Importar Fases, el Proyecto se ha Eliminado"
                        self.message_user(request, message_bi)
                    if estado==4:
                        message_bi = "No se puede Importar Fases, el Proyecto ha Finalizado"
                        self.message_user(request, message_bi)
                else:
                        message_bi = "No posee permisos para importar fases dentro del proyecto %s" % obj
                        self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione una sola fase"
            self.message_user(request, message_bi)
    importar_fases.short_description = "Importar Fases"

    #Ver Comite de Cambio
    def ver_comites(self, request, queryset):
        """Redirecciona a la lista de comites de cambio pertenecientes a un proyecto dado
        @return: URL que redirecciona al listado de comite de cambio
        @rtype: HttpResponseRedirect
        """
        print("Entro en comite")
        if len(queryset) == 1:
            for obj in queryset:
                print(request.user.is_superuser)
                if request.user.is_superuser:
                    has_perm = True
                else:
                    has_perm = utils.has_project_permission(request.user.id, obj.id, 'change_comitecambio')
                if has_perm:
                    request.session['id_proyecto'] = obj.id
                    print("Entro en el url")
                    return HttpResponseRedirect("/admin/EPMApp/comitecambio/?id_proyecto=%s" % obj.id)
                else:
                    message_bi = "No posee permiso para listar los comites de cambio del proyecto %s" % obj
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo proyecto"
            self.message_user(request, message_bi)
    ver_comites.short_description = "Comite de Cambio"

    #Miembros del Proyecto
    def ver_miembros(self, request, queryset):
        """
        Redirecciona a la lista de miembros pertenecientes a un proyecto dado
        @return: URL que redirecciona al listado de usuarios miembros del proyecto
        @rtype: HttpResponseRedirect
        """
        #request.session['proceso']=3
        if len(queryset) == 1:
            for obj in queryset:
                if request.user.is_superuser:
                    has_perm = True
                else:
                    has_perm = utils.has_project_permission(request.user.id, obj.id, configuraciones.LISTAR_MIEMBROS_PROY)
                if has_perm:
                    request.session['id_proyecto'] = obj.id
                    return HttpResponseRedirect("/admin/EPMApp/usuarios/?proyectos=%s" % obj.id)
                else:
                    message_bi = "No posee permiso para listar los miembros del proyecto %s" % obj
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo proyecto"
            self.message_user(request, message_bi)
    ver_miembros.short_description = "Usuarios miembros"

    #Eliminar Proyecto
    def eliminar_proyecto(self, request, queryset):
        """Elimina logicamente un proyecto dado. El proyecto eliminado adquiere el estado """
        if len(queryset) == 1:
            for obj in queryset:
                has_perm = utils.has_project_permission(request.user.id, obj.id, configuraciones.ELIMINAR_PROYECTOS)
                #has_perm_system=request.user.has_perm(configuraciones.ELIMINAR_PROYECTOS)
                if has_perm:
                    estado = obj.estado.id
                    if estado == 1:
                        rows_updated = queryset.update(estado=5)
                        if rows_updated == 1:
                            request.session['id_proyecto'] = obj.id
                            message_bit = "El proyecto se ha eliminado"
                        else:
                            message_bit = "%s proyectos se han eliminado" % rows_updated
                        self.message_user(request, "%s exitosamente." % message_bit)
                    elif estado == 2:
                        message_bi = "El Proyecto no puede eliminarse debido a que se encuentra Iniciado"
                        self.message_user(request, message_bi)
                    elif estado == 3:
                        message_bi = "El Proyecto no puede eliminarse debido a que se encuentra Cancelado"
                        self.message_user(request, message_bi)
                    elif estado == 4:
                        message_bi = "El Proyecto no puede eliminarse debido a que se encuentra Finalizado"
                        self.message_user(request, message_bi)
                    elif estado == 5:
                        message_bi = "El Proyecto no puede eliminarse debido a que ya ha sido eliminado previamente"
                        self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permiso para eliminar el proyecto %s" % obj
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo proyecto"
            self.message_user(request, message_bi)
    eliminar_proyecto.short_description = "Eliminar Proyecto"

    #Iniciar Proyecto
    def iniciar_proyecto(self, request, queryset):
        """
        Metodo que inicia un proyecto, el estado del proyecto cambia a Iniciado
        """
        if len(queryset) == 1:
            for obj in queryset:
                has_perm = utils.has_project_permission(request.user.id, obj.id, configuraciones.INICIAR_PROYECTO)
                if has_perm:
                    estado = obj.estado.id
                    if estado == 1:
                        comite= ComiteCambio.objects.filter(id_proyecto=obj.id)
                        c= comite.filter(estado=1)
                        if c:
                            rows_updated = queryset.update(estado=2)
                            fases= Fases.objects.filter(id_proyecto=obj.id)
                            for f in fases:
                                if f.estado_id==1:
                                    f.estado_id=3
                                    fecha=date.today()
                                    f.fecha_inicio=fecha
                                    f.save()
                            if rows_updated == 1:
                                request.session['id_proyecto'] = obj.id
                                message_bit = "El proyecto ha iniciado"
                            self.message_user(request, "%s exitosamente." % message_bit)
                        else:
                            message_bi = "El Proyecto no puede iniciarse debido a que no tiene un Comite de Cambio"
                            self.message_user(request, message_bi)
                    if estado==2:
                        message_bi = "El Proyecto no puede iniciarse debido a que ya ha sido Iniciado previamente"
                        self.message_user(request, message_bi)
                    if estado==3:
                        message_bi = "El Proyecto no puede iniciarse debido a que se encuentra Cancelado"
                        self.message_user(request, message_bi)
                    if estado==4:
                        message_bi = "El Proyecto no puede iniciarse debido a que se encuentra Finalizado"
                        self.message_user(request, message_bi)
                    if estado==5:
                        message_bi = "El Proyecto no puede iniciarse debido a que se encuentra Eliminado"
                        self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para iniciar el proyecto %s" % obj
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo proyecto"
            self.message_user(request, message_bi)
    iniciar_proyecto.short_description = "Iniciar Proyecto"

    #Cancelar Proyecto
    def cancelar_proyecto(self, request, queryset):
        """
        Metodo que cancela un Proyecto, el estado del proyecto cambia a Anulado
        """
        if len(queryset) == 1:
            for obj in queryset:
                estado = obj.estado.id
                has_perm = utils.has_project_permission(request.user.id, obj.id, configuraciones.CANCELAR_PROYECTO)
                if has_perm:
                    if estado == 2:
                        rows_updated = queryset.update(estado=3)
                        if rows_updated == 1:
                            request.session['id_proyecto'] = obj.id
                            message_bit = "El proyecto se ha cancelado"
                            self.message_user(request, "%s exitosamente." % message_bit)
                    if estado==1:
                        message_bi = "El Proyeccto no puede cancelarse debido a que No ha Iniciado"
                        self.message_user(request, message_bi)
                    if estado==3:
                        message_bi = "El Proyeccto no puede cancelarse debido a que ya ha sido Cancelado previamente"
                        self.message_user(request, message_bi)
                    if estado==4:
                        message_bi = "El Proyeccto no puede cancelarse debido a que se encuentra Finalizado"
                        self.message_user(request, message_bi)
                    if estado==5:
                        message_bi = "El Proyeccto no puede cancelarse debido a que se encuentra Eliminado"
                        self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para cancelar el proyecto %s" % obj
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo proyecto"
            self.message_user(request, message_bi)
    cancelar_proyecto.short_description = "Cancelar Proyecto"

    #Finalizar proyecto
    def finalizar_proyecto(self, request, queryset):
        """Finalizar un proyecto"""
        if len(queryset)==1:
            for obj in queryset:
                estado = obj.estado.id
                has_perm = utils.has_project_permission(request.user.id, obj.id, configuraciones.FINALIZAR_PROYECTO)
                if has_perm:
                    if estado==2:
                        fases=Fases.objects.filter(id_proyecto=obj.id)
                        cant_fases=len(fases)
                        cont=0

                        for i in fases:
                            estado_fase=i.estado_id
                            if estado_fase==4:
                                cont=cont+1

                        if cont==cant_fases:
                            queryset.update(estado=4)
                            fecha=date.today()
                            print(fecha)
                            queryset.update(fecha_finalizacion=fecha)
                            request.session['id_proyecto']=obj.id
                            message_bit = "El Proyecto ha finalizado exitosamente"
                        else:
                            message_bit = "El Proyecto no puede Finalizar ya que sus fases no han finalizado"
                        self.message_user(request, message_bit)
                    if estado==1:
                        message_bi = "El Proyeccto no puede finalizarse debido a que no ha Iniciado"
                        self.message_user(request, message_bi)
                    if estado==3:
                        message_bi = "El Proyeccto no puede finalizarse debido a que se encuentra Anulado"
                        self.message_user(request, message_bi)
                    if estado==4:
                        message_bi = "El Proyeccto no puede finalizarse debido a que ya ha sido Finalizado previamente"
                        self.message_user(request, message_bi)
                    if estado==5:
                        message_bi = "El Proyeccto no puede finalizarse debido a que se encuentra Eliminado"
                        self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para finalizar el proyecto %s" % obj
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo Proyecto"
            self.message_user(request, message_bi)
    finalizar_proyecto.short_description = "Finalizar Proyecto"

    #Ver fases del proyecto
    def ver_fases(self, request, queryset):
        """
        Metodo que representa al administrador de fases
        @return: URL que redirecciona a la lista de fases del proyecto desde el cual es invocado
        @rtype: HttpResponseRedirect
        """
        #print("ver_fases")
        #print(request)
        if len(queryset) == 1:
            for obj in queryset:
                has_perm = utils.has_project_permission(request.user.id, obj.id, configuraciones.LISTAR_FASES)
                if has_perm:
                    request.session['id_proyecto'] = obj.id
                    request.session['estado_proyecto']=obj.estado.id
                    print(request.session['estado_proyecto'])
                    return HttpResponseRedirect("/admin/EPMApp/fases/?id_proyecto=%s" % obj.id)
                else:
                    message_bi = "No posee permisos para visualizar las fases del proyecto %s" % obj
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo proyecto"
            self.message_user(request, message_bi)
    ver_fases.short_description = "Administracion de Fases"

    #Asignar Rol a usuario
    def asignar_rol(self, request, queryset):
        """
        Asigna un rol de proyecto a un usuario seleccionado
        @return: URL que retorna a la vista de agregar rol
        @rtype: HttpResponseRedirect
        """
        #print("asignar_rol")
        #print(request)
        if len(queryset) == 1:
            for obj in queryset:

                print (request.user.is_admin)
                if request.user.is_superuser:
                    has_perm = True
                else:
                    has_perm = utils.has_project_permission(request.user.id, obj.id, configuraciones.ASIGNAR_ROLES_PROY)

                if has_perm:
                    estado = obj.estado.id
                    if estado==1:
                        request.session['id_proyecto'] = obj.id
                        return HttpResponseRedirect("/admin/EPMApp/usuariosrolesproyectos/add/")
                    if estado==2:
                        #message_bi = "No se puede asignar Rol del Proyecto al Usuario debido a que el Proyecto ha Iniciado"
                        #self.message_user(request, message_bi)
                        request.session['id_proyecto'] = obj.id
                        return HttpResponseRedirect("/admin/EPMApp/usuariosrolesproyectos/add/")
                    if estado==3:
                        message_bi = "No se puede asignar Rol del Proyecto al Usuario debido a que el Proyecto se ha Cancelado"
                        self.message_user(request, message_bi)
                    if estado==4:
                        message_bi = "No se puede asignar Rol del Proyecto al Usuario debido a que el Proyecto ha Finalizado"
                        self.message_user(request, message_bi)
                    if estado==5:
                        message_bi = "No se puede asignar Rol del Proyecto al Usuario debido a que el Proyecto se ha Eliminado"
                        self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para asignar roles a usuarios en el proyecto %s" % obj
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo proyecto"
            self.message_user(request, message_bi)
    asignar_rol.short_description = "Asignacion de roles a usuarios"

    #ver roles del proyecto
    def ver_roles_proyectos(self, request, queryset):
        """
        Metodo que representa a la administracion de roles del proyecto
        @return: URL que redirecciona a la lista de fases del proyecto desde el cual es invocado
        @rtype: HttpResponseRedirect
        """
        #print("ver_roles_proyectos")
        #print(request)
        if len(queryset) == 1:
            for obj in queryset:
                if request.user.is_superuser:
                    has_perm = True
                else:
                    has_perm = utils.has_project_permission(request.user.id, obj.id, configuraciones.LISTAR_ROLES_PROYECTOS)
                if has_perm:
                    request.session['id_proyecto'] = obj.id
                    return HttpResponseRedirect("/admin/EPMApp/rolesproyectos/?id_proyecto=%s" % obj.id)
                else:
                    message_bi = "No posee permisos para listar los roles del proyecto %s" % obj
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo proyecto"
            self.message_user(request, message_bi)
    ver_roles_proyectos.short_description = "Roles del Proyecto"


    def administrar_solicitudes(self, request, queryset):
            """
             Metodo que despliega la interfaz de administracion de solicitudes de cambio
             @param: request,queryset
            @xtype:
             """
            rastro=None
            if len(queryset)==1:
                for obj in queryset:
                    estado= obj.estado.id

                    request.session['id_proyecto']=obj.id
                    try:
                        rastro=request.session['rastro']
                    except:
                        print('no se pudo obtener el rastro del usuario')
                    if rastro:
                        rastro=rastro+'/%s'%obj
                    else:
                        rastro='/%s'%obj
                    request.session['rastro']=rastro
                    p=Proyectos.objects.get(id=obj.id)
                    estado_proyecto=p.estado_id


                    if estado_proyecto==1:
                        message_bi = "No se pueden administrar solicitudes de cambio debido a que el proyecto no ha iniciado"
                        self.message_user(request, message_bi)
                    else:
                        has_perm = utils.has_project_permission(request.user.id, obj.id, configuraciones.LISTAR_SOLICITUDES)
                        if has_perm:
                            request.session['id_proyecto'] = obj.id
                            request.session['estado_proyecto'] = estado_proyecto
                            return HttpResponseRedirect("/admin/EPMApp/solicitudcambio/?proyecto=%s" % obj.id)
                        else:
                            message_bi = "No posee permisos para visualizar las solicitudes de cambio en el proyecto %s"%obj
                            self.message_user(request, message_bi)
            else:
                message_bi = "Seleccione un solo tipo item"
                self.message_user(request, message_bi)
    administrar_solicitudes.short_description = "Administrar solicitudes de cambio"


    #Campos a modificar si el proyecto esta creado
    def get_readonly_fields(self, request, obj=None):
        """Deshabilita los campos a modificar si un proyecto ya no se encuentra en estado creado
        @param: request, obj=None
        @xtype:
        """
        if obj == None:
            request.session['estado_proyecto']=None
            return ('')
        if obj != None:
            estado = obj.estado.id
            print(estado)
            if estado==1:
                if request.user.is_admin:
                    return  ('')
                else:
                    has_perm = utils.has_project_permission(request.user.id, obj.id, configuraciones.MODIFICAR_PROYECTO)
                    if has_perm:
                        request.session['estado_proyecto']=estado
                        return  ('')
                    else:
                        message_bi = "No posee permisos para Modificar Proyecto"
                        self.message_user(request, message_bi)
                        request.session['estado_proyecto']='sin_permiso'
                        return ('nombre', 'descripcion', 'lider', 'miembros')

            else:
                request.session['estado_proyecto']=estado
                return ('nombre', 'descripcion', 'lider', 'miembros')


    def has_change_permission(self, request, obj=None):
        # id_proyecto=None
        # id_fase=None
        # try:
        #     id_proyecto=request.session['id_proyecto']
        # except:
        #     print("No se encontro el id")
        # if request.user.is_superuser:
        #     has_perm = True
        # else:
        #     has_perm = utils.has_project_permission(request.user.id, id_proyecto, configuraciones.LISTAR_PROYECTO)
        # return has_perm
        has_perm = True
        return has_perm

class ProyectosChangeList(ChangeList):
    """
    Clase que administra la lista de cambio de proyectos
    """
    def __init__(self, *args, **kwargs):
        super(ProyectosChangeList, self).__init__(*args, **kwargs)
        self.title = "Seleccione un proyecto y la operacion a realizar"""


