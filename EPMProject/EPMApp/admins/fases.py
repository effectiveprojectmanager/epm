from django.contrib import admin
from django.db.models import Q

from django.contrib.admin.views.main import ChangeList

from datetime import date
from EPMApp import utils
from django.http import HttpResponseRedirect
from EPMApp import configuraciones
from EPMApp.admins.items import ItemAd
from EPMApp.admins.tipo_item import TipoItemAdmin

from EPMApp.models import Proyectos, Fases, TiposItems, Item,RolesProyectos

SESSION_ANTECESOR_FASE=1
SESION_LIST_PROYECTO=2
SESSION_IMPORT_TIPO=3
SESSION_VER_ITEM=4
IMPORTAR_FASE=5
SESSION_IMPORTAR_FASE = 15

class FaseAdmin(admin.TabularInline):
    """
    Administrador de Fases que permite crear una fase en el momento de la creacion del Proyecto
    """
    fields = ('nombre', 'descripcion' )
    model = Fases
    extra = 0


class FasesAdmin(admin.ModelAdmin):
    """
    Representa el administrador general de Fases
    """
    list_display = ('nombre', 'descripcion', 'estado')
    fields = ('nombre', 'descripcion', 'id_proyecto')

   # inlines = [TipoItemAdmin, ItemAd]

    actions = ['eliminar_fase', 'finalizar_fase', 'ver_tipositems', 'ver_atributos','ver_item','ver_lineabase',
               'ver_antecesor','ver_list_proyecto','ver_importacion_tipo','ver_importar_fase']


    #inlines = [TipoItemAdmin, ItemAd]
    #def add_fase(self, request, queryset):
        #"""
        #Agrega una fase a un proyecto
        #@param: request,queryset
        #@xtype:
        #@return: URL que redirecciona a la administracion de atributos correspondiente al tipo de item seleccionado
        #@rtype: HttpResponseRedirect
        #"""
        #print('holaa')
        #print(queryset)
        #print(request)
        #print(self)
        #if len(queryset)==1:

        #proyecto_id= request.session['id_proyecto']
        #return HttpResponseRedirect("/admin/EPMApp/fases/add/?id_proyecto=%s" %proyecto_id)
        #return HttpResponseRedirect("/admin/EPMApp/fases/add/")
    #add_fase.short_description = "Agregar Fase"

    #Admistracion de Atributos
    def ver_atributos(self, request, queryset):
        """
        Visualiza los atributos de un tipo de item
        @param: request,queryset
        @xtype:
        @return: URL que redirecciona a la administracion de atributos correspondiente al tipo de item seleccionado
        @rtype: HttpResponseRedirect
        """
        proyecto_pk=request.session['id_proyecto']
        if len(queryset)==1:
            for obj in queryset:
                has_perm =utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.LISTAR_ATRIBUTOS, obj.id)
                if has_perm:
                    request.session['id_fase']=obj.id

                    proyecto_pk=request.session['id_proyecto']
                    p=Proyectos.objects.get(id=proyecto_pk)
                    estado_proyecto=p.estado_id

                    if estado_proyecto==1:
                        message_bi = "No se puede listar los Atributos debido a que no ha Iniciado el Proyecto"
                        self.message_user(request, message_bi)
                    else:
                        request.session['estado_proyecto']=estado_proyecto
                        request.session['estado_fase']=obj.estado.id
                        return HttpResponseRedirect("/admin/EPMApp/atributos/?fases=%s" %obj.id )
                else:
                    message_bi = "No posee permisos para listar los atributos"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione una sola fase"
            self.message_user(request, message_bi)
    ver_atributos.short_description = "Administracion de Atributos"

    #Redirige a lista de tipos de items para importar
    def ver_importacion_tipo(self, request, queryset):
        """
        Metodo que representa a la importacion de tipos de items
        @param: request,queryset
        @xtype:
        @return: URL que redirecciona al listado de tipos de items candidatos a ser importados
        @rtype: HttpResponseRedirect
        """
        proyecto=request.session['id_proyecto']
        if len(queryset)==1:
            for obj in queryset:
                has_perm = utils.has_fase_permission(request.user.id, proyecto , configuraciones.IMPORTAR_TIPO_ITEM, obj.id )
                if has_perm:
                    #request.session['id_fases']=obj.id
                    request.session['proceso']=SESSION_IMPORT_TIPO
                    return HttpResponseRedirect("/admin/EPMApp/tipositems/?id_fase=%s"%obj.id)
                else:
                    message_bi = "No posee permisos para Importar Tipos de Items"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione una sola fase"
            self.message_user(request, message_bi)
    ver_importacion_tipo.short_description = "Ver Tipos de Items de la Fase"

    #Ver el Proyecto para Importar Tipo de Item a la fase
    def ver_list_proyecto(self, request, queryset):
        """
        Despliega la lista de los proyectos de donde se importara el tipo de item
        @param: request,queryset
        @xtype:
        @return: URL que redirecciona a la lista de proyectos candidatos de los cuales se podra importar el/los tipos de items
        @rtype: HttpResponseRedirect
        """
        proyecto=request.session['id_proyecto']

        if len(queryset)==1:
            for obj in queryset:
                has_perm = utils.has_fase_permission(request.user.id, proyecto , configuraciones.IMPORTAR_TIPO_ITEM, obj.id )

                if has_perm:

                    #request.session['proceso']=SESION_LIST_PROYECTO
                    request.session['proceso']= SESSION_IMPORT_TIPO
                    request.session['id_fase_actual']=obj.id
                    estado= obj.estado.id

                    proyecto_pk=request.session['id_proyecto']
                    p=Proyectos.objects.get(id=proyecto_pk)
                    estado_proyecto=p.estado_id

                    if estado_proyecto==1:
                        message_bi = "No se puede Importar Tipo de Item a la Fase debido a que el Proyecto no ha Iniciado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==2:
                        #Estados de la Fase
                        if estado==3:
                            return HttpResponseRedirect("/admin/EPMApp/proyectos/")
                        if estado==2:
                            message_bi = "No se puede Importar Tipo de Item a la Fase debido a que la Fase se encuentra ELiminada"
                            self.message_user(request, message_bi)
                        if estado==1:
                            message_bi = "No se puede Importar Tipo de Item a la Fase debido a que la Fase no se encuentra en Desarrollo"
                            self.message_user(request, message_bi)
                        if estado==4:
                            message_bi = "No se puede Importar Tipo de Item a la Fase debido a que la Fase se encuentra Finalizada"
                            self.message_user(request, message_bi)
                        if estado==5:
                            message_bi = "No se puede Importar Tipo de Item a la Fase debido a que la Fase se encuentra Comprometida"
                            self.message_user(request, message_bi)
                        if estado==6:
                            message_bi = "No se puede Importar Tipo de Item a la Fase debido a que la Fase se encuentra en Desarrollo Restringuido"
                            self.message_user(request, message_bi)
                    if estado_proyecto==3:
                            message_bi = "No se puede Importar Tipo de Item a la Fase debido a que el Proyecto se ha Cancelado"
                            self.message_user(request, message_bi)
                    if estado_proyecto==4:
                            message_bi = "No se puede Importar Tipo de Item a la Fase debido a que el Proyecto ha Finalizado"
                            self.message_user(request, message_bi)
                    if estado_proyecto==5:
                            message_bi = "No se puede Importar Tipo de Item a la Fase debido a que el Proyecto se ha Eliminado"
                            self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para Importar Tipos de Items"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione una sola fase"
            self.message_user(request, message_bi)
    ver_list_proyecto.short_description = "Importar Tipo de Item"

    #Importar Fase con sus Tipos de Items
    def ver_importar_fase(self, request, queryset):
        """
        Metodo que representa a la importacion de fases con sus tipos de items
        @param: request,queryset
        @xtype:
        """
        proyecto=request.session['id_proyecto']
        fase=request.session['id_fase']

        if len(queryset)==1:
            for obj in queryset:
                has_perm = utils.has_fase_permission(request.user.id, proyecto, configuraciones.IMPORTAR_FASES, fase)
                if has_perm:
                    tipo_item = None
                    proyecto_pk = request.session['id_proyecto_actual']
                    fase_tipo = Fases.objects.get(pk=obj.id)

                    try:
                        tipo_item = TiposItems.objects.filter(id_fase=obj.id)
                        #print(tipo_item)
                    except:
                        print("No se pudieron obtener los tipos de items")
                    #print("ver_importar_fase")
                    #print(tipo_item)
                    if fase_tipo:
                        fase_tipo_nuevo = Fases(nombre=fase_tipo.nombre, descripcion=fase_tipo.descripcion)
                        fase_tipo_nuevo.id_proyecto = Proyectos.objects.get(pk=proyecto_pk)
                        fase_tipo_nuevo.save()
                        fase_pk = fase_tipo_nuevo.id
                        if tipo_item:
                            for tipo in tipo_item:
                                tipo_item_nuevo = TiposItems(nombre=tipo.nombre, descripcion=tipo.descripcion)
                                tipo_item_nuevo.id_fase = Fases.objects.get(pk=fase_pk)
                                tipo_item_nuevo.save()
                                atributos=tipo.atributos.all()
                                print(atributos)
                                if atributos:
                                    tipo_item_nuevo.atributos=atributos
                                    tipo_item_nuevo.save()
                        message_bit = "Se ha importado la Fase con su tipo de item"
                        self.message_user(request, "%s exitosamente." % message_bit)
                        request.session['proceso'] = 0
                        return HttpResponseRedirect("/admin/EPMApp/proyectos/")
                    else:
                        message_bi = "No se ha podido importar"
                        self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para Importar Fases"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione una sola fase"
            self.message_user(request, message_bi)

    ver_importar_fase.short_description = "Importar Fase con Tipos de Items"

    def get_queryset(self, request):
        """
         Metodo que obtiene el proyecto al que pertenece una fase
        @param: request
        @xtype:
        @return: query
        @rtype: queryset
        """
        b=0
        try:
            b=request.session['proceso']
        except:
            print("No pudo obtenerse el id de proceso")
        qs = super(FasesAdmin, self).get_queryset(request)
        print("Valor de b:",b)
        if b:
            if b==SESSION_ANTECESOR_FASE:
                proyecto_pk=request.session['id_proyecto']
                fases_pk=request.session['id_fase']
                print("Fase Sucesor: ", fases_pk)
                if proyecto_pk:
                    p=Proyectos.objects.get(id=proyecto_pk)
                    qs=qs.filter(id_proyecto=proyecto_pk)
                    print(qs)
                    qs.exclude(pk=fases_pk)
                    return qs.filter(pk__lt = fases_pk)
                    print(qs.exclude(pk=fases_pk))
            if b==SESION_LIST_PROYECTO:
                proyecto_pk=request.session['id_proyecto']
                if proyecto_pk:
                    p=Proyectos.objects.get(id=proyecto_pk)
                    qs=qs.filter(id_proyecto=proyecto_pk)
                    return qs

        return qs


    def ver_antecesor(self, request, queryset):
        """
        Metodo que desliega la lista de los items candidatos a ser Antecesor
        @param: request,queryset
        @xtype:
        @return: URL que redirecciona a la vista de creacion de la relacion
        @rtype: HttpResponseRedirect
        """
        proyecto_pk=request.session['id_proyecto']
        fase_pk=request.session['id_fase']

        if len(queryset) == 1:
            for obj in queryset:
                has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.AGREGAR_RELACION_AS, fase_pk)
                if has_perm:
                    request.session['id_fase'] = obj.id
                    return HttpResponseRedirect("/admin/EPMApp/relacionantecesor/add/?id_fase=%s" % obj.id)
                else:
                    message_bi = "No posee permisos para Importar Fases"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo item"
            self.message_user(request, message_bi)
    ver_antecesor.short_description = "Seleccionar antecesor"

    def has_add_permission(self, request):
        """
        Desactiva la opcion de eliminar
        @param: request
        @xtype:
        @return: False, True
        @rtype: Boolean
        """
        b=0
        try:
            b=request.session['proceso']
        except:
            print("No pudo obtenerse el id de proceso")
        if b==SESSION_ANTECESOR_FASE:
            return False
        else:
            return True

    def get_actions(self, request):
        """
        Desactiva las acciones predeterminadas del sistema de la Administracion de Fases
        @param: request
        @xtype:
        @return: Lista de acciones
        @rtype: actions
        """
        b=0
        try:
            b=request.session['proceso']
        except:
            print("No pudo obtenerse el id de proceso")
        actions = super(FasesAdmin, self).get_actions(request)
        if b:
            if b==SESSION_ANTECESOR_FASE:
                del actions['eliminar_fase']
                del actions['ver_tipositems']
                del actions['ver_list_proyecto']
                del actions['ver_item']
                del actions['ver_importacion_tipo']
                del actions['ver_importar_fase']
                del actions['ver_lineabase']
                del actions['ver_atributos']
                del actions['finalizar_fase']
                #del actions['add_fase']
            if b==SESSION_IMPORTAR_FASE:
                del actions['eliminar_fase']
                del actions['ver_tipositems']
                del actions['ver_list_proyecto']
                del actions['ver_item']
                del actions['ver_importacion_tipo']
                del actions['ver_lineabase']
                del actions['ver_atributos']
                del actions['finalizar_fase']
                del actions['ver_antecesor']

            if b==SESSION_IMPORT_TIPO:
                del actions['eliminar_fase']
                del actions['ver_tipositems']
                del actions['ver_list_proyecto']
                del actions['ver_item']
                #del actions['ver_importacion_tipo']
                del actions['ver_lineabase']
                del actions['ver_atributos']
                del actions['finalizar_fase']
                del actions['ver_antecesor']
                del actions['ver_importar_fase']

        else:
            del actions['ver_antecesor']
            del actions['ver_importacion_tipo']
            del actions['ver_importar_fase']

        return actions

    #Administracion de Items
    def ver_item(self, request, queryset):
        """
        Representa el administrador de items de una fase
        @param: request,queryset
        @xtype:
        @return: URL que redirecciona a la Administracion de item
        @rtype: HttpResponseRedirect
        """
        print("ver_item")
        print(request)
        request.session['proceso']=SESSION_VER_ITEM
        if len(queryset) == 1:
            for obj in queryset:
                proyecto_pk=request.session['id_proyecto']
                has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.LISTAR_ITEMS, obj.id)
                if has_perm:
                    request.session['id_fase'] = obj.id
                    #proyecto_pk=request.session['id_proyecto']
                    p=Proyectos.objects.get(id=proyecto_pk)
                    estado_proyecto=p.estado_id
                    if estado_proyecto==1:
                        message_bi = "No se puede listar los Items de la Fase debido a que no ha Iniciado el Proyecto"
                        self.message_user(request, message_bi)
                    else:
                        request.session['estado_proyecto']=estado_proyecto
                        request.session['estado_fase']=obj.estado.id
                        return HttpResponseRedirect("/admin/EPMApp/item/?id_fase=%s" % obj.id)
                else:
                    message_bi = "No posee permisos para listar los Items de la Fase"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione una sola fase"
            self.message_user(request, message_bi)
    ver_item.short_description = "Administracion de Items"

    #Administracion de Tipos Items
    def ver_tipositems(self, request, queryset):
        """
        Representa al administrador de tipos de items
        @param: request,queryset
        @xtype:
        @return: URL que redirecciona a la Administracion de Tipos de Items
        @rtype: HttpResponseRedirect
        """
        if len(queryset) == 1:
            for obj in queryset:
                proyecto_pk=request.session['id_proyecto']
                request.session['id_fase'] = obj.id
                has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.LISTAR_TIPO_ITEM, obj.id)
                if has_perm:
                    proyecto_pk=request.session['id_proyecto']
                    p=Proyectos.objects.get(id=proyecto_pk)
                    estado_proyecto=p.estado_id
                    if estado_proyecto==1:
                        message_bi = "No se puede listar los Tipos de Items de la Fase debido a que no ha Iniciado el Proyecto"
                        self.message_user(request, message_bi)
                    else:
                        request.session['estado_proyecto']=estado_proyecto
                        request.session['estado_fase']=obj.estado.id
                        return HttpResponseRedirect("/admin/EPMApp/tipositems/?id_fase=%s" % obj.id)
                else:
                    message_bi = "No posee permisos para listar los Tipos de Items de la Fase"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione una sola fase"
            self.message_user(request, message_bi)
    ver_tipositems.short_description = "Administracion de Tipos de Items"

    #Administracion de Lineas Base
    def ver_lineabase(self, request, queryset):
        """
        Metodo que representa el Administrador de Linea Base
        @return: URL
        """
        if len(queryset) == 1:
            for obj in queryset:
                proyecto_pk=request.session['id_proyecto']
                has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.LISTAR_LINEA_BASE, obj.id)
                request.session['id_fase']=obj.id
                if has_perm:
                    proyecto_pk=request.session['id_proyecto']
                    p=Proyectos.objects.get(id=proyecto_pk)
                    estado_proyecto=p.estado_id
                    if estado_proyecto==1:
                        message_bi = "No se puede listar las Lineas Base de la Fase debido a que no ha Iniciado el Proyecto"
                        self.message_user(request, message_bi)
                    else:
                        request.session['estado_proyecto']=estado_proyecto
                        request.session['estado_fase']=obj.estado.id
                        return HttpResponseRedirect("/admin/EPMApp/lineabase/?id_fase=%s" % obj.id)
                else:
                    message_bi = "No posee permisos para listar las Lineas Base de la Fase"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione una sola fase"
            self.message_user(request, message_bi)
    ver_lineabase.short_description = "Administracion de Lineas Base"

    #Eliminar Fase
    def eliminar_fase(self, request, queryset):
        """
        Metodo que elimina una Fase de un proyecto
        """
        if len(queryset)==1:
            for obj in queryset:
                request.session['id_fase']=obj.id
                estado=obj.estado.id

                proyecto_pk=request.session['id_proyecto']
                p=Proyectos.objects.get(id=proyecto_pk)
                estado_proyecto=p.estado_id

                has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.ELIMINAR_FASE, obj.id)
                if has_perm:
                    if estado_proyecto==1:
                        if estado==1:
                            rows_updated = queryset.update(estado=2)
                            if rows_updated == 1:
                                message_bit = "La Fase se ha eliminado"
                            self.message_user(request, "%s exitosamente." % message_bit)
                        if estado==2:
                            message_bi = "La Fase no puede eliminarse debido a que se ha Eliminado previamente"
                            self.message_user(request, message_bi)
                        if estado==3:
                            message_bi = "La Fase no puede eliminarse debido a que se encuentra en Desarrollo"
                            self.message_user(request, message_bi)
                        if estado==4:
                            message_bi = "La Fase no puede eliminarse debido a que se encuentra Finalizada"
                            self.message_user(request, message_bi)
                        if estado==5:
                            message_bi = "La Fase no puede eliminarse debido a que se encuentra Comprometida"
                            self.message_user(request, message_bi)
                        if estado==6:
                            message_bi = "La Fase no puede eliminarse debido a que se encuentra en Desarrollo Restringido"
                            self.message_user(request, message_bi)
                    if estado_proyecto==2:
                        message_bi = "La Fase no puede eliminarse debido a que el Proyecto ha Iniciado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==3:
                        message_bi = "La Fase no puede eliminarse debido a que el Proyecto se ha Cancelado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==4:
                        message_bi = "La Fase no puede eliminarse debido a que el Proyecto se ha Finalizado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==5:
                        message_bi = "La Fase no puede eliminarse debido a que el Proyecto se ha Eliminado"
                        self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para Eliminar la Fase"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione una sola fase"
            self.message_user(request, message_bi)
    eliminar_fase.short_description = "Eliminar Fase"

    #Finalizar Fase
    def finalizar_fase(self, request, queryset):
        """Finalizar Fase de un proyecto"""
        if len(queryset)==1:
            for obj in queryset:
                request.session['id_fase']=obj.id
                estado= obj.estado.id

                proyecto_pk=request.session['id_proyecto']
                p=Proyectos.objects.get(id=proyecto_pk)
                estado_proyecto=p.estado_id

                has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.FINALIZAR_FASE, obj.id)
                if has_perm:
                    if estado_proyecto==1:
                        message_bi = "La Fase no puede finalizarse debido a que el Proyecto no ha Iniciado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==2:
                        if estado==3:
                            items=Item.objects.filter(id_fase=obj.id)
                            cant_items=len(items)
                            cont=0

                            if items:
                                for i in items:
                                    if i.estado_id==7:
                                        cont=cont+1

                                if cont==cant_items:
                                    queryset.update(estado=4)
                                    fecha=date.today()
                                    print(fecha)
                                    queryset.update(fecha_finalizacion=fecha)
                                    message_bit = "La Fase ha finalizado exitosamente"
                                else:
                                    message_bit = "La Fase no puede Finalizar ya que sus items no estan en Linea Base"
                                self.message_user(request, message_bit)
                            else:
                                message_bit = "La Fase no puede Finalizar ya que no tien items"
                                self.message_user(request, message_bit)

                        if estado==1:
                            message_bi = "La Fase no puede finalizarse debido a que no ha pasado a Desarrollo"
                            self.message_user(request, message_bi)
                        if estado==2:
                            message_bi = "La Fase no puede finalizarse debido a que se encuentra Eliminada"
                            self.message_user(request, message_bi)
                        if estado==4:
                            message_bi = "La Fase no puede finalizarse debido a que ya ha sido Finalizada previamente"
                            self.message_user(request, message_bi)
                        if estado==5:
                            message_bi = "La Fase no puede finalizarse debido a que se encuentra Comprometida"
                            self.message_user(request, message_bi)
                        if estado==6:
                            message_bi = "La Fase no puede finalizarse debido a que se encuentra en Desarrollo Restringido"
                            self.message_user(request, message_bi)
                    if estado_proyecto==3:
                        message_bi = "La Fase no puede finalizarse debido a que el Proyecto se ha Cancelado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==4:
                        message_bi = "La Fase no puede finalizarse debido a que el Proyecto ha Finalizado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==5:
                        message_bi = "La Fase no puede finalizarse debido a que el Proyecto se ha Eliminado"
                        self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para Finalizar la Fase"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione una sola fase"
            self.message_user(request, message_bi)
    finalizar_fase.short_description = "Finalizar Fase"

    def get_changelist(self, request, **kwargs):
        """
        Configuraciones de la lista principal de fases
        @param: request,**kwargs
        @xtype:
        @return Lista de Fases
        """
        return FasesChangeList

    #Redirige despues de guardar
    def response_post_save_add(self, request, obj):
        """
        Redirige a la lista de fases de un proyecto despues de crear una fase
        @param: request,obj
        @xtype:
        @return: URL que redirecciona a la lista de fases de un proyecto, luego de crear una fase
        @rtype: HttpResponseRedirect
        """
        proyecto_pk=request.session['id_proyecto']
        return HttpResponseRedirect("/admin/EPMApp/fases/?id_proyecto=%s" %proyecto_pk)

    #Filtra el proyecto
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """Despliega el proyecto en el cual se desea agregar una fase
        @param: db_field, request, **kwargs
        @xtype: FasesAdmin
        """
        if db_field.name=="id_proyecto":
            parametros=request.session['id_proyecto']
            if parametros:
                proyecto_id= parametros
                p=Proyectos.objects.filter(id=proyecto_id)
                kwargs["queryset"]=p.filter()

        return super(FasesAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    #Campos a modificar
    def get_readonly_fields(self, request, obj=None):
        """Deshabilita los campos a modificar si un proyecto o la fase ya no se encuentra en estado creado
        @param: request, obj=None
        @xtype:
        """
        proyecto_pk= request.session['id_proyecto']
        p=Proyectos.objects.get(id=proyecto_pk)
        estado_proyecto=p.estado_id

        if obj == None:
            has_perm = utils.has_project_permission(request.user.id, proyecto_pk, configuraciones.AGREGAR_FASES)
            if has_perm:
                request.session['estado_fase']=None
                return  ('')
            else:
                message_bi = "No posee permisos para Crear una Fase"
                self.message_user(request, message_bi)

        if obj != None:
            estado = obj.estado.id

            #Estado Creado del proyecto
            if estado_proyecto==1:
                #Estado Creado de la fase
                if estado==1:
                    has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.MODIFICAR_FASE, obj.id)
                    if has_perm:
                        request.session['estado_fase']=estado
                        return  ('')
                    else:
                        message_bi = "No posee permisos para Modificar Fases"
                        self.message_user(request, message_bi)
                        request.session['estado_fase']='sin_permiso'
                        return ('nombre', 'descripcion', 'id_proyecto')

                #Estados de la fase
                if estado==2 :
                    request.session['estado_fase']=estado
                    return ('nombre', 'descripcion', 'id_proyecto')
                if estado==3:
                    request.session['estado_fase']=estado
                    return ('nombre', 'descripcion', 'id_proyecto')
                if estado==4:
                    request.session['estado_fase']=estado
                    return ('nombre', 'descripcion', 'id_proyecto')
                if estado==5:
                    request.session['estado_fase']=estado
                    return ('nombre', 'descripcion', 'id_proyecto')

            #Estados del proyecto
            if estado_proyecto==2:
                return ('nombre', 'descripcion', 'id_proyecto')
            if estado_proyecto==3:
                return ('nombre', 'descripcion', 'id_proyecto')
            if estado_proyecto==4:
                return ('nombre', 'descripcion', 'id_proyecto')
            if estado_proyecto==5:
                return ('nombre', 'descripcion', 'id_proyecto')

    def has_change_permission(self, request, obj=None):
        id_proyecto=None
        id_fase=None
        try:
            id_proyecto=request.session['id_proyecto']
            #id_fase=request.session['id_fase']
        except:
            print("No se encontro el id")
        has_perm=utils.has_project_permission(request.user.id,id_proyecto,configuraciones.LISTAR_FASES)
        return has_perm

    def has_add_permission(self, request, obj=None):
        id_proyecto=None
        id_fase=None
        try:
            id_proyecto=request.session['id_proyecto']
            #id_fase=request.session['id_fase']
        except:
            print("No se encontro el id")
        has_perm=utils.has_project_permission(request.user.id,id_proyecto,configuraciones.AGREGAR_FASES)
        return has_perm

class FasesChangeList(ChangeList):
    """
    Clase que administra la lista de cambio de fases
    """
    def __init__(self, *args, **kwargs):
        super(FasesChangeList, self).__init__(*args, **kwargs)
        self.title = "Seleccione una Fase y la operacion a realizar"

