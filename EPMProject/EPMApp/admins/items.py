from datetime import date
from django.contrib import admin
from django.contrib.admin.views.main import ChangeList
from django.db.models import Q
from EPMApp import utils
from EPMApp import configuraciones

from django.http import HttpResponseRedirect
from django import forms
import networkx as nx
import matplotlib
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from EPMApp.models import Fases, Item, RelacionPadre, RelacionAntecesor, EstadosItem, TiposItems, SolicitudItems, Proyectos, LineaBase, AtributoItem
#from EPMApp.models import Fases, Item, RelacionPadre, RelacionAntecesor, EstadosItem, TiposItems, SolicitudItems, Proyectos, LineaBase
from django.template import RequestContext
from django.shortcuts import render_to_response

SESSION_ANTECESOR_FASE = 1
SESSION_VER_ITEM = 4
SESION_LISTAR_VERSIONES = 5
SESSION_RELA_PADRE = 6
SESSION_RELA_ANTECESOR = 7
SESSION_IMPACTO = 8
SESSION_FALSE_ANTECESOR = 9
SESSION_VER_PADRE = 10
SESSION_VER_ANTECESOR = 11

vector = []


class ItemAdminForm(forms.ModelForm):
    class Meta:
        model = Item


def dependencia_delante(rela_antecesor):
    """
     Metodo que obtiene el item antecesor y sus relaciones, asi como las complejidades de las mismas
     @param : rela_antecesor
     @return: retorna la suma de las complejidades que implica cada relacion
     @rtype: Integer
    """
    item = rela_antecesor[0].item_antecesor  #item que representa al item antecesor
    complejidad_antecesor = item.complejidad  #complejidad del item antecesor
    item_actual = item.pk
    vector.append(item_actual)
    rela_padre = RelacionPadre.objects.filter(item_hijo_id=item_actual)  #obtiene la relacion en la cual es hijo
    rela_hijo = RelacionPadre.objects.filter(item_padre_id=item_actual)  #obtiene la relacion en la cual es padre
    rela_antecesor = RelacionAntecesor.objects.filter(
        item_sucesor_id=item_actual)  #obtiene la relacion en la cual es sucesor
    if not rela_padre and not rela_hijo and not rela_antecesor:
        return complejidad_antecesor
    else:
        if rela_hijo and rela_antecesor and not rela_padre:
            compl_hijo = dependencia_medio(rela_hijo)
            compl_antecesor = dependencia_delante(rela_antecesor)
            compl = compl_hijo + compl_antecesor + complejidad_antecesor
            return compl
        if rela_hijo and rela_padre and not rela_antecesor:
            compl_hijo = dependencia_medio(rela_hijo)
            compl_padre = dependencia_padre(rela_padre)
            compl = complejidad_antecesor + compl_hijo + compl_padre
            return compl
        if rela_antecesor and rela_padre and not rela_hijo:
            compl_antecesor = dependencia_delante(rela_antecesor)
            compl_padre = dependencia_padre(rela_padre)
            compl = complejidad_antecesor + compl_antecesor + compl_padre
            return compl
        if rela_hijo:
            return complejidad_antecesor + dependencia_medio(rela_hijo)
        if rela_padre:
            return complejidad_antecesor + dependencia_padre(rela_padre)
        if rela_antecesor:
            return complejidad_antecesor + dependencia_delante(rela_antecesor)


def dependencia_medio(rela_hijo):
    """
     Metodo que obtiene el item hijo y sus relaciones, asi como las complejidades de las mismas
     @param : rela_hijo
     @return: retorna la suma de las complejidades que implica cada relacion
     @rtype: Integer
    """
    item = rela_hijo[0].item_hijo  #item que representa el hijo
    complejidad_hijo = item.complejidad  #complejidad del hijo
    item_actual = item.pk
    vector.append(item_actual)
    rela_hijo = RelacionPadre.objects.filter(item_padre_id=item_actual)  #obtiene la relacion en la cual es padre
    rela_antecesor = RelacionAntecesor.objects.filter(
        item_sucesor_id=item_actual)  #obtiene la relacion en la cual es sucesor
    rela_sucesor = RelacionAntecesor.objects.filter(
        item_antecesor_id=item_actual)  #obtiene la relacion en la cual es antecesor
    if not rela_hijo and not rela_antecesor and not rela_sucesor:
        return complejidad_hijo
    else:
        if rela_hijo and rela_antecesor and not rela_sucesor:
            compl_hijo = dependencia_medio(rela_hijo)
            compl_antecesor = dependencia_delante(rela_antecesor)
            compl = complejidad_hijo + compl_hijo + compl_antecesor
            return compl
        if rela_hijo and rela_sucesor and not rela_antecesor:
            compl_hijo = dependencia_medio(rela_hijo)
            compl_suce = dependencia_suce(rela_sucesor)
            compl = complejidad_hijo + compl_hijo + compl_suce
            return compl
        if rela_antecesor and rela_sucesor and not rela_hijo:
            compl_antecesor = dependencia_delante(rela_antecesor)
            compl_suce = dependencia_suce(rela_sucesor)
            compl = complejidad_hijo + compl_antecesor + compl_suce
            return compl
        if rela_hijo:
            return complejidad_hijo + dependencia_medio(rela_hijo)
        if rela_antecesor:
            return complejidad_hijo + dependencia_delante(rela_antecesor)
        if rela_sucesor:
            return complejidad_hijo + dependencia_suce(rela_sucesor)

def dependencia_padre(rela_hijo):
    """
     Metodo que obtiene el item padre y sus relaciones, asi como las complejidades de las mismas
     @param : rela_padre
     @return: retorna la suma de las complejidades que implica cada relacion
     @rtype: Integer
    """
    item = rela_hijo[0].item_padre  #item que representa al item padre
    complejidad_padre = item.complejidad  # representa la complejidad del padre
    item_actual = item.pk
    vector.append(item_actual)
    rela_antecesor = RelacionAntecesor.objects.filter(
        item_sucesor_id=item_actual)  #obtiene la relacion en la cual es sucesor
    rela_sucesor = RelacionAntecesor.objects.filter(
        item_antecesor_id=item_actual)  #obtiene la relacion en la cual es antecesor
    rela_padre = RelacionPadre.objects.filter(item_hijo_id=item_actual)  #obtiene la relacion en la cual es hijo
    if not rela_antecesor and not rela_sucesor and not rela_padre:
        return complejidad_padre
    else:
        if rela_antecesor and rela_sucesor and not rela_padre:
            compl_ante = dependencia_delante(rela_antecesor)
            compl_suce = dependencia_suce(rela_sucesor)
            compl = complejidad_padre + compl_ante + compl_suce
            return compl
        if rela_antecesor and rela_padre and not rela_sucesor:
            compl_ante = dependencia_delante(rela_antecesor)
            compl_padre = dependencia_padre(rela_padre)
            compl = complejidad_padre + compl_ante + compl_padre
            return compl
        if rela_sucesor and rela_padre and not rela_antecesor:
            compl_suce = dependencia_suce(rela_sucesor)
            compl_padre = dependencia_padre(rela_padre)
            compl = complejidad_padre + compl_suce + compl_padre
            return compl

        if rela_antecesor:
            return complejidad_padre + dependencia_delante(rela_antecesor)
        if rela_padre:
            return complejidad_padre + dependencia_padre(rela_padre)
        if rela_sucesor:
            return complejidad_padre + dependencia_suce(rela_sucesor)


def dependencia_suce(rela_suce):
    """
     Metodo que obtiene el item sucesor y sus relaciones, asi como las complejidades de las mismas
     @param : rela_sucesor
     @return: retorna la suma de las complejidades que implica cada relacion
     @rtype: Integer
    """
    item = rela_suce[0].item_sucesor  #item que representa al item sucesor
    complejidad_suce = item.complejidad  #complejidad del item sucesor
    item_actual = item.pk
    vector.append(item_actual)
    rela_sucesor = RelacionAntecesor.objects.filter(
        item_antecesor_id=item_actual)  #obtiene la relacion en la cual es antecesor
    rela_padre = RelacionPadre.objects.filter(item_hijo_id=item_actual)  #obtiene la relacion en la cual es hijo
    rela_hijo = RelacionPadre.objects.filter(item_padre_id=item_actual)  #obtiene la relacion en la cual es padre
    if not rela_sucesor and not rela_padre and not rela_hijo:
        return complejidad_suce
    else:
        if rela_sucesor and rela_padre and not rela_hijo:
            compl_suce = dependencia_suce(rela_sucesor)
            compl_padre = dependencia_padre(rela_padre)
            compl = complejidad_suce + compl_suce + compl_padre
            return compl
        if rela_sucesor and rela_hijo and not rela_padre:
            compl_suce = dependencia_suce(rela_sucesor)
            compl_hijo = dependencia_medio(rela_hijo)
            compl = complejidad_suce + compl_suce + compl_hijo
            return compl
        if rela_hijo and rela_padre and not rela_sucesor:
            compl_hijo = dependencia_medio(rela_hijo)
            compl_padre = dependencia_padre(rela_padre)
            compl = complejidad_suce + compl_hijo + compl_padre
            return compl
        if rela_sucesor:
            return complejidad_suce + dependencia_suce(rela_sucesor)
        if rela_padre:
            return complejidad_suce + dependencia_padre(rela_padre)
        if rela_hijo:
            return complejidad_suce + dependencia_medio(rela_hijo)

def obtener_impacto(item):

    rela_padre = RelacionPadre.objects.filter(item_hijo_id=item.id)  #obtiene la relacion en la cual es hijo
    rela_hijo = RelacionPadre.objects.filter(item_padre_id=item.id)  #obtiene la relacion en la cual es padre
    rela_antecesor = RelacionAntecesor.objects.filter(item_sucesor_id=item.id)  #obtiene la relacion en la cual es sucesor
    rela_sucesor = RelacionAntecesor.objects.filter(item_antecesor_id=item.id)  #obtiene la relacion en la cual es antecesor
    complejidad_actual = item.complejidad  #complejidad del item sobre el cual se calculara el impacto
    item_actual = item.id
    vector.append(item_actual)  # almacena el id del item actual en el vector
    complejidad_atras = 0
    complejidad_padre = 0
    complejidad_delante = 0
    complejidad_hijo = 0
    if rela_antecesor:
        complejidad_delante = dependencia_delante(rela_antecesor)

    if rela_sucesor:
        complejidad_atras = dependencia_suce(rela_sucesor)

    if rela_hijo:
        complejidad_padre = dependencia_medio(rela_hijo)

    if rela_padre:
        complejidad_hijo = dependencia_padre(rela_padre)
    #print("Los elementos son: ",vector) # imprime los item involucrados en el calculo de impacto
    return complejidad_atras + complejidad_padre + complejidad_actual + complejidad_delante + complejidad_hijo

def obtener_items(item):
    """
         Obtiene la lista de items relacionados entre si
         @return: vector con los items relacionados
         @rtype:
        """
    complejidad_total = obtener_impacto(item)
    return vector




class ItemAdmin(admin.ModelAdmin):
    """Representa el administrador de Items del sistema"""
    fields = ('nombre', 'descripcion', 'id_fase', 'tipo_item', 'complejidad', 'archivo')
    list_display = ('nombre', 'descripcion', 'version', 'estado')
    form = ItemAdminForm
    actions = ['confirmar_item', 'aprobar_item', 'listar_versiones_item',
               'revertir_item', 'revivir_item', 'eliminar_item', 'historial_item', 'calculo_impacto', 'ver_padre',
               'ver_antecesor_fase', 'ver_relacion_antecesor', 'ver_atributos']


    def calculo_impacto(self, request, queryset):
        """
        Metodo que calcula el impacto que implica el costo de la modificacion de un item
        @param: request,queryset
        @xtype:
        @return: el calculo de impacto del item seleccionado
        @rtype: Integer
        """
        proyecto_pk= request.session['id_proyecto']
        fase_pk=request.session['id_fase']

        request.session['proceso'] = SESSION_IMPACTO
        if len(queryset) == 1:
            for obj in queryset:
                has_perm =utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.CALCULAR_IMPACTO, fase_pk)
                if has_perm:
                    try:
                        request.session['item_pk'] = obj.id
                    except:
                        print("No se pudo obtener el ID")
                    item = Item.objects.get(pk=obj.id)  #obtiene el item actual
                    estado_item = obj.estado.id

                    complejidad_total = obtener_impacto(item)
                    item.calculo_impacto = complejidad_total
                    item.save()
                    generar_grafo_item(request,item)
                    message_bi = "El impacto del item es:%s" % complejidad_total
                    self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para Calcular el Impacto"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo item"
            self.message_user(request, message_bi)

    calculo_impacto.short_description = "Calcular Impacto"

    def ver_grafo(self, request, queryset):
        """
        Despliega las lista de los miembros del comite de la solicitud de cambio con sus correspondientes votos
        @return: Lista de los usuarios miembros del comite
        @rtype: HttpResponseRedirect
        """

        if len(queryset) == 1:
            for obj in queryset:

               # return HttpResponseRedirect("/admin/EPMApp/votaciones/")
                item = Item.objects.get(pk=obj.id)
                MG = nx.MultiDiGraph()
                print item
                context = RequestContext(request)
                #lista_items=[item.id]
                #lista_items = list(set(lista_items))

                #get_items(id_item,item.version,lista_items)

                lista_items = obtener_items(item)

                print lista_items
                #crear nodos
                i = 0


                x = 0
                labels = {}

                for item_id in lista_items:
                    x = x + 1
                    print("id del item", item_id)
                    labels[item_id] = item_id
                    print("labels", labels)

                    MG.add_node(item_id, pos=(item_id, x))




                color_list = []
                for node in nx.nodes(MG):



                    item = Item.objects.get(id=node)

                    color_list.append(item.id_fase.id)

                pos = nx.get_node_attributes(MG, 'pos')

                #pos = nx.circular_layout(MG)
                nx.draw_networkx_nodes(MG, pos=pos, node_color=color_list, node_size=1500)



                nx.draw_networkx_labels(MG, pos=pos, labels=labels)




                #crear arcos
                edge_labels = []

                for item_id in lista_items:

                        antecesores_padres = RelacionAntecesor.objects.filter(item_sucesor_id=item_id)

                        for ap in antecesores_padres:
                            item_origen = ap.item_antecesor

                            item_destino = ap.item_sucesor
                            edge = MG.add_edge(item_origen.id, item_destino.id)
                            edge_labels.append(((item_origen.id, item_destino.id), "Antecesor-Sucesor"))


                        padre_hijo=RelacionPadre.objects.filter(item_hijo_id=item_id)

                        for ap in padre_hijo:
                            item_origen = ap.item_padre

                            item_destino = ap.item_hijo
                            edge = MG.add_edge(item_origen.id, item_destino.id)
                            edge_labels.append(((item_origen.id, item_destino.id), "Padre-Hijo"))



                #endfor
                print pos
                print edge_labels

                nx.draw_networkx_edges(MG, pos=pos)


                #agregar etiquetas a los arcos
                edge_labels = dict(edge_labels)

                nx.draw_networkx_edge_labels(MG, pos=pos, edge_labels=edge_labels)




                image_path = "/home/mai/IS2/Repositorio/codigo/epm/EPMProject/files/" + item.nombre+".png"



                #verificar que no existan conflictos de nombres
                plt.savefig(image_path)

                plt.show()


                MG.clear()
                plt.clf()
                return render_to_response('ver_grafo_relaciones.html', {'image_name': image_path} , context)

        else:
            message_bi = "Seleccione una sola solicitud"
            self.message_user(request, message_bi)

    ver_grafo.short_description = "Visualizar grafo"

    # def obtener_impacto(self, item):
    #
    #     rela_padre = RelacionPadre.objects.filter(item_hijo_id=item.id)  #obtiene la relacion en la cual es hijo
    #     rela_hijo = RelacionPadre.objects.filter(item_padre_id=item.id)  #obtiene la relacion en la cual es padre
    #     rela_antecesor = RelacionAntecesor.objects.filter(
    #         item_sucesor_id=item.id)  #obtiene la relacion en la cual es sucesor
    #     rela_sucesor = RelacionAntecesor.objects.filter(
    #         item_antecesor_id=item.id)  #obtiene la relacion en la cual es antecesor
    #     complejidad_actual = item.complejidad  #complejidad del item sobre el cual se calculara el impacto
    #     item_actual = item.id
    #     vector.append(item_actual)  # almacena el id del item actual en el vector
    #     complejidad_atras = 0
    #     complejidad_padre = 0
    #     complejidad_delante = 0
    #     complejidad_hijo = 0
    #     if rela_antecesor:
    #         complejidad_delante = dependencia_delante(rela_antecesor)
    #
    #     if rela_sucesor:
    #         complejidad_atras = dependencia_suce(rela_sucesor)
    #
    #     if rela_hijo:
    #         complejidad_padre = dependencia_medio(rela_hijo)
    #
    #     if rela_padre:
    #         complejidad_hijo = dependencia_padre(rela_padre)
    #     #print("Los elementos son: ",vector) # imprime los item involucrados en el calculo de impacto
    #     return complejidad_atras + complejidad_padre + complejidad_actual + complejidad_delante + complejidad_hijo


    #     def ver_grafo_relaciones(request, id_proyecto):
    #     """
    #     Vista para crear el grafo de relaciones entre items de un proyecto dado
    #     """
    #     context = RequestContext(request)
    #     fases = Fase.objects.filter(proyecto=id_proyecto)
    #     MG = nx.MultiDiGraph()
    #
    #     #crear nodos
    #     i = 0
    #
    #     x = 0
    #     labels = {}
    #     for fase in fases:
    #         items = Item.objects.filter(fase_id=fase.id).exclude(estado='ELIM')
    #         print items
    #         i = i + 1
    #         for item in items:
    #             x = x + 1
    #             labels[item.id] = item.id
    #             MG.add_node(item.id, pos=(item.id, x))
    #
    #     color_list = []
    #     for node in nx.nodes(MG):
    #         item = Item.objects.get(id=node)
    #         color_list.append(item.fase_id)
    #     pos = nx.get_node_attributes(MG, 'pos')
    #     pos = nx.circular_layout(MG)
    #     nx.draw_networkx_nodes(MG, pos=pos, node_color=color_list, node_size=1500)
    #     nx.draw_networkx_labels(MG, pos=pos, labels=labels)
    #
    #     #crear arcos
    #     edge_labels = []
    #     for fase in fases:
    #         items = Item.objects.filter(fase_id=fase.id).exclude(estado='ELIM')
    #         for item in items:
    #             antecesores_padres = relaciones.objects.filter(item_destino_id=item.id, item_destino_version=item.version).exclude(activo=False)
    #             for ap in antecesores_padres:
    #                 item_origen = ap.item_origen
    #                 item_destino = ap.item_destino
    #                 edge = MG.add_edge(item_origen.id, item_destino.id)
    #                 edge_labels.append(((item_origen.id, item_destino.id), ap.tipo_relacion))
    #     #endfor
    #     nx.draw_networkx_edges(MG, pos=pos)
    #
    #     #agregar etiquetas a los arcos
    #     edge_labels = dict(edge_labels)
    #     nx.draw_networkx_edge_labels(MG, pos=pos, edge_labels=edge_labels)
    #
    #     image_path = os.path.join(RUTA_PROYECTO,"static/grafos/image.png")
    #     print image_path
    #     #verificar que no existan conflictos de nombres
    #     plt.savefig(image_path)
    #     #plt.show()
    #
    #     itemlist = []
    #     for fase in fases:
    #         items = Item.objects.filter(fase_id=fase.id)
    #         for item in items:
    #             itemlist.append(item)
    #
    #     items = Item.objects.filter(fase__proyecto_id=id_proyecto)
    #     itemlist = ListaItemTable(items)
    #     proy_nombre = Proyecto.objects.get(id=id_proyecto).nombre
    #     RequestConfig(request, paginate={"per_page": 5}).configure(itemlist)
    #     return render_to_response('ver_grafo_relaciones.html', {'image_name': "image.png", 'lista': itemlist,
    #                                                             'id_proyecto': id_proyecto, 'proy_nombre': proy_nombre},
    #                               context)
    #
    #
    #
    # #Para verificar que no haya ciclos en el grafo:
    # def es_consistente(id_fase, nodelist=None):
    #     """
    #     Implementacion del algoritmo de busqueda de ciclos
    #     Return True is there is no cicle in the graph.
    #     """
    #     MG = nx.MultiDiGraph()
    #     if nodelist is None:
    #         items = Item.objects.filter(fase_id=id_fase).exclude(estado='ELIM')
    #     else:
    #         items=nodelist
    #
    #     for item in items:
    #         MG.add_node(item.id, pos=(item.id, item.id))
    #         padres = relaciones.objects.filter(item_destino_id=item.id, item_destino_version=item.version,
    #                                            tipo_relacion='HIJ').exclude(activo=False)
    #         for ap in padres:
    #             item_origen = ap.item_origen
    #             item_destino = ap.item_destino
    #             MG.add_edge(item_origen.id, item_destino.id)
    #
    #     if nx.simple_cycles(MG).__len__() == 0:
    #         return True
    #     else:
    #         return False


    # def ver_relacion_padre(self, request, queryset):
    #     """
    #     Despliega la lista de las relaciones Padre-Hijo del item seleccionado
    #     @return: La lista de las relaciones Padre-Hijo del item
    #     @rtype: HttpResponseRedirect
    #     """
    #     request.session['proceso']=SESSION_VER_PADRE
    #     if len(queryset)==1:
    #         for obj in queryset:
    #             try:
    #                 request.session['id_item']=obj.id
    #             except:
    #                 print("No se pudieron obtener los IDs")
    #
    #             #relacionPadre=RelacionPadre.objects.filter(item_padre_id=obj.id) #relacion donde es padre
    #             return HttpResponseRedirect("/admin/EPMApp/relacionpadre/")
    #
    #     else:
    #         message_bi = "Seleccione un solo item"
    #         self.message_user(request, message_bi)
    # ver_relacion_padre.short_description = "Ver Relacion Padre-Hijo"

    def ver_relacion_antecesor(self, request, queryset):
        """
        Despliega la lista de las relaciones Antecesor-Sucesor del item seleccionado
        @return: La lista de las relaciones del item
        @rtype: HttpResponseRedirect
        """
        request.session['proceso'] = SESSION_VER_ANTECESOR
        if len(queryset) == 1:
            for obj in queryset:

                try:
                    request.session['id_item'] = obj.id
                except:
                    print("No se pudieron obtener los IDs")

                proyecto_pk=request.session['id_proyecto']
                fase_pk=request.session['id_fase']

                has_perm =utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.LISTAR_RELACIONES, fase_pk)

                if has_perm:
                    try:
                        request.session['id_item']=obj.id
                    except:
                        print("No se pudieron obtener los IDs")


                    return HttpResponseRedirect("/admin/EPMApp/relacionantecesor/")
                else:
                    message_bi = "No posee permisos para listar Relaciones Antecesor-Sucesor"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo item"
            self.message_user(request, message_bi)

    ver_relacion_antecesor.short_description = "Ver Relacion Antecesor-Sucesor"

    def listar_versiones_item(self, request, queryset):
        """
        Despliega la lista de las versiones del item seleccionado
        @param: request,queryset
        @xtype:
        @return: URL que redirecciona a la lista de las versiones del item seleccionado
        @rtype: HttpResponseRedirect
        """
        request.session['proceso'] = SESION_LISTAR_VERSIONES
        if len(queryset) == 1:
            for obj in queryset:

                proyecto_pk= request.session['id_proyecto']
                p=Proyectos.objects.get(id=proyecto_pk)
                estado_proyecto=p.estado_id

                fase_pk=request.session['id_fase']
                f=Fases.objects.get(id=fase_pk)
                estado_fase=f.estado_id

                estado=obj.estado.id

                has_perm =utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.REVERTIR_ITEM, fase_pk)
                if has_perm:
                    try:
                        request.session['cod_item']=obj.item_version
                        request.session['item_pk']=obj.id
                        request.session['version']=obj.version
                    except:
                        print("No se pudieron obtener los IDs")

                    if estado_proyecto==1:
                        message_bi = "El Item no puede Revertise ya que el Proyecto no ha Iniciado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==2:
                        if estado_fase==1:
                            message_bi = "El item no puede Revertise ya que la Fase no ha Iniciado"
                            self.message_user(request, message_bi)
                        if estado_fase==2:
                            message_bi = "El item no puede Revertise ya que la Fase se ha Eliminado"
                            self.message_user(request, message_bi)
                        if estado_fase==3:
                            if estado==1:
                                return HttpResponseRedirect("/admin/EPMApp/item/?id_fases=%s" %obj.id)
                            if estado==2:
                                message_bi="El item no puede eliminarse debido a que se encuentra en Desarrollo Restringido"
                                self.message_user(request,message_bi)
                            if estado==3:
                                message_bi="El item no puede Revertise debido a que se encuentra Pendiente"
                                self.message_user(request,message_bi)
                            if estado==4:
                                message_bi="El item no puede Revertise debido a que se encuentra Aprobado con modificacion"
                                self.message_user(request,message_bi)
                            if estado==5:
                                message_bi="El item no puede Revertise debido a que se encuentra Aprobado sin modificacion"
                                self.message_user(request,message_bi)
                            if estado== 6:
                                message_bi= "El item no puede Revertise debido a que ha sido eliminado y no se puede realizar la operacion de Revertir"
                                self.message_user(request,message_bi)
                            if estado==7:
                                message_bi="El item no puede Revertise debido a que se encuentra Bloqueado"
                                self.message_user(request,message_bi)
                            if estado==8:
                                message_bi="El item no puede Revertise debido a que se encuentra en Revision"
                                self.message_user(request,message_bi)
                            elif estado==9:
                                message_bi="El item no puede Revertise debido a que se encuentra Pendiente..."
                                self.message_user(request,message_bi)
                            else:
                                message_bi = "El item no puede Revertise"
                                self.message_user(request, message_bi)
                        if estado_fase==4:
                            message_bi = "El Item item no puede Revertise ya que la Fase ha Finalizado"
                            self.message_user(request, message_bi)
                        if estado_fase==5:
                            message_bi = "El Item no puede Revertise ya que la Fase se encuentra Comprometida"
                            self.message_user(request, message_bi)
                        if estado_fase==6:
                            message_bi = "El item no puede Revertise debido adebido a que la Fase se encuentra en Desarrollo Restringido"
                            self.message_user(request, message_bi)
                    if estado_proyecto==3:
                        message_bi = "El Item no puede puede Revertise ya que el Proyecto se ha Cancelado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==4:
                        message_bi = "El Item no puede puede Revertise ya que el Proyecto ha Finalizado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==5:
                        message_bi = "El Item no puede puede Revertise ya que el Proyecto se ha Eliminado"
                        self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para Revertir Item"
                    self.message_user(request, message_bi)

        else:
            message_bi = "Seleccione un solo item"
            self.message_user(request, message_bi)

    listar_versiones_item.short_description = "Revertir Item"

    def revertir_item(self, request, queryset):
        """
        Revierte el item a una version anterior
        @param: request,queryset
        @xtype:
        @return: URL que redirecciona a la lista de relaciones del item
        @rtype: HttpResponseRedirect
        """
        try:
            proyecto_pk=request.session['id_proyecto']
            fase_pk = request.session['id_fase']
            version_anterior = request.session['version']
        except:
            print("No se pudieron obtener los Ids")
        version = 0

        if len(queryset)==1:
            for obj in queryset:
                has_perm =utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.REVERTIR_ITEM, fase_pk)
                if has_perm:
                    request.session['id_item']=obj.id
                    item=Item.objects.get(pk=obj.id)
                    print("El item vale: ",obj.id)
                    id_rela=RelacionPadre.item_hijo=obj.id
                    relacionPadre=RelacionPadre.objects.filter(item_hijo_id=obj.id)

                    relacionAntecesor=RelacionAntecesor.objects.filter(item_sucesor_id=obj.id)
                    print("Relacion Padre: ", relacionPadre)
                    estado_item=obj.estado.id
                    if estado_item == 6:
                        message_bi= "El item seleccionado ha sido eliminado y no se puede realizar la operacion de Revertir"
                        self.message_user(request,message_bi)
                    else:
                        if item:
                            item_nuevo=Item(nombre=item.nombre,descripcion=item.descripcion,item_version=item.item_version,
                                            tipo_item=item.tipo_item)
                            item_nuevo.id_fase=Fases.objects.get(pk=fase_pk)
                            item_nuevo.version=version_anterior+1
                            item_nuevo.save()
                            print("Item nuevo:", item_nuevo.id)
                            try:
                                request.session['item_revertido']=item_nuevo.id
                            except:
                                print("Error")

                            if relacionPadre:
                                item_padre = Item.objects.filter(item_version=relacionPadre[0].item_padre.item_version).order_by('-id')[0]
                                print("Item?padre: ",item_padre)
                                print("Estado padre:", item_padre.estado.id)
                                if item_padre:
                                    #si el item padre aun esta activo
                                    if not item_padre.estado.id == 6:
                                        request.session['proceso'] = SESSION_RELA_PADRE
                                        message_bi = "El item ha sido revertido con su relacion padre."
                                        self.message_user(request, message_bi)
                                        return HttpResponseRedirect("/admin/EPMApp/relacionpadre/?id_item=%s" %obj.id)
                                    else:
                                        message_bi = "El item ha sido revertido sin su relacion padre debido a que el item padre ha sido eliminado. Por favor verifique la relacion y vuelva a crearla de ser necesario."
                                        self.message_user(request, message_bi)
                            elif relacionAntecesor:
                                item_antecesor=Item.objects.filter(item_version=relacionAntecesor[0].item_antecesor.item_version).order_by('-id')[0]
                                print("ItemAntecesor:",item_antecesor.estado.id)
                                if item_antecesor:
                                    if not item_antecesor.estado.id==6:
                                        request.session['proceso']=SESSION_RELA_ANTECESOR
                                        message_bi = "El item ha sido revertido con su relacion padre."
                                        self.message_user(request, message_bi)

                                        return HttpResponseRedirect("/admin/EPMApp/relacionantecesor/?id_item=%s" %obj.id)
                                    else:
                                        message_bi = "El item ha sido revertido sin su relacion padre debido a que el item padre ha sido eliminado. Por favor verifique la relacion y vuelva a crearla de ser necesario."
                                        self.message_user(request, message_bi)
                            message_bit = "Se ha revertido el item"
                            self.message_user(request, "%s exitosamente." % message_bit)
                        else:
                            message_bi = "No se ha podido revertir el item"
                            self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para Revertir Item"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo item"
            self.message_user(request, message_bi)

    revertir_item.short_description = "Revertir"

    def ver_padre(self, request, queryset):
        """
        Metodo que representa la relacion Padre-Hijo
        @param: request,queryset
        @xtype:
        @return: URL que redirecciona al administrador de relacion padre
        @rtype: HttpResponseRedirect
        """
        if len(queryset) == 1:
            for obj in queryset:
                proyecto_pk=request.session['id_proyecto']
                fase_pk=request.session['id_fase']

                has_perm =utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.LISTAR_RELACIONES, fase_pk)
                if has_perm:
                    request.session['id_item'] = obj.id
                    return HttpResponseRedirect("/admin/EPMApp/relacionpadre/")
                else:
                    message_bi = "No posee permisos para Listar Relacion Padre-Hijo"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo item"
            self.message_user(request, message_bi)

    ver_padre.short_description = "Administrar Relacion Padre-Hijo"

    def ver_antecesor_fase(self, request, queryset):
        """
        Representa la relacion Antecesor-Sucesor
        @param: request,queryset
        @xtype:
        @return: URL que redirecciona a la lista de fases anteriores perteneciente al item seleccionado
        @rtype: HttpResponseRedirect
        """
        valor=0
        fase_pk = request.session['id_fase']
        proyecto_pk = request.session['id_proyecto']
        request.session['proceso'] = SESSION_ANTECESOR_FASE
        print("Id Fase")
        print(fase_pk)

        if len(queryset)==1:
            has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.AGREGAR_RELACION_AS, fase_pk)
            if has_perm:
                fases=Fases.objects.filter(id_proyecto=proyecto_pk)
                for fase in fases:
                   menor=fase.id
                   if fase_pk>menor:
                       valor=1
                if valor == 1:
                    for obj in queryset:
                        request.session['id_item'] = obj.id
                        item_actual = obj.id
                        # Comprueba si el item esta en una solicitud de cambio y que la misma este en estado pendiente
                        # Si la solicitud se encuentra en estado pendiente, no se podra agregar ni eliminar relacion al item
                        sol_items = SolicitudItems.objects.filter(
                            items=item_actual)  #objeto de la tabla intermedia de solicitud de cambio
                        solicitud = None  #en caso de que no existe una solicitud
                        rela_antecesor = RelacionAntecesor.objects.filter(
                            item_sucesor_id=item_actual)  #obtiene la relacion en la cual es sucesor
                        for i in sol_items:
                            solicitud = i.solicitud  #solicitud de cambio del item actual
                            if solicitud:
                                estado = solicitud.estado.id
                                if estado == 3:  #si la solicitud de cambio ya ha sido confirmada y esta en espera de ser procesada
                                    message_bi = "No se puede realizar la accion ya que el item se encuentra en una solicitud de cambio en espera de ser procesada"
                                    self.message_user(request, message_bi)
                                else:
                                    return HttpResponseRedirect("/admin/EPMApp/fases/")
                            elif not solicitud and rela_antecesor:  #si el item no esta en una solicitud de cambio y ya tiene un padre
                                message_bi = "El item ya posee un antecesor, si desea conocer la relacion seleccione la opcion Ver Relacion Antecesor-Sucesor"
                                self.message_user(request, message_bi)
                            else:
                                return HttpResponseRedirect("/admin/EPMApp/fases/")
                else:
                    request.session['proceso'] = SESSION_FALSE_ANTECESOR
                    message_bi = "No existe una fase antecesora para la relacion"
                    self.message_user(request, message_bi)
                    #return HttpResponseRedirect("/admin/EPMApp/fases/?id_proyecto=%s"%proyecto_pk)
            else:
                message_bi = "No posee permisos para Crear Relacion Antecesor-Sucesor"
                self.message_user(request, message_bi)

        else:
            message_bi = "Seleccione un solo item"
            self.message_user(request, message_bi)

    ver_antecesor_fase.short_description = "Relacion Antecesor-Sucesor"

    def eliminar_item(self, request, queryset):
        """Eliminar item de una fase, el item adquiere el estado Eliminado"""
        if len(queryset) == 1:
            for obj in queryset:
                proyecto_pk= request.session['id_proyecto']
                p=Proyectos.objects.get(id=proyecto_pk)
                estado_proyecto=p.estado_id

                fase_pk=request.session['id_fase']
                f=Fases.objects.get(id=fase_pk)
                estado_fase=f.estado_id

                has_perm =utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.ELIMINAR_ITEM, fase_pk)
                if has_perm:
                    estado=obj.estado.id
                    if estado_proyecto==1:
                        message_bi = "El Item no puede Eliminarse ya que el Proyecto no ha Iniciado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==2:
                        if estado_fase==1:
                            message_bi = "El item no puede Eliminarse ya que la Fase no ha Iniciado"
                            self.message_user(request, message_bi)
                        if estado_fase==2:
                            message_bi = "El item no puede Eliminarse ya que la Fase se ha Eliminado"
                            self.message_user(request, message_bi)
                        if estado_fase==3:
                            if estado==1:
                                rows_updated = queryset.update(estado=6)
                                if rows_updated == 1:
                                    message_bit = "El item se ha eliminado"
                                self.message_user(request, "%s exitosamente." % message_bit)
                            elif estado==2:
                                message_bi="El item no puede eliminarse debido a que se encuentra en Desarrollo Restringido"
                                self.message_user(request,message_bi)
                            elif estado==3:
                                message_bi="El item no puede eliminarse debido a que se encuentra Pendiente"
                                self.message_user(request,message_bi)
                            elif estado==4:
                                message_bi="El item no puede eliminarse debido a que se encuentra Aprobado con modificacion"
                                self.message_user(request,message_bi)
                            elif estado==5:
                                message_bi="El item no puede eliminarse debido a que se encuentra Aprobado sin modificacion"
                                self.message_user(request,message_bi)
                            elif estado==7:
                                message_bi="El item no puede eliminarse debido a que se encuentra Bloqueado"
                                self.message_user(request,message_bi)
                            elif estado==8:
                                message_bi="El item no puede eliminarse debido a que se encuentra en Revision"
                                self.message_user(request,message_bi)
                            elif estado==9:
                                message_bi="El item no puede eliminarse debido a que se encuentra Pendiente..."
                                self.message_user(request,message_bi)
                            else:
                                message_bi = "El item no puede eliminarse"
                                self.message_user(request, message_bi)
                        if estado_fase==4:
                            message_bi = "El Item item no puede Eliminarse ya que la Fase ha Finalizado"
                            self.message_user(request, message_bi)
                        if estado_fase==5:
                            message_bi = "El Item no puede Eliminarse ya que la Fase se encuentra Comprometida"
                            self.message_user(request, message_bi)
                        if estado_fase==6:
                            message_bi = "El item no puede Eliminarse debido adebido a que la Fase se encuentra en Desarrollo Restringido"
                            self.message_user(request, message_bi)
                    if estado_proyecto==3:
                        message_bi = "El Item no puede Eliminarse ya que el Proyecto se ha Cancelado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==4:
                        message_bi = "El Item no puede Eliminarse ya que el Proyecto ha Finalizado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==5:
                        message_bi = "El Item no puede Eliminarse ya que el Proyecto se ha Eliminado"
                        self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para Eliminar Item"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo item"
            self.message_user(request, message_bi)

    eliminar_item.short_description = "Eliminar Item"

    def save_model(self, request, obj, form, change):
        """
        Guarda los atributos del item creado
        @param: request,obj,form,change
        @xtype:
        """
        item_pk = obj.id
        if item_pk == None:
            obj.save()
            obj.item_version = obj.id
            obj.version = 1
            obj.save()

            tipo_item=obj.tipo_item
            atributos=tipo_item.atributos.all()

            if atributos:
                for atri in atributos:
                    atributo_item=AtributoItem(item=obj, atributo=atri)
                    atributo_item.save()
            else:
                print("La lista de atributos del item es null")
        elif obj.estado.id== 2:
             obj.save()
        else:
            item = Item.objects.get(id=item_pk)

            print("El viejo item es: ", item)
            rela_padre = RelacionPadre.objects.filter(item_hijo_id=item.id)  #obtiene la relacion en la cual es hijo
            print("La relacion donde es hijo es:", rela_padre)
            rela_hijo = RelacionPadre.objects.filter(item_padre_id=item.id)  #obtiene la relacion en la cual es padre
            print("La relacion en la que es padre es: ", rela_hijo)
            rela_antecesor = RelacionAntecesor.objects.filter(
                item_sucesor_id=item.id)  #obtiene la relacion en la cual es sucesor
            print("La relacion en la que es sucesor es: ", rela_antecesor)
            rela_sucesor = RelacionAntecesor.objects.filter(
                item_antecesor_id=item.id)  #obtiene la relacion en la cual es antecesor
            item_nuevo = Item(nombre=item.nombre, descripcion=item.descripcion, item_version=item.item_version,
                              tipo_item=item.tipo_item,
                              complejidad=item.complejidad)
            item_nuevo.nombre = request.POST.get('nombre', '')
            item_nuevo.descripcion = request.POST.get('descripcion', '')
            item_nuevo.id_fase = item.id_fase
            item_nuevo.archivo=obj.archivo
            item_nuevo.version = item.version + 1
            item_nuevo.save()
            item_nuevo_id = item_nuevo.id  #almacena el id del nuevo item creado
            if rela_padre:
                relacion = RelacionPadre.objects.get(item_hijo_id=item.pk)
                relacion_nuevo = RelacionPadre(nombre=relacion.nombre, descripcion=relacion.descripcion,
                                               item_padre=relacion.item_padre)
                relacion_nuevo.item_hijo_id = item_nuevo_id
                relacion_nuevo.save()
            if rela_hijo:
                relacion = RelacionPadre.objects.get(item_padre_id=item.id)
                relacion_nuevo = RelacionPadre(nombre=relacion.nombre, descripcion=relacion.descripcion,
                                               item_hijo=relacion.item_hijo)
                relacion_nuevo.item_padre_id = item_nuevo_id
                relacion_nuevo.save()
            if rela_antecesor:
                relacion = RelacionAntecesor.objects.get(item_sucesor_id=item.id)
                relacion_nuevo = RelacionAntecesor(nombre=relacion.nombre, descripcion=relacion.descripcion,
                                                   item_antecesor=relacion.item_antecesor)
                relacion_nuevo.item_sucesor_id = item_nuevo_id
                relacion_nuevo.save()
            if rela_sucesor:
                relacion = RelacionAntecesor.objects.get(item_antecesor_id=item.id)
                relacion_nuevo = RelacionAntecesor(nombre=relacion.nombre, descripcion=relacion.descripcion,
                                                   item_sucesor=item_nuevo.id)
                relacion_nuevo.item_antecesor_id = item_nuevo_id
                relacion_nuevo.save()

            atributo_item=AtributoItem.objects.filter(item=obj.id)

            if atributo_item:
               for atri in atributo_item:
                   atributo_item=AtributoItem(item=item_nuevo, atributo=atri.atributo, valor=atri.valor)
                   atributo_item.save()

    def response_post_save_add(self, request, obj):
        """
        Redirige a la lista de items de una fase despues de crear un item
        @param: request,obj
        @xtype:
        @return: URL que redirecciona a la lista de items de una fase, luego de crear una fase
        @rtype: HttpResponseRedirect
        """
        fase_pk = request.session['id_fase']
        return HttpResponseRedirect("/admin/EPMApp/item/?id_fase=%s" % fase_pk)

    def get_form(self, request, *args, **kwargs):
        form = super(ItemAdmin, self).get_form(request, *args, **kwargs)
        fase = request.session['id_fase']
        #print("fase")
        #print(fase)
        #print(args)
        #print("-------------")
        #print(kwargs)
        if fase:
            kwargs['initial'] = {'id_fase': Fases.objects.get(id=fase)}
            form.initial = {'id_fase': Fases.objects.get(id=fase)}
            #print(kwargs)
        return form

    def aprobar_item(self, request, queryset):
        """Aprueba un item modificando su estado a Aprobado con modificacion o Aprobado sin modificacion"""
        if len(queryset) == 1:
            for obj in queryset:

                proyecto_pk= request.session['id_proyecto']
                p=Proyectos.objects.get(id=proyecto_pk)
                estado_proyecto=p.estado_id

                fase_pk=request.session['id_fase']
                f=Fases.objects.get(id=fase_pk)
                estado_fase=f.estado_id

                #if estado_fase == 3:
                has_perm =utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.APROBAR_ITEM, fase_pk)
                #if estado_fase == 6:
                    #has_perm =utils.has_perm_temporal(request.user.id, configuraciones.APROBAR_ITEM, obj.id)

                if has_perm:
                    print(obj.estado.id)
                    estado = obj.estado.id

                    if estado_proyecto==1:
                        message_bi = "El Item no puede Aprobarse ya que el Proyecto no ha Iniciado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==2:
                        if estado_fase==1:
                            message_bi = "El item no puede Aprobarse ya que la Fase no ha Iniciado"
                            self.message_user(request, message_bi)
                        if estado_fase==2:
                            message_bi = "El item no puede Aprobarse ya que la Fase se ha Eliminado"
                            self.message_user(request, message_bi)
                        if estado_fase==3:
                            if estado == 3:
                                rows_updated = queryset.update(estado=4)
                                if rows_updated == 1:
                                    message_bit = "El item ha sido aprobado exitosamente"
                                else:
                                    message_bit = 'El item no puede Aprobarse'
                                self.message_user(request, message_bit)
                            elif estado == 1 or estado == 2:
                                message_bi = "El item no puede Aprobarse debido a que aun se encuentra en desarrollo. Por favor confirme sus cambios para que pueda ser aprobado."
                                self.message_user(request, message_bi)
                            elif estado == 4 or estado == 5:
                                message_bi = "El item no puede Aprobarse debido a que ya se encuentra en un estado aprobado."
                                self.message_user(request, message_bi)
                            elif estado == 6:
                                message_bi = "El item no puede Aprobarse debido a que ha sido eliminado."
                                self.message_user(request, message_bi)
                            elif estado == 7:
                                message_bi = "El item no puede Aprobarse debido a que se encuentra en una linea base."
                                self.message_user(request, message_bi)
                            elif estado == 8:
                                message_bi = "El item no puede Aprobarse debido a que se encuentra en Revision."
                                self.message_user(request, message_bi)
                            elif estado == 9:
                                #Update del estado del item a bloqueado
                                rows_updated = queryset.update(estado=7)
                                if rows_updated == 1:
                                    #Linea Base de la que forma un item
                                    #lineaBase= LineaBase.objects.all()
                                    linea_base= LineaBase.objects.filter(items__id=obj.id)
                                    message_bit = "El item ha sido aprobado exitosamente"

                                    for lb in linea_base:

                                        if lb.estado_id == 3 or lb.estado_id== 4:

                                            items= lb.items.all()

                                            if items:
                                                cant_items=len(items)
                                                cont=0

                                                for i in items:
                                                    print('Items')
                                                    print(i.id)
                                                    if i.estado_id==7:
                                                        cont=cont+1

                                                if cont==cant_items:

                                                    lb.estado_id=1
                                                    lb.save()
                                                    message_bit = "Se ha cerrado la Linea Base con el item aprobado"

                                                    # items_fase=Item.objects.filter(id_fase=fase_pk)
                                                    # cant_items_fase=len(items_fase)
                                                    # conti=0
                                                    #
                                                    # if items_fase:
                                                    #     for i in items_fase:
                                                    #         if i.estado_id==7:
                                                    #             conti=conti+1
                                                    #
                                                    #     if conti==cant_items_fase:
                                                    #         f.estado=4
                                                    #         fecha=date.today()
                                                    #         print(fecha)
                                                    #         f.fecha_finalizacion=fecha
                                                    #         f.save()
                                                    #         message_bit = "La Fase ha finalizado exitosamente"
                                                    #     else:
                                                    #         message_bit = "La Fase no puede Finalizar ya que sus items no estan en Linea Base"
                                                    #     #self.message_user(request, message_bit)
                                                    # else:
                                                    #     message_bit = "La Fase no puede Finalizar ya que no tien items"
                                                    #     #self.message_user(request, message_bit)
                                                else:
                                                    message_bit = "El item ha sido aprobado exitosamente"
                                                    #self.message_user(request, message_bit)
                                            else:
                                                message_bit = "El item ha sido aprobado exitosamente"
                                                    #self.message_user(request, message_bit)
                                        else:
                                            message_bit = "El item ha sido aprobado exitosamente"
                                            #self.message_user(request, message_bit)
                                else:
                                    message_bit = 'El item no puede Aprobarse'
                                self.message_user(request, message_bit)
                        if estado_fase==4:
                            message_bi = "El item no puede Aprobarse debido a que la Fase ha Finalizado"
                            self.message_user(request, message_bi)
                        if estado_fase==5:
                            message_bi = "El item no puede Aprobarse debido adebido a que la Fase se encuentra Comprometida"
                            self.message_user(request, message_bi)
                        #Fase en estado de desarrollo restringido
                        if estado_fase==6:
                            #Item pendiente despues de haber realizado cambios a partir de una solicitud de cambio
                            if estado == 9:
                                #Update del estado del item a bloqueado
                                rows_updated = queryset.update(estado=7)
                                if rows_updated == 1:
                                    #Linea Base de la que forma un item
                                    #lineaBase= LineaBase.objects.all()
                                    linea_base= LineaBase.objects.filter(items__id=obj.id)
                                    message_bit = "El item ha sido aprobado exitosamente"

                                    for lb in linea_base:

                                        if lb.estado_id == 3 or lb.estado_id== 4:

                                            items= lb.items.all()

                                            if items:
                                                cant_items=len(items)
                                                cont=0

                                                for i in items:
                                                    print('Items')
                                                    print(i.id)
                                                    if i.estado_id==7:
                                                        cont=cont+1

                                                if cont==cant_items:

                                                    lb.estado_id=1
                                                    lb.save()
                                                    message_bit = "Se ha creado la Linea Base con el item aprobado"

                                                    fase=Fases.objects.get(id=fase_pk)
                                                    print(fase.id)

                                                    if fase:
                                                        lineas_base=LineaBase.objects.filter(id_fase=fase_pk)
                                                        cant_lbase=len(lineas_base)
                                                        contlb=0

                                                        for i in lineas_base:
                                                            print('Linea Base estado')
                                                            print(i.estado_id)
                                                            if i.estado_id==1:
                                                                contlb=contlb+1

                                                        if contlb==cant_lbase:
                                                            fase.estado_id=4
                                                            fecha=date.today()
                                                            print(fecha)
                                                            fase.fecha_finalizacion=fecha
                                                            fase.save()
                                                            message_bit = "La Fase ha finalizado con la aprobacion de este Item"
                                                            #self.message_user(request, message_bit)
                                                else:
                                                    message_bit = "El item ha sido aprobado exitosamente"
                                                    #self.message_user(request, message_bit)
                                            else:
                                                message_bit = "El item ha sido aprobado exitosamente"
                                                    #self.message_user(request, message_bit)
                                        else:
                                            message_bit = "El item ha sido aprobado exitosamente"
                                            #self.message_user(request, message_bit)
                                else:
                                    message_bit = 'El item no puede Aprobarse'
                                self.message_user(request, message_bit)
                            elif estado == 1 or estado == 2:
                                message_bi = "El item no puede Aprobarse debido a que aun se encuentra en desarrollo. Por favor confirme sus cambios para que pueda ser aprobado."
                                self.message_user(request, message_bi)
                            elif estado == 4 or estado == 5:
                                message_bi = "El item no puede Aprobarse debido a que ya se encuentra en un estado aprobado."
                                self.message_user(request, message_bi)
                            elif estado == 6:
                                message_bi = "El item no puede Aprobarse debido a que ha sido eliminado."
                                self.message_user(request, message_bi)
                            elif estado == 7:
                                message_bi = "El item no puede Aprobarse debido a que se encuentra en una linea base."
                                self.message_user(request, message_bi)
                            elif estado == 8:
                                message_bi = "El item no puede Aprobarse debido a que se encuentra en Revision."
                                self.message_user(request, message_bi)
                            # elif estado == 9:
                            #     message_bi = "El item no puede Aprobarse debido a que se encuentra Pendiente."
                            #     self.message_user(request, message_bi)
                    if estado_proyecto==3:
                        message_bi = "El item no puede Aprobarse debido a que el Proyecto se ha Cancelado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==4:
                        message_bi = "El item no puede Aprobarse debido a que el Proyecto ha Finalizado"
                        self.message_user(request, message_bi)
                    if estado_proyecto==5:
                        message_bi = "El item no puede Aprobarse debido a que el Proyecto se ha Eliminado"
                        self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para Aprobar Item"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo item a aprobar"
            self.message_user(request, message_bi)

    aprobar_item.short_description = "Aprobar item"

    def confirmar_item(self, request, queryset):
        """Confirma los cambios hechos en un item"""
        if len(queryset) == 1:
            for obj in queryset:
                proyecto_pk= request.session['id_proyecto']
                p=Proyectos.objects.get(id=proyecto_pk)
                estado_proyecto=p.estado_id

                fase_pk=request.session['id_fase']
                f=Fases.objects.get(id=fase_pk)
                estado_fase=f.estado_id


                has_perm =utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.CONFIRMAR_ITEM, fase_pk)
                # if estado_fase == 3:
                #      has_perm =utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.CONFIRMAR_ITEM, fase_pk)
                #  if estado_fase == 6:
                #       has_perm =utils.has_perm_temporal(request.user.id, configuraciones.CONFIRMAR_ITEM_T, obj.id)

                # if has_perm:
                print(obj.estado.id)
                estado = obj.estado.id
                if estado_proyecto==1:
                    message_bi = "Los cambios en el Item no se pueden confirmar debido a que el Proyecto no ha Iniciado"
                    self.message_user(request, message_bi)
                if estado_proyecto==2:
                    if estado_fase==1:
                        message_bi = "Los cambios en el Item no se pueden confirmar debido a que la Fase no ha Iniciado"
                        self.message_user(request, message_bi)
                    if estado_fase==2:
                        message_bi = "Los cambios en el Item no se pueden confirmar debido a que la Fase se ha Eliminado"
                        self.message_user(request, message_bi)
                    if estado_fase==3:
                        if estado == 1:
                            has_perm =utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.CONFIRMAR_ITEM, fase_pk)
                            if has_perm:
                                rows_updated = queryset.update(estado=3)
                                if rows_updated == 1:
                                    message_bit = "Los cambios en el Item se han confirmado y el mismo se encuentra pendiente de aprobacion."
                                self.message_user(request, message_bit)
                            else:
                                message_bi = "No posee permisos para Confirmar Item"
                                self.message_user(request, message_bi)
                        if estado==2:
                            has_perm =utils.has_perm_temporal(request.user.id, configuraciones.MODIFICAR_ITEM, obj.id)
                            if has_perm:
                                rows_updated = queryset.update(estado=9)
                                if rows_updated == 1:
                                    message_bit = "Los cambios en el Item se han confirmado y el mismo se encuentra pendiente de aprobacion"
                                self.message_user(request,message_bit)
                            else:
                                message_bi = "No posee permisos para Confirmar Item"
                                self.message_user(request, message_bi)
                        if estado == 3:
                            message_bi = "No se pudo realizar la operacion debido a que el item se encuentra pendiente de aprobacion."
                            self.message_user(request, message_bi)
                        if estado == 4 or estado == 5:
                            message_bi = "No se pudo realizar la operacion debido a que el item se encuentra en un estado aprobado."
                            self.message_user(request, message_bi)
                        if estado == 6:
                            message_bi = "No se pudo realizar la operacion debido a que el item ha sido eliminado."
                            self.message_user(request, message_bi)
                        if estado == 7:
                            message_bi = "No se pudo realizar la operacion debido a que el item se encuentra en una linea base."
                            self.message_user(request, message_bi)
                        if estado == 8:
                            has_perm =utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.CONFIRMAR_ITEM, fase_pk)
                            if has_perm:
                                rows_updated = queryset.update(estado=9)
                                if rows_updated == 1:
                                    message_bit = "Los cambios en el Item se han confirmado y el mismo se encuentra pendiente de aprobacion"
                                self.message_user(request,message_bit)
                            else:
                                message_bi = "No posee permisos para Confirmar Item"
                                self.message_user(request, message_bi)
                        if estado == 9:
                            message_bi = "No se pudo realizar la operacion debido a que el item se encuentra Pendiente."
                            self.message_user(request, message_bi)
                    if estado_fase==4:
                        message_bi = "Los cambios en el Item no se pueden confirmar debido a que la Fase ha Finalizado"
                        self.message_user(request, message_bi)
                    if estado_fase==5:
                        message_bi = "Los cambios en el Item no se pueden confirmar debido a que la Fase se encuentra Comprometida"
                        self.message_user(request, message_bi)
                    if estado_fase==6:
                        if estado == 2:
                            has_perm =utils.has_perm_temporal(request.user.id, configuraciones.MODIFICAR_ITEM, obj.id)
                            if has_perm:
                                rows_updated = queryset.update(estado=9)
                                if rows_updated == 1:
                                    message_bit = "Los cambios en el Item se han confirmado y el mismo se encuentra pendiente de aprobacion"
                                self.message_user(request,message_bit)
                            else:
                                message_bi = "No posee permisos para Confirmar Item"
                                self.message_user(request, message_bi)
                        if estado == 1:
                             message_bi = "No se pudo realizar la operacion debido a que el item se encuentra en desarrollo."
                             self.message_user(request, message_bi)
                        if estado == 3:
                             message_bi = "No se pudo realizar la operacion debido a que el item se encuentra pendiente."
                             self.message_user(request, message_bi)
                        if estado == 4 or estado == 5:
                            message_bi = "No se pudo realizar la operacion debido a que el item se encuentra en un estado aprobado."
                            self.message_user(request, message_bi)
                        if estado == 6:
                            message_bi = "No se pudo realizar la operacion debido a que el item ha sido eliminado."
                            self.message_user(request, message_bi)
                        if estado == 7:
                            message_bi = "No se pudo realizar la operacion debido a que el item se encuentra en una linea base."
                            self.message_user(request, message_bi)
                        if estado == 8:
                            has_perm =utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.CONFIRMAR_ITEM, fase_pk)
                            if has_perm:
                                rows_updated = queryset.update(estado=9)
                                if rows_updated == 1:
                                    message_bit = "Los cambios en el Item se han confirmado y el mismo se encuentra pendiente de aprobacion"
                                self.message_user(request,message_bit)
                            else:
                                message_bi = "No posee permisos para Confirmar Item"
                                self.message_user(request, message_bi)
                        if estado == 9:
                            message_bi = "No se pudo realizar la operacion debido a que el item se encuentra Pendiente."
                            self.message_user(request, message_bi)
                if estado_proyecto==3:
                    message_bi = "Los cambios en el Item no se pueden confirmar debido a que el Proyecto se ha Cancelado"
                    self.message_user(request, message_bi)
                if estado_proyecto==4:
                    message_bi = "Los cambios en el Item no se pueden confirmar debido a que el Proyecto ha Finalizado"
                    self.message_user(request, message_bi)
                if estado_proyecto==5:
                    message_bi = "Los cambios en el Item no se pueden confirmar debido a que el Proyecto se ha Eliminado"
                    self.message_user(request, message_bi)
                # else:
                #     message_bi = "No posee permisos para Confirmar Item"
                #     self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo item a confirmar"
            self.message_user(request, message_bi)

    confirmar_item.short_description = "Confirmar cambios"

    def get_queryset(self, request):
        """
        Metodo que obtiene los items a los que pertenece una determinada fase
        @param: request
        @xtype:
        """
        try:
            b = request.session['proceso']
        except:
            print("No se puede obtener el ID proceso")
        qs = super(ItemAdmin, self).get_queryset(request)
        print("Valor de b:", b)
        if b:
            if b == SESION_LISTAR_VERSIONES:
                try:
                    fases_pk = request.session['id_fase']
                    print("La fase actual es: ", fases_pk)
                    item_cod = request.session['cod_item']
                    item_pk = request.session['item_pk']
                except:
                    print("No se pudo obtener los Ids")
                if item_cod:
                    qs = Item.objects.filter(item_version=item_cod)
                    return qs.exclude(pk=item_pk)
        return qs

    def get_actions(self, request):
        """
        Desactiva las acciones predeterminadas del sistema de la Administracion de Items
        @param: request
        @xtype:

        """
        b = 0

        try:
            b = request.session['proceso']
        except:
            print("No pudo obtenerse el id de proceso")
        actions = super(ItemAdmin, self).get_actions(request)
        print("La b aqui vale: ", b)
        if b:
            if b == SESSION_VER_ITEM:
                del actions['revertir_item']

            elif b == SESION_LISTAR_VERSIONES:
                del actions['confirmar_item']
                del actions['aprobar_item']
                del actions['ver_padre']
                del actions['ver_antecesor_fase']
                del actions['listar_versiones_item']
                del actions['eliminar_item']
                del actions['historial_item']
                del actions['revivir_item']
                del actions['calculo_impacto']
                del actions['ver_relacion_antecesor']
            elif b == SESSION_IMPACTO:
                del actions['confirmar_item']
                del actions['aprobar_item']
                del actions['ver_antecesor_fase']
                del actions['listar_versiones_item']
                del actions['eliminar_item']
                del actions['historial_item']
                del actions['revivir_item']
                del actions['revertir_item']
                del actions['calculo_impacto']
            elif b == SESSION_FALSE_ANTECESOR:
                del actions['revertir_item']

        return actions

    def has_add_permission(self, request):
        """
        Desactiva la opcion de agregar
        @return: True, False
        @rtype: Boolean
        """
        id_proyecto=None
        id_fase=None
        b = 0
        try:
            id_proyecto=request.session['id_proyecto']
            id_fase=request.session['id_fase']
            b = request.session['proceso']
        except:
            print("No pudo obtener el id de proceso")
        if b == SESION_LISTAR_VERSIONES or b == SESSION_IMPACTO:
            return False
        else:
            #return True
            has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.LISTAR_ITEMS,id_fase)
            return has_perm

    def get_changelist(self, request, **kwargs):
        """
        Configuraciones de la lista principal de item
        @return Lista de Items del sistema
        """
        return ItemChangeList

    # def get_list_display(self, request):
    #     """
    #     Despliega la lista de items de acuerdo a que accion se realiza
    #     @param: request
    #     @xtype: ItemAdmin
    #     @return:
    #     @rtype:
    #     """
    #     b=request.session['proceso']
    #     if b:
    #         if b==10:
    #             return ('nombre','descripcion','version','estado')
    #         else:
    #             return ('nombre','descripcion','estado')
    #     else:
    #         return ('nombre','descripcion','estado')


    def response_post_save_add(self, request, obj):
        fase_pk = request.session['id_fase']
        return HttpResponseRedirect("/admin/EPMApp/item/?id_fase=%s" % fase_pk)

    def historial_item(self, request, queryset):
        """Despliega todas la versiones de un ITEM
        @param: request, queryset
        @xtype: ItemAdmin
        @return: URL que redirecciona a la lista de las versiones del item seleccionado
        @rtype: HttpResponseRedirect
        """

        proyecto_pk= request.session['id_proyecto']

        fase_pk=request.session['id_fase']

        request.session['proceso'] = 3
        if len(queryset) == 1:
            for obj in queryset:
                has_perm =utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.HISTORIAL_ITEM, fase_pk)
                if has_perm:
                    request.session['proceso'] = 10
                    return HttpResponseRedirect("/admin/EPMApp/item/?item_version=%s" % obj.item_version)
                else:
                    message_bi = "No posee permisos para visualizar historial de item"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo Item"
            self.message_user(request, message_bi)

    historial_item.short_description = "Historial Item"

    def revivir_item(self, request, queryset):
        """
        Revive un item eliminado

        @param: request,queryset
        @xtype:
        @return: URL que redirecciona a la lista de relaciones del item
        @rtype: HttpResponseRedirect
        """
        try:
            proyecto_pk= request.session['id_proyecto']
            fase_pk = request.session['id_fase']
            #version_anterior=request.session['version']
        except:
            print("No se pudieron obtener los Ids")
        version = 0
        if len(queryset) == 1:
            for obj in queryset:
                has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.REVIVIR_ITEM, fase_pk)
                if has_perm:
                    # request.session['id_item']=obj.id
                    item = Item.objects.get(pk=obj.id)
                    print("El item vale: ", obj.id)
                    # id_rela=RelacionPadre.item_hijo=obj.id
                    relacionPadre = RelacionPadre.objects.filter(item_hijo_id=obj.id)
                    relacionAntecesor = RelacionAntecesor.objects.filter(item_sucesor_id=obj.id)
                    print("Relacion Padre: ", relacionPadre)
                    estado_item = obj.estado.id
                    if not estado_item == 6:
                        message_bi = "El item seleccionado no ha sido eliminado y no se puede realizar la operacion de Revivir"
                        self.message_user(request, message_bi)
                    else:
                        if item:
                            item.estado = EstadosItem.objects.get(id=1)
                            item.save()
                            if relacionPadre:
                                item_padre = \
                                    Item.objects.filter(item_version=relacionPadre[0].item_padre.item_version).order_by('-id')[0]
                                print(item_padre)
                                if item_padre:
                                    #si el item padre aun esta activo
                                    if not item_padre.estado.id == 6:
                                        request.session['proceso'] = SESSION_RELA_PADRE
                                        message_bi = "El item ha sido revivido con su relacion padre."
                                        self.message_user(request, message_bi)
                                    else:
                                        message_bi = "El item ha sido revivido sin su relacion padre debido a que el item padre ha sido eliminado. Por favor verifique la relacion y vuelva a crearla de ser necesario."
                                        self.message_user(request, message_bi)

                            elif relacionAntecesor:
                                request.session['proceso'] = SESSION_RELA_ANTECESOR
                                item_antecesor = \
                                    Item.objects.filter(item_version=relacionAntecesor[0].item_antecesor.item_version).order_by(
                                        '-id')[0]

                                if item_antecesor:
                                    print(item_antecesor)
                                    #si el item padre aun esta activo
                                    if not item_antecesor.estado.id == 6:
                                        message_bi = "El item ha sido revivido con su relacion antecesor."
                                        self.message_user(request, message_bi)
                                    else:
                                        message_bi = "El item ha sido revivido sin su relacion antecesor-sucesor debido a que el item antecesor ha sido eliminado. Por favor verifique la relacion y vuelva a crearla de ser necesario."
                                        self.message_user(request, message_bi)
                            message_bit = "Se ha revivido el item"
                            self.message_user(request, "%s exitosamente." % message_bit)

                        else:
                            message_bi = "No se ha podido revivir el item"
                            self.message_user(request, message_bi)
                else:
                    message_bi = "No posee permisos para revivir el item"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo Item"
            self.message_user(request, message_bi)

    revivir_item.short_description = "Revivir Item "

    def ver_atributos(self, request, queryset):
        """
        Despliega las lista de los atributos del item
        @rtype: HttpResponseRedirect
        """

        proyecto_pk= request.session['id_proyecto']
        fase_pk=request.session['id_fase']

        if len(queryset) == 1:
            for obj in queryset:
                has_perm =utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.LISTAR_ATRIBUTOS, fase_pk)
                if has_perm:
                    request.session['id_item']=obj.id
                    return HttpResponseRedirect("/admin/EPMApp/atributoitem/?item=%s" %obj.id)
                else:
                    message_bi = "No posee permisos para Listar los atributos"
                    self.message_user(request, message_bi)
        else:
            message_bi = "Seleccione un solo Item"
            self.message_user(request, message_bi)

    ver_atributos.short_description = "Visualizar atributos"

    def get_readonly_fields(self, request, obj=None):
        """Deshabilita los campos a modificar si un proyecto ya no se encuentra en estado creado
        @param: request, obj=None
        @xtype:
        """

        proyecto_pk= request.session['id_proyecto']
        p=Proyectos.objects.get(id=proyecto_pk)
        estado_proyecto=p.estado_id

        fase_pk=request.session['id_fase']
        f=Fases.objects.get(id=fase_pk)
        estado_fase=f.estado_id

        #cuando se crea
        if obj == None:
            has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.AGREGAR_ITEM, fase_pk)
            if has_perm:
                request.session['estado_item']=None
                return  ('')
            else:
                message_bi = "No posee permisos para Crear un Item"
                self.message_user(request, message_bi)

        #cuando existe un item
        if obj != None:
            estado = obj.estado.id

            #Estados del proyecto
            if estado_proyecto==1:
                return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
            if estado_proyecto==2:
                #Estados de la fase
                if estado_fase==1:
                    return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                if estado_fase==2 :
                    return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                if estado_fase==3:
                    #Estado del item
                    if estado==1:
                        has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.MODIFICAR_ITEM, fase_pk)
                        if has_perm:
                            request.session['estado_item']=estado
                            return  ('')
                        else:
                            message_bi = "No posee permisos para Modificar Items"
                            request.session['estado_item']='sin_permiso'
                            return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                        self.message_user(request, message_bi)

                    if estado==2:
                        # request.session['estado_item']='sin_permiso'
                        # return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                        has_perm = utils.has_perm_temporal(request.user.id,configuraciones.MODIFICAR_ITEM, obj.id)

                        if has_perm:
                            request.session['estado_item']=estado
                            return  ('')
                        else:
                            message_bi = "No posee permisos para Modificar Items"
                            request.session['estado_item']='sin_permiso'
                            return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                        self.message_user(request, message_bi)
                    if estado==3:
                        has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.MODIFICAR_ITEM, fase_pk)
                        if has_perm:
                            request.session['estado_item']=estado
                            return  ('')
                        else:
                            message_bi = "No posee permisos para Modificar Items"
                            request.session['estado_item']='sin_permiso'
                            return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                        self.message_user(request, message_bi)
                    if estado==4:
                        has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.MODIFICAR_ITEM, fase_pk)
                        if has_perm:
                            request.session['estado_item']=estado
                            return  ('')
                        else:
                            message_bi = "No posee permisos para Modificar Items"
                            request.session['estado_item']='sin_permiso'
                            return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                        self.message_user(request, message_bi)
                    if estado==5:
                        request.session['estado_item']=estado
                        return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                    #Item Eliminado
                    if estado==6:
                        request.session['estado_item']=estado
                        return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                    #Item Bloqueado
                    if estado==7:
                        request.session['estado_item']=estado
                        return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                    if estado==8:
                        request.session['estado_item']=estado
                        return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                    if estado==9:
                        request.session['estado_item']=estado
                        return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                if estado_fase==4:
                    return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                if estado_fase==5:
                    return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                if estado_fase==6:
                    if estado==1:
                        request.session['estado_item']='sin_permiso'
                        return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                    if estado==2:
                        has_perm = utils.has_perm_temporal(request.user.id,configuraciones.MODIFICAR_ITEM, obj.id)
                        if has_perm:
                            request.session['estado_item']=estado
                            return  ('')
                        else:
                            message_bi = "No posee permisos para Modificar Items"
                            request.session['estado_item']='sin_permiso'
                            return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                        self.message_user(request, message_bi)
                    if estado==3:
                        request.session['estado_item']='sin_permiso'
                        return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                    if estado==4:
                        request.session['estado_item']='sin_permiso'
                        return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                    if estado==5:
                        request.session['estado_item']=estado
                        return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                    if estado==6:
                        request.session['estado_item']=estado
                        return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                    if estado==7:
                        request.session['estado_item']=estado
                        return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                    if estado==8:
                        request.session['estado_item']=estado
                        return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                    if estado==9:
                        request.session['estado_item']=estado
                        return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
            if estado_proyecto==3:
                return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
            if estado_proyecto==4:
                return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
            if estado_proyecto==5:
                return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')


    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """Despliega la fase en el cual se desea agregar un Item
        @param: db_field, request, **kwargs
        @xtype: ItemAdmin
        """
        if db_field.name == "id_fase":
            parametros = request.session['id_fase']
            if parametros:
                fases_id = parametros
                f = Fases.objects.filter(id=fases_id)
                kwargs["queryset"] = f.filter()
                #print('Fases')
                #print(kwargs)

            return super(ItemAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

        if db_field.name == "tipo_item":
            parametros = request.session['id_fase']
            if parametros:
                fases_id = parametros
                tipoItem = TiposItems.objects.filter(id_fase=fases_id)
                kwargs["queryset"] = tipoItem.filter()
                #print('Tipo')
                #print(kwargs)

            return super(ItemAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def has_change_permission(self, request, obj=None):
        id_proyecto=None
        id_fase=None
        try:
            id_proyecto=request.session['id_proyecto']
            id_fase=request.session['id_fase']
        except:
            print("No se encontro el id")
        has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.LISTAR_ITEMS,id_fase)
        return has_perm

    # def has_add_permission(self, request, obj=None):
    #     id_proyecto=None
    #     id_fase=None
    #     try:
    #         id_proyecto=request.session['id_proyecto']
    #         id_fase=request.session['id_fase']
    #     except:
    #         print("No se encontro el id")
    #     has_perm=utils.has_fase_permission(request.user.id,id_proyecto,configuraciones.AGREGAR_ITEM,id_fase)
    #     return has_perm

class ItemAd(admin.TabularInline):
    """Representa el administrador item inline a la creacion de fases"""
    fields = ('nombre', 'descripcion' )
    model = Item
    extra = 0


class ItemChangeList(ChangeList):
    """
    Clase que administra la lista de cambio de Items
    """
    def __init__(self, *args, **kwargs):
        super(ItemChangeList, self).__init__(*args, **kwargs)
        self.title = "Seleccione un item y la operacion a realizar"


class AtributosItemAdmin(admin.ModelAdmin):
    """Representa el administrador de las votaciones realizadas por los usuarios a una solicitud de cambio"""
    model=AtributoItem
    list_display = ('atributo','valor')
    fields = ('atributo','valor')

    def get_readonly_fields(self, request, obj=None):
            """Deshabilita los campos a modificar si un proyecto ya no se encuentra en estado creado
            @param: request, obj=None
            @xtype:
            """

            proyecto_pk= request.session['id_proyecto']
            p=Proyectos.objects.get(id=proyecto_pk)
            estado_proyecto=p.estado_id

            fase_pk=request.session['id_fase']
            f=Fases.objects.get(id=fase_pk)
            estado_fase=f.estado_id

            #cuando se crea
            if obj == None:
                has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.AGREGAR_ITEM, fase_pk)
                if has_perm:
                    request.session['estado_item']=None
                    return  ('')
                else:
                    message_bi = "No posee permisos para Crear un Item"
                    self.message_user(request, message_bi)

            #cuando existe un item
            if obj != None:

                item_pk=request.session['id_item']
                item=Item.objects.get(id=item_pk)
                estado =item.estado_id

                #Estados del proyecto
                if estado_proyecto==1:
                    return ('atributo','valor')
                if estado_proyecto==2:
                    #Estados de la fase
                    if estado_fase==1:
                        return ('atributo','valor')
                    if estado_fase==2 :
                        return ('atributo','valor')
                    if estado_fase==3:
                        #Estado del item
                        if estado==1:
                            has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.MODIFICAR_ITEM, fase_pk)
                            if has_perm:
                                request.session['estado_item']=estado
                                return  ('')
                            else:
                                message_bi = "No posee permisos para Modificar Items"
                                request.session['estado_item']='sin_permiso'
                                return ('atributo','valor')
                            self.message_user(request, message_bi)

                        if estado==2:
                            # request.session['estado_item']='sin_permiso'
                            # return ('nombre', 'descripcion', 'id_fase','tipo_item','complejidad')
                            has_perm = utils.has_perm_temporal(request.user.id,configuraciones.MODIFICAR_ITEM, obj.id)

                            if has_perm:
                                request.session['estado_item']=estado
                                return  ('')
                            else:
                                message_bi = "No posee permisos para Modificar Items"
                                request.session['estado_item']='sin_permiso'
                                return ('atributo','valor')
                            self.message_user(request, message_bi)
                        if estado==3:
                            has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.MODIFICAR_ITEM, fase_pk)
                            if has_perm:
                                request.session['estado_item']=estado
                                return  ('')
                            else:
                                message_bi = "No posee permisos para Modificar Items"
                                request.session['estado_item']='sin_permiso'
                                return ('atributo','valor')
                            self.message_user(request, message_bi)
                        if estado==4:
                            has_perm = utils.has_fase_permission(request.user.id, proyecto_pk, configuraciones.MODIFICAR_ITEM, fase_pk)
                            if has_perm:
                                request.session['estado_item']=estado
                                return  ('')
                            else:
                                message_bi = "No posee permisos para Modificar Items"
                                request.session['estado_item']='sin_permiso'
                                return ('atributo','valor')
                            self.message_user(request, message_bi)
                        if estado==5:
                            request.session['estado_item']=estado
                            return ('atributo','valor')
                        #Item Eliminado
                        if estado==6:
                            request.session['estado_item']=estado
                            return ('atributo','valor')
                        #Item Bloqueado
                        if estado==7:
                            request.session['estado_item']=estado
                            return ('atributo','valor')
                        if estado==8:
                            request.session['estado_item']=estado
                            return ('atributo','valor')
                        if estado==9:
                            request.session['estado_item']=estado
                            return ('atributo','valor')
                    if estado_fase==4:
                        return ('atributo','valor')
                    if estado_fase==5:
                        return ('atributo','valor')
                    if estado_fase==6:
                        if estado==1:
                            request.session['estado_item']='sin_permiso'
                            return ('atributo','valor')
                        if estado==2:
                            has_perm = utils.has_perm_temporal(request.user.id,configuraciones.MODIFICAR_ITEM, obj.id)
                            if has_perm:
                                request.session['estado_item']=estado
                                return  ('')
                            else:
                                message_bi = "No posee permisos para Modificar Items"
                                request.session['estado_item']='sin_permiso'
                                return ('atributo','valor')
                            self.message_user(request, message_bi)
                        if estado==3:
                            request.session['estado_item']='sin_permiso'
                            return ('atributo','valor')
                        if estado==4:
                            request.session['estado_item']='sin_permiso'
                            return ('atributo','valor')
                        if estado==5:
                            request.session['estado_item']=estado
                            return ('atributo','valor')
                        if estado==6:
                            request.session['estado_item']=estado
                            return ('atributo','valor')
                        if estado==7:
                            request.session['estado_item']=estado
                            return ('atributo','valor')
                        if estado==8:
                            request.session['estado_item']=estado
                            return ('atributo','valor')
                        if estado==9:
                            request.session['estado_item']=estado
                            return ('atributo','valor')
                if estado_proyecto==3:
                    return ('atributo','valor')
                if estado_proyecto==4:
                    return ('atributo','valor')
                if estado_proyecto==5:
                    return ('atributo','valor')


def generar_imagen_grafo_proyecto(id_proyecto):

    """
    Metodo interno para graficar el grafo del proyecto
    :param id_proyecto: el ID del proyecto

    :return: el path de la imagen guardada
    """
    proyecto = Proyectos.objects.get(id=id_proyecto)

    fases = Fases.objects.filter(id_proyecto=id_proyecto)

    MG = nx.MultiDiGraph()


    #crear nodos
    i = 0


    x = 0
    labels = {}
    for fase in fases:

        items = Item.objects.filter(id_fase_id=fase.id).exclude(estado=6)

        print items
        i = i + 1

        for item in items:
            x = x + 1

            labels[item.id] = item.id

            MG.add_node(item.id, pos=(item.id, x))


    color_list = []
    for node in nx.nodes(MG):

        item = Item.objects.get(id=node)

        color_list.append(item.id_fase.id)

    pos = nx.get_node_attributes(MG, 'pos')

    pos = nx.circular_layout(MG)

    nx.draw_networkx_nodes(MG, pos=pos, node_color=color_list, node_size=1500)

    nx.draw_networkx_labels(MG, pos=pos, labels=labels)


    #crear arcos
    edge_labels = []

    for fase in fases:
        items = Item.objects.filter(id_fase_id=fase.id).exclude(estado=6)

        for item in items:
            antecesores_padres = RelacionAntecesor.objects.filter(item_sucesor_id=item.id)

            for ap in antecesores_padres:
                item_origen = ap.item_antecesor

                item_destino = ap.item_sucesor
                edge = MG.add_edge(item_origen.id, item_destino.id)

                edge_labels.append(((item_origen.id, item_destino.id), 'Antecesor-Sucesor'))

            padre_hijo=RelacionPadre.objects.filter(item_hijo_id=item.id)

            for ap in padre_hijo:
                item_origen = ap.item_padre

                item_destino = ap.item_hijo
                edge = MG.add_edge(item_origen.id, item_destino.id)

                edge_labels.append(((item_origen.id, item_destino.id), 'Padre-Hijo'))

    #endfor
    nx.draw_networkx_edges(MG, pos=pos)


    #agregar etiquetas a los arcos
    edge_labels = dict(edge_labels)

    nx.draw_networkx_edge_labels(MG, pos=pos, edge_labels=edge_labels)


    ruta = "/home/mai/IS2/Repositorio/codigo/epm/EPMProject/files/" + proyecto.nombre + ".png"


        #verificar que no existan conflictos de nombres
    plt.savefig(ruta)

    MG.clear()
    plt.clf()
    plt.show()

    return ruta


# def get_items(id_item, version, lista=[]):
#
#
#
#     item = Item.objects.get(id=id_item)
#
#     sucesores_hijos = relaciones.objects.filter(item_origen_id=id_item, item_origen_version=version).exclude(activo=False)
#
#
#
#     for sh in sucesores_hijos:
#         lista.append(sh.item_destino)
#
#
#
#         get_items(sh.item_destino_id, sh.item_destino_version,lista)







def generar_grafo_item(request, item):
    """
Vista para crear el grafo de relaciones entre items de un proyecto dado
"""

    MG = nx.MultiDiGraph()
    print item
    context = RequestContext(request)
    #lista_items=[item.id]
    #lista_items = list(set(lista_items))

    #get_items(id_item,item.version,lista_items)

    lista_items = obtener_items(item)

    print lista_items
    #crear nodos
    i = 0


    x = 0
    labels = {}

    for item_id in lista_items:
        x = x + 1
        print("id del item", item_id)
        labels[item_id] = item_id
        print("labels", labels)

        MG.add_node(item_id, pos=(item_id, x))




    color_list = []
    for node in nx.nodes(MG):



        item = Item.objects.get(id=node)

        color_list.append(item.id_fase.id)

    pos = nx.get_node_attributes(MG, 'pos')

    #pos = nx.circular_layout(MG)
    nx.draw_networkx_nodes(MG, pos=pos, node_color=color_list, node_size=1500)



    nx.draw_networkx_labels(MG, pos=pos, labels=labels)




    #crear arcos
    edge_labels = []

    for item_id in lista_items:

            antecesores_padres = RelacionAntecesor.objects.filter(item_sucesor_id=item_id)

            for ap in antecesores_padres:
                item_origen = ap.item_antecesor

                item_destino = ap.item_sucesor
                edge = MG.add_edge(item_origen.id, item_destino.id)
                edge_labels.append(((item_origen.id, item_destino.id), "Antecesor-Sucesor"))


            padre_hijo=RelacionPadre.objects.filter(item_hijo_id=item_id)

            for ap in padre_hijo:
                item_origen = ap.item_padre

                item_destino = ap.item_hijo
                edge = MG.add_edge(item_origen.id, item_destino.id)
                edge_labels.append(((item_origen.id, item_destino.id), "Padre-Hijo"))



    #endfor
    print pos
    print edge_labels

    nx.draw_networkx_edges(MG, pos=pos)


    #agregar etiquetas a los arcos
    edge_labels = dict(edge_labels)

    nx.draw_networkx_edge_labels(MG, pos=pos, edge_labels=edge_labels)




    image_path = "/home/mai/IS2/Repositorio/codigo/epm/EPMProject/files/" + item.nombre+".png"



    #verificar que no existan conflictos de nombres
    plt.savefig(image_path)

    plt.show()


    MG.clear()
    plt.clf()
    return render_to_response('ver_grafo_relaciones.html', {'image_name': "image.png"} , context)

