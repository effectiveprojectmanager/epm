from django.contrib import admin
from EPMApp.models import Proyectos, RolesProyectos


class UsuarioRolesProyectosAdmin(admin.ModelAdmin):
    """
    Clase que representa la asignacion de roles de proyectos a usuarios del proyecto
    """
    fields = ('usuario', 'rol_proyecto', 'fase')

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        """Realiza el filtrado de los usuarios del proyecto actual"""
        if db_field.name == "usuario":
            print(request)
            proyecto_pk = request.session['id_proyecto']
            if proyecto_pk:
                print(proyecto_pk)
                p = Proyectos.objects.get(id=proyecto_pk)
                kwargs["queryset"] = p.miembros.all()
        elif db_field.name == "rol_proyecto":
            print(request)
            proyecto_pk = request.session['id_proyecto']
            if proyecto_pk:
                print(proyecto_pk)
                kwargs["queryset"] = RolesProyectos.objects.filter(id_proyecto=proyecto_pk)
        elif db_field.name == "fase":
            print(request)
            proyecto_pk = request.session['id_proyecto']
            if proyecto_pk:
                print(proyecto_pk)
                p = Proyectos.objects.get(id=proyecto_pk)
                kwargs["queryset"] = p.fases_set.all()

        return super(UsuarioRolesProyectosAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
