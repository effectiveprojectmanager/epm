from django.contrib.admin.views.main import ChangeList
from django.contrib import admin

from django.db.models import Q


class PermisosAdmin(admin.ModelAdmin):
    """Representa el administrador de Permisos del sistema"""
    list_display = ('name', 'codename')
    actions = None
    fields = ('name', 'codename')
    list_per_page = 13
    search_fields = ('name', 'codename')
    #change_list_template = 'admin/change_list_permission.html'

    def get_readonly_fields(self, request, obj=None):
        """Deshabilita la modificacion los campos"""
        return ('name', 'codename')

    def get_queryset(self, request):
        """
        Retorna la lista de permisos de la aplicacion
        @return: query
        @rtype: queryset
        """
        qs = super(PermisosAdmin, self).get_queryset(request)
        return qs.filter(Q(content_type__app_label="EPMApp") | Q(content_type__app_label="auth"))

    def has_add_permission(self, request):
        """
        deshabilita la opcion de agregar permisos
        @return False
        @rtype: Boolean
        """
        return False

    def has_delete_permission(self, request, obj=None):
        """
        Deshabilita la opcion de eliminar permisos
        @return: False
        @rtype: Boolean
        """
        return False

    def get_changelist(self, request, **kwargs):
        """
        Configuraciones de la lista principal de permisos
        @param: request,**kwargs
        @xtype:
        @return Lista de permisos
        """
        return PermisosChangeList


class PermisosChangeList(ChangeList):
    """
    Clase que administra la lista de cambio de permisos
    """
    def __init__(self, *args, **kwargs):
        super(PermisosChangeList, self).__init__(*args, **kwargs)
        self.title = "Permisos del sistema"